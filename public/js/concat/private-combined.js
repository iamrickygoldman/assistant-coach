document.addEventListener('DOMContentLoaded', () => {

	// Close Notification
	(document.querySelectorAll('.notification .delete') || []).forEach(($delete) => {
		const $notification = $delete.parentNode;
		$delete.addEventListener('click', () => {
			$notification.parentNode.removeChild($notification);
		});
	});


	// Get all "navbar-burger" elements
	const $navbarBurgers = Array.prototype.slice.call(document.querySelectorAll('.navbar-burger'), 0);

	// Check if there are any navbar burgers
	if ($navbarBurgers.length > 0) {

		// Add a click event on each of them
		$navbarBurgers.forEach( el => {
			el.addEventListener('click', () => {

				// Get the target from the "data-target" attribute
				const target = el.dataset.target;
				const $target = document.getElementById(target);

				// Toggle the "is-active" class on both the "navbar-burger" and the "navbar-menu"
				el.classList.toggle('is-active');
				$target.classList.toggle('is-active');

			});
		});
	}

});

/*
jQuery(function($){
	$(document).ready(function(){
		$('.navbar-link').on('click',function(){
			$(this).next('.navbar-dropdown').toggleClass('is-hidden-touch');
		});
	});
});
*/
jQuery(function($){
	$(document).ready(function($){
		// Panels
		$('.panel-toggle').on('click',function(){
			$('.panel-toggle').removeClass('is-active');
			$(this).addClass('is-active');
			if ($(this).data('toggle') === 'all')
			{
				$('.panel-block-toggle').removeClass('toggle-hidden');
			}
			else
			{
				$('.panel-block-toggle').addClass('toggle-hidden');
				$('.panel-block-toggle[data-toggle="'+$(this).data('toggle')+'"]').removeClass('toggle-hidden');
			}
			$('input[name="panel-search"]').trigger('input');
		});
		$('input[name="panel-search"]').on('input',function(){
			var search = $(this).val().toLowerCase();
			$('.panel-block-toggle').addClass('search-hidden').each(function(){
				if ($(this).data('search').toLowerCase().includes(search))
				{
					$(this).removeClass('search-hidden');
				}
			});
		});

		// Modals
		$('.modal-show').on('click',function(){
			$('.modal[data-modal="'+$(this).data('modal')+'"]').addClass('is-active');
		});
		$('.modal-hide').on('click',function(){
			var modalName = $(this).data('modal');
			if (typeof modalName === 'string' && $(this).data('modal') != '')
			{
				$('.modal[data-modal="'+$(this).data('modal')+'"]').removeClass('is-active');
			}
			else
			{
				$(this).closest('.modal').removeClass('is-active');
			}
		});
		$('.modal-background').on('click',function(){
			$('.modal').removeClass('is-active');
		});
	});
});
