ACCal = window.ACCal || {};

ACCal.Helper = {
	loadingCount: 0,
	hideLoading: function() {
		hideLoading();
	},
	showLoading: function() {
		showLoading();
	},
	ajax: function(params) {
		ajax(params);
	},
	hasClass: function(ele,cls) {
		return !!ele.className.match(new RegExp('(\\s|^)'+cls+'(\\s|$)'));
	},
	addClass: function(ele,cls) {
		if (!this.hasClass(ele,cls)) (ele.className += " "+cls).trim();
		return ele;
	},
	removeClass: function(ele,cls) {
		if (this.hasClass(ele,cls)) {
			let reg = new RegExp('(\\s|^)'+cls+'(\\s|$)');
			ele.className=ele.className.replace(reg,' ').trim();
		}
		return ele;
	},
	isObject: function(e){
		return typeof e === 'object' && !Array.isArray(e) && e !== null;
	}
}

function ajax(params) {
	let xhr = new XMLHttpRequest();

	if (typeof params.loading !== 'boolean') {
		params.loading = true;
	}
	if (params.loading) {
		ACCal.Helper.loadingCount++;
		if (ACCal.Helper.loadingCount > 0) {
			ACCal.Helper.showLoading();
		}
	}
	if (typeof params.url === 'undefined') {
		params.url = '';
	}
	if (typeof params.type === 'undefined') {
		params.type = 'GET';
	}
	params.type = params.type.toUpperCase();
	if (typeof params.dataType === 'undefined') {
		params.dataType = 'text';
	}
	xhr.responseType = 'text';
	if (!ACCal.Helper.isObject(params.headers)) {
		params.headers = {};
	}
	const headersKeys = Object.keys(params.headers);
	headersKeys.forEach((key, index) => {
		xhr.setRequestHeader(key, params.headers[key]);
	});

	if (typeof params.load === 'function') {
		xhr.onload = function(){
			if (xhr.status >= 200 && xhr.status < 300) {
				let response = xhr.responseText;
				if (params.dataType === 'json') {
					response = JSON.parse(response);
				}
				params.load(response, xhr.status, xhr.statusText);
			}
			else {
				params.error(xhr);
			}
			if (params.loading) {
				ACCal.Helper.loadingCount--;
				if (ACCal.Helper.loadingCount === 0) {
					ACCal.Helper.hideLoading();
				}
			}
		}
	}
	if (typeof params.error === 'function') {
		xhr.onerror = function(){
			params.error(xhr);
		}
	}

	if (typeof params.timeout === 'undefined') {
		xhr.timeout = 10000;
	}
	else {
		xhr.timeout = params.timeout;
	}
	if (!ACCal.Helper.isObject(params.data)) {
		params.data = {};
	}
	const dataKeys = Object.keys(params.data);
	let first = true;
	if (params.type === 'GET') {
		dataKeys.forEach((key, index) => {
			if (first) {
				params.url += '?';
				first = false;
			}
			else {
				params.url += '&';
			}
			params.url += key + '=' + params.data[key];
		});
	}

	xhr.open(params.type, params.url);

	if (params.type === 'POST') {
		let formData = new FormData();
		dataKeys.forEach((key, index) => {
			formData.append(key, params.data[key]);
		});
		xhr.send(formData);
	}
	else {
		xhr.send();
	}
}

function hideLoading() {
	document.querySelectorAll('.ac-loading-overlay').forEach(overlay => ACCal.Helper.removeClass(ACCal.Helper.addClass(overlay, 'is-hidden'), 'is-flex'));
}

function showLoading() {
	document.querySelectorAll('.ac-loading-overlay').forEach(overlay => ACCal.Helper.removeClass(ACCal.Helper.addClass(overlay, 'is-flex'), 'is-hidden'));
}
ACCal = window.ACCal || {};

ACCal.Localize = {
	days: {
		long: ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"],
		short: ["Sun","Mon","Tue","Wed","Thu","Fri","Sat"],
	},
	intervals: ['hour','day','week','month','year','date'],
	months: {
		long: ["January","February","March","April","May","June","July","August","September","October","November","December"],
		short: ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"],
	},
	booking: {
		formSuccess: 'Your booking request has submitted successfully.',
		formError: 'Something went wrong with your booking. Please try again later.',
	},
	initialize: function(){
		var self = this;
		ACCal.Helper.ajax({
			type: "get",
			url: "/api/calendar/localize",
			headers: {},
			data: {},
			dataType: 'json',
			load: function(data){
				self.days = data.days;
				self.intervals = data.intervals;
				self.months = data.months;
				self.booking = data.booking;
				self.initialized = true;
			},
			error: function(error){
				console.log(error);
			}
		});
	},
	initialized: false,
}
ACCal = window.ACCal || {};

ACCal.Form = {
	addSubmitFormListener: function() {
		document.getElementById("request-booking-form").addEventListener("submit", function(e){
			e.preventDefault();
			ACCal.Helper.ajax({
				type: "post",
				url: "/api/calendar/booking/request",
				headers: {},
				data: {
					start: document.getElementById('ac_start').value,
					camp_id: document.getElementById('ac_camp_id').value,
					trainer_id: document.getElementById('ac_trainer_id').value,
					organization_id: document.getElementById('ac_organization_id').value,
					first_name: document.getElementById('ac_first_name').value,
					last_name: document.getElementById('ac_last_name').value,
					email: document.getElementById('ac_email').value,
					phone: document.getElementById('ac_phone').value,
					notes: document.getElementById('ac_notes').value,
				},
				dataType: 'json',
				load: function(data){
					console.log(data);
					ACCal.Structure.hideModal();
					if (data.success) {
						ACCal.Structure.showNotification(ACCal.Localize.booking.bookingSuccess, 'success');
					}
					else {
						ACCal.Structure.showNotification(data.error, 'danger');
					}
				},
				error: function(error){
					console.log(error);
					ACCal.Structure.hideModal();
					ACCal.Structure.showNotification(ACCal.Localize.booking.bookingError, 'danger');
				},
			});;
		});
	}
}
ACCal = window.ACCal || {};

ACCal.Structure = {
	next: function(){
		next();
	},
	initialize: function(){
		initialize();
	},

	getUpdateResourcesParams: function(){
		return getUpdateResourcesParams();
	},
	getUpdateFrameParams: function(){
		return getUpdateFrameParams();
	},
	getUpdateDayParams: function(){
		return getUpdateDayParams();
	},
	getUpdateWeekParams: function(){
		return getUpdateWeekParams();
	},
	getUpdateMonthParams: function(){
		return getUpdateMonthParams();
	},
	getUpdateFormParams: function(){
		return getUpdateFormParams();
	},

	readyResources: false,
	readyFrame: false,
	readyDay: false,
	readyWeek: false,
	readyMonth: false,
	readyForm: false,

	selectedDate: '',
	selectedTime: '',
	offsetWeek: 0,
	offsetMonth: 0,

	updateResources: function(){
		ACCal.Helper.ajax(this.getUpdateResourcesParams());
	},
	updateFrame: function(){
		ACCal.Helper.ajax(this.getUpdateFrameParams());
	},
	updateDay: function(){
		ACCal.Helper.ajax(this.getUpdateDayParams());
	},
	updateWeek: function(){
		ACCal.Helper.ajax(this.getUpdateWeekParams());
	},
	updateMonth: function(){
		ACCal.Helper.ajax(this.getUpdateMonthParams());
	},
	updateForm: function(){
		ACCal.Helper.ajax(this.getUpdateFormParams());
	},

	addTogglePanelsListener: function(){
		addTogglePanelsListener();
	},
	togglePanels: function(e){
		togglePanels(e);
	},
	addShowModalListener: function(){
		addShowModalListener();
	},
	showModal: function(){
		showModal();
	},
	addHideModalListener: function(){
		addHideModalListener();
	},
	hideModal: function(){
		hideModal();
	},
	addSubmitFormListener: function(){
		addSubmitFormListener();
	},
	showNotification: function(message, type){
		showNotification(message, type);
	},
	addHideNotificationListener: function(){
		addHideNotificationListener();
	},
	hideNotification: function(e){
		hideNotification(e);
	}
}

function next(){
	if (!ACCal.Structure.readyResources)
	{
		ACCal.Structure.updateResources();
	}
	else if (!ACCal.Structure.readyFrame)
	{
		ACCal.Structure.updateFrame();
	}
	else if (!ACCal.Structure.readyWeek)
	{
		ACCal.Structure.updateWeek();
	}
	else if (!ACCal.Structure.readyMonth)
	{
		ACCal.Structure.updateMonth();
	}
}

function initialize(){
	ACCal.Structure.next();
}

function getUpdateResourcesParams(){
	ACCal.Structure.readyResources = false;
	return {
		type: "get",
		url: "/api/calendar/base-url",
		headers: {},
		data: {},
		dataType: 'json',
		load: function(data){
			document.head.insertAdjacentHTML('beforeend','<link href="'+data.url+'/css/calendar-combined.min.css" rel="stylesheet" type="text/css">');
			ACCal.Structure.readyResources = true;
			ACCal.Structure.next();
		},
		error: function(error){
			console.log(error);
		},
	};
}

function getUpdateFrameParams(){
	ACCal.Structure.readyFrame = false;
	return {
		type: "get",
		url: "/api/calendar/frame",
		headers: {},
		data: {},
		dataType: 'html',
		load: function(html){
			let cal = document.getElementsByClassName('ac-calendar');
			for (let i = 0; i < cal.length; i++) {
				cal[i].insertAdjacentHTML('beforeend',html);
			}
			ACCal.Structure.addTogglePanelsListener();
			ACCal.Structure.addHideNotificationListener()
			ACCal.Structure.readyFrame = true;
			ACCal.Structure.next();
		},
		error: function(error){
			console.log(error);
		},
	};
}

function getUpdateDayParams(){
	ACCal.Structure.readyDay = false;
	ACCal.Helper.showLoading();
	return {
		type: "get",
		url: "/api/calendar/day",
		headers: {},
		data: {
			date: ACCal.Structure.selectedDate,
		},
		dataType: 'html',
		load: function(html){
			let day = document.getElementById('ac-modal-content');
			day.innerHTML = html;
			ACCal.Structure.addShowModalListener();
			ACCal.Structure.readyDay = true;
			ACCal.Structure.showModal();
			ACCal.Structure.addHideModalListener();
			ACCal.Helper.hideLoading();
		},
		error: function(error){
			console.log(error);
		},
	};
}

function getUpdateWeekParams(){
	ACCal.Structure.readyWeek = false;
	ACCal.Helper.showLoading();
	return {
		type: "get",
		url: "/api/calendar/week",
		headers: {},
		data: {
			offset: ACCal.Structure.offsetWeek,
		},
		dataType: 'html',
		load: function(html){
			let week = document.getElementsByClassName('ac-week');
			for (let i = 0; i < week.length; i++) {
				week[i].innerHTML = html;
			}
			ACCal.Structure.addShowModalListener();
			ACCal.Structure.readyWeek = true;
			ACCal.Helper.hideLoading();
			ACCal.Structure.next();
		},
		error: function(error){
			console.log(error);
		},
	};
}

function getUpdateMonthParams(){
	ACCal.Structure.readyMonth = false;
	return {
		type: "get",
		url: "/api/calendar/month",
		headers: {},
		data: {
			offset: ACCal.Structure.offsetMonth,
		},
		dataType: 'html',
		load: function(html){
			let month = document.getElementsByClassName('ac-month');
			for (let i = 0; i < month.length; i++) {
				month[i].innerHTML = html;
			}
			ACCal.Structure.addShowModalListener();
			ACCal.Structure.readyMonth = true;
			ACCal.Helper.hideLoading();
			ACCal.Structure.next();
		},
		error: function(error){
			console.log(error);
		},
	};
}

function getUpdateFormParams(){
	ACCal.Structure.readyForm = false;
	ACCal.Helper.showLoading();
	return {
		type: "get",
		url: "/api/calendar/form",
		headers: {},
		data: {
			date: ACCal.Structure.selectedDate,
			time: ACCal.Structure.selectedTime,
		},
		dataType: 'html',
		load: function(html){
			let form = document.getElementById('ac-modal-content');
			form.innerHTML = html;
			ACCal.Structure.readyForm = true;
			ACCal.Structure.showModal();
			ACCal.Structure.addHideModalListener();
			ACCal.Structure.addShowModalListener();
			ACCal.Structure.addSubmitFormListener();
			ACCal.Helper.hideLoading();
		},
		error: function(error){
			console.log(error);
		},
	};
}

function addTogglePanelsListener(){
	document.querySelectorAll(".ac-calendar .panel-toggle").forEach(toggle => toggle.addEventListener("click", function(e){
		ACCal.Structure.togglePanels(e);
	}));
}
function togglePanels(e){
	document.querySelectorAll(".ac-calendar .panel-toggle").forEach(toggle => ACCal.Helper.removeClass(toggle.parentNode,'is-active'));
	ACCal.Helper.addClass(e.target.parentNode,'is-active');
	document.querySelectorAll(".ac-calendar .panel-block-toggle").forEach(block => ACCal.Helper.addClass(block,'toggle-hidden'));
	ACCal.Helper.removeClass(document.querySelector(".ac-calendar .panel-block-toggle[data-toggle=\""+e.target.dataset.toggle+"\"]"),'toggle-hidden');
}

function addShowModalListener(){
	document.querySelectorAll(".ac-calendar .modal-show").forEach(toggle => toggle.addEventListener("click", function(e){
		if (ACCal.Helper.hasClass(this,'open-day'))
		{
			ACCal.Structure.selectedDate = this.getAttribute('data-date');
			ACCal.Structure.updateDay();
		}
		else if (ACCal.Helper.hasClass(this,'open-form'))
		{
			ACCal.Structure.selectedDate = this.getAttribute('data-date');
			ACCal.Structure.selectedTime = this.getAttribute('data-time');
			ACCal.Structure.updateForm();
		}
	}));
}
function showModal(){
	ACCal.Helper.addClass(document.getElementById("ac-calendar-modal"),'is-active');
}

function addHideModalListener(){
	document.querySelectorAll(".ac-calendar .modal-hide").forEach(toggle => toggle.addEventListener("click", hideModal));
}
function hideModal(){
	ACCal.Helper.removeClass(document.getElementById("ac-calendar-modal"),'is-active');
}
function addSubmitFormListener(){
	ACCal.Form.addSubmitFormListener();
}
function showNotification(message, type){
	if (typeof type == 'undefined') {
		type = 'info';
	}
	(document.querySelectorAll('.form-notification') || []).forEach((notification) => {
		ACCal.Helper.removeClass(notification.parentNode,'is-hidden');
		notification.className = 'notification form-notification is-'+type;
	});
	(document.querySelectorAll('.form-notification .content') || []).forEach((content) => {
		content.innerHTML = message;
	});
}
function addHideNotificationListener(){
	(document.querySelectorAll('.notification .delete') || []).forEach((e) => {
		ACCal.Structure.hideNotification(e);
	});
}
function hideNotification(e){
	const notification = e.parentNode;
	const panel = notification.parentNode;
	e.addEventListener('click', () => {
		ACCal.Helper.addClass(panel,'is-hidden');
	});
}
ACCal = window.ACCal || {};

ACCal.Localize.initialize();

ACCal.Structure.initialize();