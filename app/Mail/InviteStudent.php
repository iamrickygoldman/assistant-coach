<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class InviteStudent extends Mailable
{
    use Queueable, SerializesModels;

    private string $key;
    private string $inviter_name;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(string $key, string $inviter_name)
    {
        $this->key = $key;
        $this->inviter_name = $inviter_name;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $params = [
            'url' => route('student.join',['key' => $this->key]),
            'inviter_name' => $this->inviter_name,
        ];

        return $this->view('emails.invite.student',$params);
    }
}
