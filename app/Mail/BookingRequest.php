<?php

namespace App\Mail;

use App\Models\BookingRequest as BookingRequestModel;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class BookingRequest extends Mailable
{
    use Queueable, SerializesModels;

    private BookingRequestModel $br;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(BookingRequestModel $br)
    {
        $this->br = $br;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $params = [
            'br' => $this->br,
            'url' => '/',
        ];

        return $this->view('emails.booking.request',$params);
    }
}
