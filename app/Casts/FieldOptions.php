<?php

namespace App\Casts;

use Illuminate\Contracts\Database\Eloquent\CastsAttributes;

class FieldOptions implements CastsAttributes
{
    /**
     * Cast the given value.
     *
     * @param  \Illuminate\Database\Eloquent\Model  $model
     * @param  string  $key
     * @param  mixed  $value
     * @param  array  $attributes
     * @return mixed
     */
    public function get($model, $key, $value, $attributes)
    {
        $tmp = json_decode($value,true);
        if (is_array($tmp))
        {
            if (isset($tmp['list']))
            {
                $list = explode(PHP_EOL,$tmp['list']);
                $tmp['list'] = [];
                foreach ($list as $item)
                {
                    $trim = trim($item);
                    $tmp['list'][$trim] = $trim;
                }
            }
        }
        return $tmp;
    }

    /**
     * Prepare the given value for storage.
     *
     * @param  \Illuminate\Database\Eloquent\Model  $model
     * @param  string  $key
     * @param  mixed  $value
     * @param  array  $attributes
     * @return mixed
     */
    public function set($model, $key, $value, $attributes)
    {
        return $value;
    }
}
