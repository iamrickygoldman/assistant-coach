<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

use App\Mail\Ping;

class PingMail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mail:ping';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Ping that email configuration is working by sending a test email.';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        Mail::to('iamrickygoldman@gmail.com')->send(new Ping());
    }
}
