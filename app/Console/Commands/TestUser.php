<?php

namespace App\Console\Commands;

use App\Models\Organization;
use App\Models\Role;
use App\Models\Student;
use App\Models\Trainer;
use App\Models\User;
use Illuminate\Console\Command;

class TestUser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'test:user {--students=} {--organizations=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a test user.';

    /**
     * Execute the console command.
     */
    public function handle(): void
    {
        $user = User::factory(1)->create()->first();
        if ($this->option('students') > 0)
        {
            $user->roles()->attach(Role::TRAINER_ID);
            if ($this->option('organizations') > 0)
            {
                $organizations = Organization::factory($this->option('organizations'))->create();
                foreach ($organizations as $organization)
                {
                    $trainer = Trainer::factory(1)->create(['user_id' => $user->id,'organization_id' => $organization->id])->first();
                    Student::factory($this->option('students'))->create(['organization_id' => $organization->id]);
                }
            }
            $trainer = Trainer::factory(1)->create(['user_id' => $user->id])->first();
            Student::factory($this->option('students'))->create(['trainer_id' => $trainer->id]);
        }
        $this->info('User ID: '.$user->id);
    }
}
