<?php

namespace App\Http\Controllers\Student;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Student;

class StudentController extends Controller
{
    public function showJoin(Request $request, $key)
    {
        $student = Student::where('invite_key',$key)->first();
        if (is_null($student))
        {
            return redirect(url('/'))->with('warning', __('notifications.expired_invite'));
        }
        if (!is_null($student->user_id))
        {
            return redirect(url('/'))->with('warning', __('notifications.expired_invite'));
        }
        $params = [
            'student' => $student
        ];
        
        return view('student.pages.join',$params);
    }
}
