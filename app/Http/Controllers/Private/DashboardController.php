<?php

namespace App\Http\Controllers\Private;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Helpers\Helper;
use App\Models\Role;
use App\Models\User;
use Auth;

class DashboardController extends Controller
{
    public function showDashboard(Request $request)
    {
        $roles = Helper::getPrivateRoles($this->current_user);
        if (count($roles) === 1)
        {
            return redirect(route($roles[0].'.dashboard'));
        }
        else if (count($roles) < 1)
        {
            return redirect('/');
        }

    	$params = [
            'user' => $this->current_user,
            'roles' => $roles,
        ];
    	
    	return view('private.pages.dashboard',$params);
    }
}
