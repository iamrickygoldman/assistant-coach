<?php

namespace App\Http\Controllers\Private;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Helpers\Helper;

class MessageController extends Controller
{
    public function showMessages(Request $request)
    {
        $params = [
            'user' => $this->current_user,
            'roles' => Helper::getPrivateRoles($this->current_user),
        ];
        
        return view('private.pages.message.list',$params);
    }
}
