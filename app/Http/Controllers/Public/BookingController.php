<?php

namespace App\Http\Controllers\Public;

use App\Http\Controllers\Controller;
use App\Models\BookingRequest;
use App\Models\Camp;
use App\Models\Organization;
use App\Models\Trainer;
use Illuminate\Http\Request;

use App\Models\Calendar;

class BookingController extends Controller
{
    public function apiRequestBooking(Request $request)
    {
        $validated_data = $request->validate([
            'camp_id' => 'nullable|integer',
            'trainer_id' => 'nullable|integer',
            'organization_id' => 'nullable|integer',
            'start' => 'nullable|date_format:Y-m-d H:i:s',
            'stop' => 'nullable|date_format:Y-m-d H:i:s',
            'first_name' => 'string',
            'last_name' => 'nullable|string',
            'email' => 'email',
            'phone' => 'nullable|string',
            'notes' => 'nullable|string',
            'fake' => 'nullable|boolean',
        ]);

        if (isset($validated_data['trainer_id']))
        {
            $trainer = Trainer::find($validated_data['trainer_id']);
            if (is_null($trainer))
            {
                return response()->json([
                    'success' => false,
                    'error' => __('notifications.invalid_address'),
                ], 400);
            }
        }
        if (isset($validated_data['organization_id']))
        {
            $organization = Organization::find($validated_data['organization_id']);
            if (is_null($organization))
            {
                return response()->json([
                    'success' => false,
                    'error' => __('notifications.invalid_address'),
                ], 400);
            }
        }
        if (isset($validated_data['camp_id']))
        {
            $camp = Camp::find($validated_data['camp_id']);
            if (is_null($camp))
            {
                return response()->json([
                    'success' => false,
                    'error' => __('notifications.invalid_address'),
                ], 400);
            }
        }

        if (isset($validated_data['trainer_id']) xor isset($validated_data['organization_id']))
        {
            $booking_request = BookingRequest::create($validated_data);
            $fake = isset($validated_data['fake']) ? $validated_data['fake'] : false;
            if (isset($trainer))
            {
                $booking_request->send($trainer->user->email,$fake);
            }
            elseif (isset($organization))
            {
                $email = is_null($organization->email) ? $organization->user->email : $organization->email;
                $booking_request->send($email,$fake);
            }
        }
        else {
            return response()->json([
                'success' => false,
                'error' => __('notifications.invalid_address'),
            ], 400);
        }

        return response()->json([
            'success' => true,
        ], 201);
    }
}
