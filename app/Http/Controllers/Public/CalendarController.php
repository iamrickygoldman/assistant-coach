<?php

namespace App\Http\Controllers\Public;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Helpers\Helper;
use App\Helpers\ValuesHelper;
use App\Models\Calendar;
use App\Models\Trainer;

use Carbon\Carbon;

class CalendarController extends Controller
{
    public function showCalendar(Request $request)
    {
        $validated_data = $request->validate([

        ]);

        $trainer = Trainer::find(1);

        $params = [
            'trainer' => $trainer,
        ];
        
        return view('public.pages.calendar.view',$params);
    }

    public function apiGetBaseUrl(Request $request)
    {
        return response()->json([
            'url' => Helper::getAbsoluteRootUrl($request),
        ]);
    }

    public function apiGetLocalize(Request $request)
    {
        return response()->json(Calendar::getLocale());
    }

    public function apiGetFrame(Request $request)
    {
        $validated_data = $request->validate([

        ]);

        $params = [
            'locale' => Calendar::getLocale(),
        ];

        $html = view('public.modules.calendar.frame',$params)->render();

        return response($html);
    }

    public function apiGetDay(Request $request)
    {
        $format = 'Y-m-d';
        $validated_data = $request->validate([
            'date' => 'date_format:'.$format,
        ]);

        $calendar = new Calendar;
        $days_locale = Calendar::getDaysLong(indexed: true);
        $date = Carbon::createFromFormat($format,$validated_data['date']);
        $day = new \stdClass;
        $day->date_full = $date->format('Y-m-d');
        $day->date = $date->format('n/j/Y');
        $day->schedule = $calendar->getDaySchedule('');
        $day->day = $days_locale[$date->format('w')];
        $day->hours_of_day = ValuesHelper::getHoursOfDay(start: 8, stop: 16);

        $params = [
            'locale' => Calendar::getLocale(),
            'day' => $day,
        ];
        
        $html = view('public.modules.calendar.day',$params)->render();

        return response($html);
    }

    public function apiGetWeek(Request $request)
    {
        $validated_data = $request->validate([
            'offset' => 'nullable|integer',
        ]);

        $offset = isset($validated_data['offset']) ? $validated_data['offset'] : 0;
        $calendar = new Calendar;
        $schedule = [];
        $carbon = Carbon::now();
        $carbon->addWeeks($offset);
        $current = $carbon->startOfWeek(Carbon::SUNDAY);
        $days_locale = Calendar::getDaysLong(indexed: true);
        for ($i = 0; $i < 7; $i++)
        {
            $tmp = new \stdClass;
            $tmp->date_full = $current->format('Y-m-d');
            $tmp->date = $current->format('m/d');
            $tmp->schedule = $calendar->getDaySchedule('');
            $tmp->day = $days_locale[$current->format('w')];
            $tmp->hours_of_day = ValuesHelper::getHoursOfDay(start: 8, stop: 16);
            $schedule[] = $tmp;
            $current->addDay();
        }
        $hours_of_day = ValuesHelper::getHoursOfDay();

        $params = [
            'hours_of_day' => $hours_of_day,
            'locale' => Calendar::getLocale(),
            'schedule' => $schedule,
            'offset' => $offset,
        ];
        
        $html = view('public.modules.calendar.week',$params)->render();

        return response($html);
    }

    public function apiGetMonth(Request $request)
    {
        $validated_data = $request->validate([
            'offset' => 'nullable|integer',
        ]);

        $offset = isset($validated_data['offset']) ? $validated_data['offset'] : 0;
        $calendar = new Calendar;
        $schedule = [];
        $carbon = Carbon::now();
        $carbon->addMonths($offset);
        $current = $carbon->startOfMonth();
        $month = $current->month;
        $days_locale = Calendar::getDaysLong(indexed: true);
        for ($i = 0; $i < 6; $i++)
        {
            $current = $current->startOfWeek(Carbon::SUNDAY);
            $week = [];
            for ($j = 0; $j < 7; $j++)
            {
                $tmp = new \stdClass;
                $tmp->date_full = $current->format('Y-m-d');
                $tmp->date = $current->format('m/d');
                $tmp->schedule = $calendar->getDaySchedule('');
                $tmp->day = $days_locale[$current->format('w')];
                $tmp->hours_of_day = ValuesHelper::getHoursOfDay(start: 8, stop: 16);
                $tmp->prev = $current->month < $month || ($current->month === 12 && $month === 1);
                $tmp->next = $current->month > $month || ($current->month === 1 && $month === 12);
                $week[] = $tmp;
                $current->addDay();
            }
            $schedule[] = $week;
            if ($current->month !== $month)
            {
                break;
            }
        }
        $hours_of_day = ValuesHelper::getHoursOfDay();
        $locale = Calendar::getLocale();
        $month_key = $month - 1;

        $params = [
            'hours_of_day' => $hours_of_day,
            'locale' => $locale,
            'schedule' => $schedule,
            'month' => $locale['months']['long'][array_keys($locale['months']['long'])[$month_key]],
            'offset' => $offset,
        ];
        
        $html = view('public.modules.calendar.month',$params)->render();

        return response($html);
    }

    public function apiGetForm(Request $request)
    {
        $date_format = 'Y-m-d';
        $time_format = 'H:i';
        $validated_data = $request->validate([
            'date' => 'date_format:'.$date_format,
            'time' => 'date_format:'.$time_format,
        ]);

        $trainer = Trainer::find(1);

        $calendar = new Calendar;
        $days_locale = Calendar::getDaysLong(indexed: true);
        $date = Carbon::createFromFormat($date_format,$validated_data['date']);
        $day = new \stdClass;
        $day->date_full = $date->format('Y-m-d');
        $day->date = $date->format('n/j/Y');
        $day->schedule = $calendar->getDaySchedule('');
        $day->day = $days_locale[$date->format('w')];
        
        $t = Carbon::createFromFormat($time_format,$validated_data['time']);
        $time = new \stdClass;
        $time->time_full = $t->format('H:i:s');
        $time->time = $t->format('g:ia');

        $params = [
            'locale' => Calendar::getLocale(),
            'day' => $day,
            'time' => $time,
            'trainer' => $trainer,
        ];
        
        $html = view('public.modules.calendar.form',$params)->render();

        return response($html);
    }
}
