<?php

namespace App\Http\Controllers\Coach;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Helpers\Helper;
use App\Models\Field;
use App\Models\Organization;
use App\Models\Trainer;

class FieldController extends Controller
{
    public function showFieldList(Request $request)
    {
        $validated_data = $request->validate([
            'list' => 'nullable|string',
            'sort' => 'nullable|string',
            'direction' => 'nullable|string|in:asc,desc',
        ]);

    	$field_groups = Trainer::getFieldGroups($this->current_user);
        $list = false;
        foreach ($field_groups as $group => $fields)
        {
            if (!$list)
            {
                $list = $group;
            }
        }
        if (isset($validated_data['list']))
        {
            $list = $validated_data['list'];
        }
        foreach ($field_groups as $group => $fields)
        {
            if ($group != $list)
            {
                $field_groups[$group] = [];
            }
        }
        $sort = isset($validated_data['sort']) ? $validated_data['sort'] : 'title';
        $direction = isset($validated_data['direction']) ? $validated_data['direction'] : 'asc';
        $is_asc = $direction === 'asc';

        $cols = [
            'title' => __('field.column_title'),
            'type_text' => __('field.column_type'),
        ];
        $col_data = [];
        foreach ($cols as $key => $title)
        {
            $tmp = new \stdClass;
            $tmp->key = $key;
            $tmp->title = $title;
            $tmp->sort = $sort === $key;
            $dir = $tmp->sort ? !$is_asc : 'asc';
            $tmp->direction = $dir ? 'asc' : 'desc';
            $tmp->arrow = $direction === 'asc' ? 'down' : 'up';
            $col_data[] = $tmp;
        }

        $all_fields = [];
        foreach ($field_groups as $group => $fields)
        {
            $all_fields = array_merge($all_fields,$fields);
        }
        usort($all_fields,function($a,$b) use ($sort,$is_asc){
            if ($is_asc)
            {
                return strcmp($a->$sort,$b->$sort);
            }
            return strcmp($b->$sort,$a->$sort);
        });

    	$params = [
    		'user' => $this->current_user,
    		'trainers' => $this->current_user->trainers,
    		'field_groups' => $field_groups,
            'all_fields' => $all_fields,
            'list' => $list,
            'query' => $validated_data,
            'columns' => $col_data,
    	];
    	
    	return view('coach.pages.field.list',$params);
    }

    public function showFieldNew(Request $request)
    {
        $validated_data = $request->validate([
            'o' => 'nullable|string',
        ]);

        $trainer_id = '';
        $organization_id = '';
        if (isset($validated_data['o']))
        {
            if (Helper::getNameFromGroup($validated_data['o']) === 'personal')
            {
                $id = Helper::getIDFromGroup($validated_data['o']);
                if ($id < 1)
                {
                    return redirect(route('coach.field.list'))->with('warning', __('notifications.invalid_address'));
                }
                $trainer = Trainer::find($id);
                if (!is_null($trainer) && !is_null($trainer->user) && $trainer->user->id === $this->current_user->id)
                {
                    $trainer_id = $trainer->id;
                }
                else
                {
                    return redirect(route('coach.field.list'))->with('warning', __('notifications.invalid_address'));
                }
            }
            else
            {
                $id = Helper::getIDFromGroup($validated_data['o']);
                if ($id < 1)
                {
                    return redirect(route('coach.field.list'))->with('warning', __('notifications.invalid_address'));
                }
                $organization = Organization::find($id);
                if (!is_null($organization) && $organization->hasTrainerUser($this->current_user))
                {
                    $organization_id = $organization->id;
                }
                else
                {
                    return redirect(route('coach.field.list'))->with('warning', __('notifications.invalid_address'));
                }
            }
        }
        else
        {
            return redirect(route('coach.field.list'))->with('warning', __('notifications.invalid_address'));
        }

        $params = [
            'user' => $this->current_user,
            'field' => null,
            'types' => Field::getTypeOptions(true),
            'trainer_id' => $trainer_id,
            'organization_id' => $organization_id,
            'list_option' => Field::TYPE_LIST,
        ];
        
        return view('coach.pages.field.edit',$params);
    }

    public function showFieldEdit(Request $request, $id)
    {
        $field = Field::find($id);
        if (is_null($field) || !$field->hasTrainerUser($this->current_user))
        {
            return redirect(url('/'))->with('warning', __('notifications.invalid_address'));
        }
        $list = '';
        if (!is_null($field->trainer_id))
        {
            $list = 'personal|'.$field->trainer_id;
        }
        else if (!is_null($field->organization_id))
        {
            $list = $field->organization->name.'|'.$field->organization_id;
        }

        $params = [
            'user' => $this->current_user,
            'field' => $field,
            'types' => Field::getTypeOptions(true),
            'trainer_id' => null,
            'organization_id' => null,
            'list' => $list,
            'list_option' => Field::TYPE_LIST,
        ];
        
        return view('coach.pages.field.edit',$params);
    }

    public function saveField(Request $request)
    {
        $validated_data = $request->validate([
            'id' => 'nullable|integer',
            'trainer_id' => 'nullable|integer',
            'organization_id' => 'nullable|integer',
            'title' => 'string',
            'type' => 'string',
            'display_students_list' => 'boolean',
            'options' => 'nullable|array',
            'options_list_text' => 'nullable|string',
        ]);

        if (isset($validated_data['organization_id']))
        {
            $organization = Organization::find($validated_data['organization_id']);
            if (is_null($organization) || !$organization->hasTrainerUser($this->current_user))
            {
                return redirect(url('/'))->with('warning', __('notifications.invalid_address'));
            }
        }
        if (isset($validated_data['trainer_id']))
        {
            $trainer = Trainer::find($validated_data['trainer_id']);
            if (is_null($trainer) || $trainer->user_id !== $this->current_user->id)
            {
                return redirect(url('/'))->with('warning', __('notifications.invalid_address'));
            }
        }

        if (!isset($validated_data['display_students_list']))
        {
            $validated_data['display_students_list'] = false;
        }

        if (isset($validated_data['options_list_text']))
        {
            $validated_data['options']['list'] = $validated_data['options_list_text'];
        }

        if (!isset($validated_data['id']))
        {
            if (isset($validated_data['trainer_id']) xor isset($validated_data['organization_id']))
            {
                $field = Field::create($validated_data);
            }
            else {
                return redirect(url('/'))->with('warning', __('notifications.invalid_address'));
            }
        }
        else
        {
            $field = Field::find($validated_data['id']);
            if (is_null($field) || !$field->hasTrainerUser($this->current_user))
            {
                return redirect(url('/'))->with('warning', __('notifications.invalid_address'));
            }
            $field->update($validated_data);
        }

        return redirect(route('coach.field.edit',['id' => $field->id]))->with('success', __('field.saved_field'));
    }

    public function deleteField(Request $request, $id)
    {
        $field = Field::find($id);
        if (is_null($field) || !$field->hasTrainerUser($this->current_user))
        {
            return redirect(url('/'))->with('warning', __('notifications.invalid_address'));
        }

        $field->delete();

        return redirect(route('coach.field.list'))->with('success', __('field.deleted_field'));
    }
}
