<?php

namespace App\Http\Controllers\Coach;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Trainer;

class DashboardController extends Controller
{
    public function showDashboard(Request $request)
    {
    	$params = [
    		'user' => $this->current_user,
    		'trainers' => $this->current_user->trainers,
    		'student_groups' => Trainer::getStudentGroups($this->current_user),
    	];
    	
    	return view('coach.pages.dashboard',$params);
    }
}
