<?php

namespace App\Http\Controllers\Coach;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;

use App\Helpers\Helper;
use App\Models\Organization;
use App\Models\Student;
use App\Models\Trainer;

class StudentController extends Controller
{
    public function showStudentList(Request $request)
    {
        $validated_data = $request->validate([
            'list' => 'nullable|string',
            'sort' => 'nullable|string',
            'direction' => 'nullable|string|in:asc,desc',
        ]);

    	$student_groups = Trainer::getStudentGroups($this->current_user);
        $list = false;
        foreach ($student_groups as $group => $students)
        {
            if (!$list)
            {
                $list = $group;
            }
        }
        if (isset($validated_data['list']))
        {
            $list = $validated_data['list'];
        }
        foreach ($student_groups as $group => $students)
        {
            if ($group != $list)
            {
                $student_groups[$group] = [];
            }
        }
        $sort = isset($validated_data['sort']) ? $validated_data['sort'] : 'name';
        $direction = isset($validated_data['direction']) ? $validated_data['direction'] : 'asc';
        $is_asc = $direction === 'asc';
        $sort_type = 'string';

        $cols = [
            'name' => 'Name',
            'birth_date_display' => 'Birthdate',
            'age' => 'Age',
        ];
        $col_data = [];
        foreach ($cols as $key => $title)
        {
            $tmp = new \stdClass;
            $tmp->key = $key;
            $tmp->title = $title;
            $tmp->sort = $sort === $key;
            $dir = $tmp->sort ? !$is_asc : 'asc';
            $tmp->direction = $dir ? 'asc' : 'desc';
            $tmp->arrow = $direction === 'asc' ? 'down' : 'up';
            $col_data[] = $tmp;
        }

        $all_students = [];
        foreach ($student_groups as $group => $students)
        {
            $all_students = array_merge($all_students,$students);
        }
        usort($all_students,function($a,$b) use ($sort,$is_asc, $sort_type){
            if (!isset($a->$sort) || !isset($b->$sort) || $sort_type === 'none')
            {
                return 0;
            }
            switch ($sort_type)
            {
                case 'string':
                    if ($is_asc)
                    {
                        return strcmp($a->$sort,$b->$sort);
                    }
                    return strcmp($b->$sort,$a->$sort);
                case 'number':
                    if ($a->$sort == $b->$sort)
                    {
                        return 0;
                    }
                    if ($is_asc)
                    {
                        return $a->$sort < $b->$sort;
                    }
                    return $b->$sort < $a->$sort;
                case 'datetime':
                    $da = Carbon::createFromFormat('M j, Y', $a->$sort);
                    $db = Carbon::createFromFormat('M j, Y', $b->$sort);
                    if ($da === $db)
                    {
                        return 0;
                    }
                    if ($is_asc)
                    {
                        return $da < $db;
                    }
                    return $db < $da;
            }
            return 0;
        });

    	$params = [
    		'user' => $this->current_user,
    		'trainers' => $this->current_user->trainers,
    		'student_groups' => $student_groups,
            'all_students' => $all_students,
            'list' => $list,
            'query' => $validated_data,
            'columns' => $col_data,
    	];
    	
    	return view('coach.pages.student.list',$params);
    }

    public function showStudentNew(Request $request)
    {
        $validated_data = $request->validate([
            'o' => 'nullable|string',
        ]);

        $trainer_id = '';
        $organization_id = '';
        $field_list = [];
        if (isset($validated_data['o']))
        {
            if (Helper::getNameFromGroup($validated_data['o']) === 'personal')
            {
                $id = Helper::getIDFromGroup($validated_data['o']);
                if ($id < 1)
                {
                    return redirect(route('coach.student.list'))->with('warning', __('notifications.invalid_address'));
                }
                $trainer = Trainer::find($id);
                if (!is_null($trainer) && !is_null($trainer->user) && $trainer->user->id === $this->current_user->id)
                {
                    $trainer_id = $trainer->id;
                    $field_list = $trainer->fields;
                }
                else
                {
                    return redirect(route('coach.student.list'))->with('warning', __('notifications.invalid_address'));
                }
            }
            else
            {
                $id = Helper::getIDFromGroup($validated_data['o']);
                if ($id < 1)
                {
                    return redirect(route('coach.student.list'))->with('warning', __('notifications.invalid_address'));
                }
                $organization = Organization::find($id);
                if (!is_null($organization) && $organization->hasTrainerUser($this->current_user))
                {
                    $organization_id = $organization->id;
                    $field_list = $organization->fields;
                }
                else
                {
                    return redirect(route('coach.student.list'))->with('warning', __('notifications.invalid_address'));
                }
            }
        }
        else
        {
            return redirect(route('coach.student.list'))->with('warning', __('notifications.invalid_address'));
        }

        $params = [
            'user' => $this->current_user,
            'student' => null,
            'trainer_id' => $trainer_id,
            'organization_id' => $organization_id,
            'list' => $validated_data['o'],
            'field_list' => $field_list,
        ];
        
        return view('coach.pages.student.edit',$params);
    }

    public function showStudentEdit(Request $request, $id)
    {
        $student = Student::find($id);
        if (is_null($student) || !$student->hasTrainerUser($this->current_user))
        {
            return redirect(url('/'))->with('warning', __('notifications.invalid_address'));
        }
        $trainer_id = null;
        $organization_id = null;
        $list = '';
        if (!is_null($student->trainer_id))
        {
            $trainer_id = $student->trainer_id;
            $list = 'personal|'.$student->trainer_id;
        }
        else if (!is_null($student->organization_id))
        {
            $organization_id = $student->organization_id;
            $list = $student->organization->name.'|'.$student->organization_id;
        }

        $params = [
            'user' => $this->current_user,
            'student' => $student,
            'trainer_id' => $trainer_id,
            'organization_id' => $organization_id,
            'list' => $list,
            'field_list' => $student->field_list,
            'invite_action' => route('coach.student.invite',['id' => $student->id]),
            'invite_email_label' => __('student.invite_email'),
        ];
        
        return view('coach.pages.student.edit',$params);
    }

    public function saveStudent(Request $request)
    {
        $validated_data = $request->validate([
            'id' => 'nullable|integer',
            'trainer_id' => 'nullable|integer',
            'organization_id' => 'nullable|integer',
            'nickname' => 'nullable|string',
            'birth_date' => 'nullable|date_format:Y-m-d',
            'birth_day' => 'nullable|integer',
            'birth_month' => 'nullable|integer',
            'birth_year' => 'nullable|integer',
            'field_data' => 'array',
            'invite_email' => 'nullable|email',
            'fake' => 'nullable|boolean',
        ]);

        if (isset($validated_data['trainer_id']))
        {
            $trainer = Trainer::find($validated_data['trainer_id']);
            if (is_null($trainer) || $trainer->user_id !== $this->current_user->id)
            {
                return redirect(url('/'))->with('warning', __('notifications.invalid_address'));
            }
            $name = $trainer->name;
        }
        if (isset($validated_data['organization_id']))
        {
            $organization = Organization::find($validated_data['organization_id']);
            if (is_null($organization) || !$organization->hasTrainerUser($this->current_user))
            {
                return redirect(url('/'))->with('warning', __('notifications.invalid_address'));
            }
            $name = $organization->name;
        }

        if (!isset($validated_data['id']))
        {
            if (isset($validated_data['trainer_id']) xor isset($validated_data['organization_id']))
            {
                $field_data = $validated_data['field_data'];
                unset($validated_data['field_data']);
                $student = Student::create($validated_data);
                $validated_data['field_data'] = $field_data;
                // So field_data will sync
                $student->update($validated_data);
                if (isset($validated_data['invite_email']))
                {
                    $fake = isset($validated_data['fake']) ? $validated_data['fake'] : false;
                    $student->sendInvite($name, $validated_data['invite_email'],$fake);
                }
            }
            else {
                return redirect(url('/'))->with('warning', __('notifications.invalid_address'));
            }
        }
        else
        {
            $student = Student::find($validated_data['id']);
            if (is_null($student) || !$student->hasTrainerUser($this->current_user))
            {
                return redirect(url('/'))->with('warning', __('notifications.invalid_address'));
            }
            $student->update($validated_data);
        }

        return redirect(route('coach.student.edit',['id' => $student->id]))->with('success', __('student.saved_student'));
    }

    public function deleteStudent(Request $request, $id)
    {
        $student = Student::find($id);
        if (is_null($student) || !$student->hasTrainerUser($this->current_user))
        {
            return redirect(url('/'))->with('warning', __('notifications.invalid_address'));
        }
        $student->delete();

        return redirect(route('coach.student.list'))->with('success', __('student.deleted_student'));
    }

    public function inviteStudent(Request $request, $id)
    {
        $validated_data = $request->validate([
            'trainer_id' => 'nullable|integer',
            'organization_id' => 'nullable|integer',
            'email' => 'required||string|email|max:255',
            'fake' => 'nullable|boolean',
        ]);

        if (isset($validated_data['trainer_id']))
        {
            $trainer = Trainer::find($validated_data['trainer_id']);
            if (is_null($trainer) || $trainer->user_id !== $this->current_user->id)
            {
                return redirect(url('/'))->with('warning', __('notifications.invalid_address'));
            }
            $name = $trainer->name;
        }
        if (isset($validated_data['organization_id']))
        {
            $organization = Organization::find($validated_data['organization_id']);
            if (is_null($organization) || !$organization->hasTrainerUser($this->current_user))
            {
                return redirect(url('/'))->with('warning', __('notifications.invalid_address'));
            }
            $name = $organization->name;
        }

        if (isset($validated_data['trainer_id']) xor isset($validated_data['organization_id']))
        {
            $student = Student::find($id);
            if (is_null($student) || !$student->hasTrainerUser($this->current_user))
            {
                return redirect(url('/'))->with('warning', __('notifications.invalid_address'));
            }

            $fake = isset($validated_data['fake']) ? $validated_data['fake'] : false;
            $student->sendInvite($name, $validated_data['email'],$fake);
        }
        else
        {
            return redirect(url('/'))->with('warning', __('notifications.invalid_address'));
        }

        return redirect(route('coach.student.edit', ['id' => $id]))->with('success', __('student.invite_success', ['email' => $validated_data['email']]));
    }
}
