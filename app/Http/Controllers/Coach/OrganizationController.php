<?php

namespace App\Http\Controllers\Coach;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Organization;

class OrganizationController extends Controller
{
    public function showOrganizationList(Request $request)
    {
        $validated_data = $request->validate([
            'sort' => 'nullable|string',
            'direction' => 'nullable|string|in:asc,desc',
        ]);

        $sort = isset($validated_data['sort']) ? $validated_data['sort'] : 'name';
        $direction = isset($validated_data['direction']) ? $validated_data['direction'] : 'asc';
        $is_asc = $direction === 'asc';
        $sort_type = 'string';

        $cols = [
            'name' => 'Name',
            'trainers_count' => 'Coaches',
            'students_count' => 'Students',
            'created_at' => 'Creation Date',
        ];
        $col_data = [];
        foreach ($cols as $key => $title)
        {
            $tmp = new \stdClass;
            $tmp->key = $key;
            $tmp->title = $title;
            $tmp->sort = $sort === $key;
            $dir = $tmp->sort ? !$is_asc : 'asc';
            $tmp->direction = $dir ? 'asc' : 'desc';
            $tmp->arrow = $direction === 'asc' ? 'down' : 'up';
            $col_data[] = $tmp;
        }

        $organizations = $this->current_user->organization_memberships;
        usort($organizations,function($a,$b) use ($sort,$is_asc, $sort_type){
            if (!isset($a->$sort) || !isset($b->$sort) || $sort_type === 'none')
            {
                return 0;
            }
            switch ($sort_type)
            {
                case 'string':
                    if ($is_asc)
                    {
                        return strcmp($a->$sort,$b->$sort);
                    }
                    return strcmp($b->$sort,$a->$sort);
                case 'number':
                    if ($a->$sort == $b->$sort)
                    {
                        return 0;
                    }
                    if ($is_asc)
                    {
                        return $a->$sort < $b->$sort;
                    }
                    return $b->$sort < $a->$sort;
                case 'datetime':
                    $da = Carbon::createFromFormat('M j, Y', $a->$sort);
                    $db = Carbon::createFromFormat('M j, Y', $b->$sort);
                    if ($da === $db)
                    {
                        return 0;
                    }
                    if ($is_asc)
                    {
                        return $da < $db;
                    }
                    return $db < $da;
            }
            return 0;
        });

        $params = [
            'user' => $this->current_user,
            'trainers' => $this->current_user->trainers,
            'organizations' => $organizations,
            'query' => $validated_data,
            'columns' => $col_data,
        ];
        
        return view('coach.pages.organization.list',$params);
    }

    public function showOrganizationNew(Request $request)
    {
        $params = [
            'user' => $this->current_user,
            'organization' => null,
        ];
        
        return view('coach.pages.organization.edit',$params);
    }

    public function showOrganizationEdit(Request $request, $id)
    {
        $organization = Organization::find($id);
        if (is_null($organization) || !$organization->hasTrainerUser($this->current_user))
        {
            return redirect(url('/'))->with('warning', __('notifications.invalid_address'));
        }
        else if ($organization->user_id !== $this->current_user->id)
        {
            return redirect(route('coach.organization.view',['id' => $organization->id]))->with('warning', __('notifications.cannot_edit_organization'));
        }

        $params = [
            'user' => $this->current_user,
            'organization' => $organization,
        ];
        
        return view('coach.pages.organization.edit',$params);
    }

    public function showOrganizationView(Request $request, $id)
    {
        $organization = Organization::find($id);
        if (is_null($organization) || !$organization->hasTrainerUser($this->current_user))
        {
            return redirect(url('/'))->with('warning', __('notifications.invalid_address'));
        }

        $params = [
            'user' => $this->current_user,
            'organization' => $organization,
        ];
        
        return view('coach.pages.organization.view',$params);
    }

    public function saveOrganization(Request $request)
    {
        $validated_data = $request->validate([
            'id' => 'nullable|integer',
            'name' => 'string',
            'email' => 'nullable|email',
            'phone' => 'nullable|string',
            'address1' => 'nullable|string',
            'address2' => 'nullable|string',
            'city' => 'nullable|string',
            'state' => 'nullable|string',
            'zip' => 'nullable|string',
            'country' => 'nullable|string',
        ]);

        if (!isset($validated_data['id']))
        {
            $validated_data['user_id'] = $this->current_user->id;
            $organization = Organization::create($validated_data);
        }
        else
        {
            $organization = Organization::find($validated_data['id']);
            if (is_null($organization) || $organization->user_id !== $this->current_user->id)
            {
                return redirect(url('/'))->with('warning', __('notifications.invalid_address'));
            }
            $organization->update($validated_data);
        }

        return redirect(route('coach.organization.edit',['id' => $organization->id]))->with('success', __('organization.saved_organization'));
    }

    public function deleteOrganization(Request $request, $id)
    {
        $organization = Organization::find($id);
        if (is_null($organization) || $organization->user_id !== $this->current_user->id)
        {
            return redirect(url('/'))->with('warning', __('notifications.invalid_address'));
        }
        $organization->delete();

        return redirect(route('coach.organization.list'))->with('success', __('organization.deleted_organization'));
    }
}
