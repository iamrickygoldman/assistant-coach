<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

use App\Models\User;

class HasRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next, int ...$role_ids)
    {
        $user = User::currentUser($request);

        if ($user)
        {
            foreach ($role_ids as $role_id)
            {
                if ($user->hasRole($role_id))
                {
                    return $next($request);
                }
            }
        }

        return redirect('/');
    }
}
