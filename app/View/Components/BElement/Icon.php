<?php

namespace App\View\Components\BElement;

use Illuminate\View\Component;

class Icon extends Component
{
    public static int $counter = 0;

    public string $count;
    public string $icon;
    public string $class;
    public int $height;
    public int $width;
    public ?string $title;
    public ?string $link;
    public ?string $slug;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(string $icon, string $color = 'black', ?string $size = null, string $class = 'icon', ?string $title = null, ?string $link = null, ?string $slug = '')
    {
        if (self::$counter === 0)
        {
            self::$counter = time();
        }
        $this->slug = $slug;
        $this->count = $this->slug.++self::$counter;
        $this->icon = $icon;
        $this->class = $class.' is-'.$color;
        $this->title = $title;
        if (!is_null($size))
        {
            $this->class .= ' is-'.$size;
        }
        switch ($size)
        {
            case 'small':
                $this->height = 16;
                $this->width = 16;
                break;
            case 'medium':
                $this->height = 32;
                $this->width = 32;
                break;
            case 'large':
                $this->height = 48;
                $this->width = 48;
                break;
            default:
                $this->height = 24;
                $this->width = 24;
                break;
        }
        $this->link = $link;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.b-element.icon');
    }
}
