<?php

namespace App\View\Components\BElement;

use Illuminate\View\Component;

class Notification extends Component
{
    public string $class;
    public bool $close;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(string $class = 'notification', string $type = '', bool $close = true)
    {
        $this->class = $class;
        $this->class .= ' is-'.$type;
        $this->close = $close;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.b-element.notification');
    }
}
