<?php

namespace App\View\Components\BView;

use Illuminate\View\Component;

class List extends Component
{
    public array $items;
    public array $columns;
    public string $route;
    public ?string $edit;
    public array $query;
    public ?string $title;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(string $route, array $items = [], array $columns = [], ?string $edit = null, array $query = [], ?string $title = null)
    {
        $this->items = $items;
        $this->columns = $columns;
        $this->route = $route;
        $this->edit = $edit;
        $this->query = $query;
        $this->title = $title;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.b-view.list');
    }
}
