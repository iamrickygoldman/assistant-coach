<?php

namespace App\View\Components\BView;

use Illuminate\View\Component;

class GroupedList extends Component
{
    public array $groups;
    public array $items;
    public array $columns;
    public string $route;
    public ?string $edit;
    public string $list;
    public array $query;
    public ?string $title;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(string $route, array $groups = [], array $items = [], array $columns = [], ?string $edit = null, string $list = '', array $query = [], ?string $title = null)
    {
        $this->groups = $groups;
        $this->items = $items;
        $this->columns = $columns;
        $this->route = $route;
        $this->edit = $edit;
        $this->list = $list;
        $this->query = $query;
        $this->title = $title;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.b-view.grouped-list');
    }
}
