<?php

namespace App\View\Components\BForm;

use Illuminate\View\Component;
use App\View\Components\BForm\Text;

class Email extends Text
{   
    protected function postProcess()
    {
        $this->type = 'email';
    }
}
