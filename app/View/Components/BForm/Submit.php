<?php

namespace App\View\Components\BForm;

use Illuminate\View\Component;

class Submit extends Component
{
    public string $cclass;
    public array $atts;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(
        string $cclass = 'column',
        ?string $class = null
    ){
        $this->cclass = $cclass;
        $this->atts = [
            'class' => 'button',
            'type' => 'submit',
        ];
        if (!is_null($class))
        {
            $this->atts['class'] .= ' '.$class;
        }
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.b-form.submit');
    }
}
