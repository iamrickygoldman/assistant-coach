<?php

namespace App\View\Components\BForm;

use Illuminate\View\Component;

class Radio extends Component
{
    public static $count;

    public ?string $label;
    public string $name;
    public ?string $value;
    public array $options;
    public array $atts;
    public array $latts;
    public ?string $rclass;
    public string $cclass;
    public ?string $dclass;
    public bool $show_error;
    public ?string $description;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(
        string $name,
        ?string $label = null,
        ?string $value = null,
        array $options,
        bool $required = false,
        ?string $class = null,
        ?string $lclass = null,
        ?string $rclass = null,
        string $cclass = 'column',
        ?string $dclass = null,
        bool $star = true,
        bool $show_error = true,
        ?string $description = null
    ){
        $this->name = $name;
        $this->label = $label;
        if ($required && $star)
        {
            $this->label .= ' *';
        }
        $this->value = $value;
        $this->options = $options;
        $this->atts = [];
        $this->latts = [
            'class' => 'label',
        ];
        $this->rclass = 'radio';
        if (!is_null($class))
        {
            $this->atts['class'] = $class;
        }
        if (!is_null($lclass))
        {
            $this->latts['class'] .= ' '.$lclass;
        }
        if (!is_null($rclass))
        {
            $this->rclass .= ' '.$rclass;
        }
        if ($required)
        {
            $this->atts[] = 'required';
        }
        $this->cclass = $cclass;
        $this->dclass = $dclass;
        $this->show_error = $show_error;
        $this->description = $description;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.b-form.radio');
    }
}
