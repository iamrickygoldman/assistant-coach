<?php

namespace App\View\Components\BForm;

use Illuminate\View\Component;

class Description extends Component
{
    public ?string $class;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(?string $class = null)
    {
        $this->class = 'help';
        if (!is_null($class))
        {
            $this->class .= ' '.$class;
        }
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.b-form.description');
    }
}
