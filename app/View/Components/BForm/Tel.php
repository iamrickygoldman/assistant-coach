<?php

namespace App\View\Components\BForm;

use Illuminate\View\Component;
use App\View\Components\BForm\Text;

class Tel extends Text
{   
    protected function postProcess()
    {
        $this->type = 'tel';
    }
}
