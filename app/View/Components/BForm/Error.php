<?php

namespace App\View\Components\BForm;

use Illuminate\View\Component;

class Error extends Component
{
    public string $name;
    public string $class;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(string $name, string $class = 'column content error', string $size = 'small', string $padding = 'p-0', string $margin = 'mb-0', string $color = 'danger')
    {
        $this->name = $name;
        $this->class = $class .= ' is-'.$size . ' '.$padding . ' '.$margin . ' has-text-'.$color;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.b-form.error');
    }
}
