<?php

namespace App\View\Components\BForm;

use Illuminate\View\Component;

class FormOpen extends Component
{
    public ?string $url;
    public ?string $method;
    public ?string $class;
    public ?string $fid;
    public ?string $route;
    public ?string $action;
    public bool $files;
    public ?object $model;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(?string $url = null, ?string $method = null, ?string $class = null, ?string $fid = null, ?string $route = null, ?string $action = null, $files = false, ?object $model = null)
    {
        $this->url = $url;
        $this->method = $method;
        $this->class = $class;
        $this->fid = $fid;
        $this->route = $route;
        $this->action = $action;
        $this->files = $files;
        $this->model = $model;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.b-form.form-open');
    }
}
