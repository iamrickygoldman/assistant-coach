<?php

namespace App\View\Components\BForm;

use Illuminate\View\Component;

class Checkbox extends Component
{
    public ?string $label;
    public string $name;
    public ?string $fid;
    public string $value;
    public array $atts;
    public array $latts;
    public string $cclass;
    public ?string $dclass;
    public ?bool $checked;
    public bool $show_error;
    public ?string $description;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(
        string $name,
        ?string $fid = null,
        ?string $label = null,
        string $value = '1',
        bool $required = false,
        ?string $class = null,
        ?string $lclass = null,
        string $cclass = 'column',
        ?string $dclass = null,
        bool $star = true,
        ?bool $checked = null,
        bool $show_error = true,
        ?string $description = null
    ){
        $this->name = $name;
        $this->fid = is_null($fid) ? $name : $fid;
        $this->label = $label;
        if ($required && $star)
        {
            $this->label .= ' *';
        }
        $this->value = $value;
        $this->atts = [
            'id' => $this->fid,
        ];
        $this->latts = [
            'class' => 'checkbox',
        ];
        if (!is_null($class))
        {
            $this->atts['class'] = $class;
        }
        if (!is_null($lclass))
        {
            $this->latts['class'] .= ' '.$lclass;
        }
        if ($required)
        {
            $this->atts[] = 'required';
        }
        $this->cclass = $cclass;
        $this->dclass = $dclass;
        $this->checked = $checked;
        $this->show_error = $show_error;
        $this->description = $description;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.b-form.checkbox');
    }
}
