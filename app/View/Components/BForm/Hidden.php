<?php

namespace App\View\Components\BForm;

use Illuminate\View\Component;

class Hidden extends Component
{
    public string $name;
    public ?string $fid;
    public ?string $value;
    public array $atts;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(string $name, ?string $fid = null, ?string $value = null)
    {
        $this->name = $name;
        $this->fid = is_null($fid) ? $name : $fid;
        $this->atts = [
            'id' => $this->fid,
        ];
        $this->value = $value;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.b-form.hidden');
    }
}
