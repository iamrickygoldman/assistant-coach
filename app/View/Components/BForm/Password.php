<?php

namespace App\View\Components\BForm;

use Illuminate\View\Component;

class Password extends Component
{
    public string $name;
    public ?string $fid;
    public ?string $label;
    public array $atts;
    public array $latts;
    public string $cclass;
    public ?string $dclass;
    public bool $show_error;
    public ?string $description;
    
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(
        string $name,
        ?string $fid = null,
        ?string $label = null,
        bool $required = false,
        ?string $class = null,
        ?string $lclass = null,
        string $cclass = 'column',
        ?string $dclass = null,
        bool $star = true,
        bool $show_error = true,
        bool $new_password = false,
        bool $current_password = true,
        ?string $description = null
    ){
        $this->name = $name;
        $this->fid = is_null($fid) ? $name : $fid;
        $this->label = $label;
        if ($required && $star)
        {
            $this->label .= ' *';
        }
        $this->atts = [
            'class' => 'input',
            'id' => $this->fid,
        ];
        $this->latts = [
            'class' => 'label',
        ];
        if (!is_null($class))
        {
            $this->atts['class'] .= ' '.$class;
        }
        if (!is_null($lclass))
        {
            $this->latts['class'] .= ' '.$lclass;
        }
        if ($required)
        {
            $this->atts[] = 'required';
        }
        if ($current_password)
        {
            $this->atts['autocomplete'] = 'current-password';
        }
        if ($new_password)
        {
            $this->atts['autocomplete'] = 'new-password';
        }
        $this->cclass = $cclass;
        $this->dclass = $dclass;
        $this->show_error = $show_error;
        $this->description = $description;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.b-form.password');
    }
}
