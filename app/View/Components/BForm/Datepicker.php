<?php

namespace App\View\Components\BForm;

use Illuminate\View\Component;

class Datepicker extends Component
{
    public ?string $label;
    public string $name;
    public ?string $fid;
    public ?string $value;
    public array $atts;
    public array $latts;
    public string $cclass;
    public ?string $dclass;
    public bool $show_error;
    public string $format;
    public ?string $description;
    
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(
        string $name,
        ?string $fid = null,
        ?string $label = null,
        ?string $value = null,
        bool $required = false,
        ?string $class = null,
        ?string $lclass = null,
        string $cclass = 'column',
        ?string $dclass = null,
        bool $star = true,
        bool $show_error = true,
        ?string $placeholder = null,
        string $format = 'F j, Y',
        ?string $description = null
    ){
        $this->name = $name;
        $this->fid = is_null($fid) ? $name : $fid;
        $this->label = $label;
        if ($required && $star)
        {
            $this->label .= ' *';
        }
        $this->value = $value;
        $this->atts = [
            'class' => 'input',
            'id' => $this->fid,
        ];
        $this->latts = [
            'class' => 'label',
        ];
        if (!is_null($class))
        {
            $this->atts['class'] .= ' '.$class;
        }
        if (!is_null($lclass))
        {
            $this->latts['class'] .= ' '.$lclass;
        }
        if ($required)
        {
            $this->atts[] = 'required';
        }
        if (!is_null($placeholder))
        {
            $this->atts['placeholder'] = $placeholder;
        }
        $this->cclass = $cclass;
        $this->dclass = $dclass;
        $this->show_error = $show_error;
        $this->format = $format;
        $this->description = $description;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.b-form.datepicker');
    }
}
