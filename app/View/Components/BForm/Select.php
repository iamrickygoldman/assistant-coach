<?php

namespace App\View\Components\BForm;

use Illuminate\View\Component;

class Select extends Component
{
    public ?string $label;
    public string $name;
    public ?string $fid;
    public ?string $value;
    public array $options;
    public array $atts;
    public array $oatts;
    public array $ogatts;
    public array $latts;
    public string $cclass;
    public ?string $dclass;
    public string $wclass;
    public bool $show_error;
    public ?string $description;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(
        string $name,
        array $options,
        ?string $fid = null,
        ?string $label = null,
        ?string $value = null,
        bool $required = false,
        ?string $class = null,
        ?string $oclass = null,
        ?string $ogclass = null,
        ?string $lclass = null,
        string $cclass = 'column',
        ?string $dclass = null,
        string $wclass = 'select is-fullwidth',
        bool $star = true,
        bool $multiple = false,
        ?string $placeholder = null,
        bool $show_error = true,
        ?string $description = null
    ){
        $this->name = $name;
        $this->fid = is_null($fid) ? $name : $fid;
        $this->label = $label;
        if ($required && $star)
        {
            $this->label .= ' *';
        }
        $this->value = $value;
        $this->options = $options;
        $this->atts = [];
        $this->oatts = [];
        $this->ogatts = [];
        $this->latts = [
            'class' => 'label',
            'id' => $this->fid,
        ];
        if (!is_null($class))
        {
            $this->atts['class'] = ' '.$class;
        }
        if (!is_null($lclass))
        {
            $this->latts['class'] .= ' '.$lclass;
        }
        if ($required)
        {
            $this->atts[] = 'required';
        }
        if (!is_null($placeholder))
        {
            $this->atts['data-placeholder'] = $placeholder;
        }
        $this->cclass = $cclass;
        $this->dclass = $dclass;
        $this->wclass = $wclass;
        if ($multiple)
        {
            $this->atts[] = 'multiple';
            $this->wclass .= ' is-multiple';
        }
        $this->show_error = $show_error;
        $this->description = $description;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.b-form.select');
    }
}
