<?php

namespace App\View\Components\BForm;

use Illuminate\View\Component;

class Label extends Component
{
    public ?string $name;
    public ?string $text;
    public ?string $cclass;
    public array $atts;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(?string $name, ?string $text, bool $required = false, ?string $class = null, ?string $cclass = null, bool $star = true, string $margin = "mb-1")
    {
        $this->name = $name;
        $this->text = $text;
        if ($required && $star)
        {
            $this->text .= ' *';
        }
        $this->cclass = $cclass;
        $this->atts = [
            'class' => 'label' . ' '.$margin,
        ];
        if (!is_null($class))
        {
            $this->atts['class'] .= ' '.$class;
        }
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.b-form.label');
    }
}
