<?php

namespace App\Helpers;

use Illuminate\Http\Request;
use Auth;
use App\Models\User;
use App\Models\Role;

class Helper {

	public static function arrayWithoutKey(array $array, string $key): array
	{
		unset($array[$key]);
		return $array;
	}

	public static function autoversion(string $file, ?string $path = null): string
	{
		if (is_null($path))
		{
			return $file.'?'.filemtime(public_path($file));
		}
		return $file.'?'.filemtime($path.DIRECTORY_SEPARATOR.ltrim($file,'/'));
	}

	public static function convertHourToString(int $hour, bool $military = false, $separator = '', bool $leading_zero = false): string
	{
		$str = '';
		if ($military || $hour < 0 || $hour > 23)
		{
			return $leading_zero ? str_pad((string)$hour,2,'0',STR_PAD_LEFT) : (string)$hour;
		}
		if ($hour === 12)
		{
			$hour = $leading_zero ? str_pad($hour,2,'0',STR_PAD_LEFT) : $hour;
			return $hour.$separator.'pm';
		}
		if ($hour > 11)
		{
			$hour = $leading_zero ? str_pad($hour,2,'0',STR_PAD_LEFT) : $hour;
			return ($hour-12).$separator.'pm';
		}
		$hour = $leading_zero ? str_pad($hour,2,'0',STR_PAD_LEFT) : $hour;
		return $hour.$separator.'am';
	}

	public static function getAbsoluteRootUrl(?Request $request = null): string
	{
		if (!is_null($request))
		{
			return $request->getSchemeAndHttpHost();
		}
		return request()->getSchemeAndHttpHost();
	}

	public static function getIdFromGroup(string $group_name): int
	{
		$parts = explode('|',$group_name);
		if (count($parts) < 2)
		{
			return 0;
		}
		return (int)$parts[1];
	}

	public static function getNameFromGroup(string $group_name): string
	{
		return explode('|',$group_name)[0];
	}

	public static function getNotifications(): array
	{
		return [
			'success' => 'success',
			'info' => 'info',
			'warning' => 'warning',
			'fail' => 'danger',
		];
	}

	public static function getPrivateRoles(User $user): array
	{
		$roles = [];
        if ($user)
        {
            if ($user->hasRole(Role::SUPERADMIN_ID) || $user->hasRole(Role::ADMIN_ID))
            {
                $roles[] = 'admin';
            }
            if ($user->hasRole(Role::TRAINER_ID))
            {
                $roles[] = 'coach';
            }
            if ($user->hasRole(Role::STUDENT_ID))
            {
                $roles[] = 'student';
            }
        }
        return $roles;
	}
}