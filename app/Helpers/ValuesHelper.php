<?php

namespace App\Helpers;

use Auth;

class ValuesHelper {

	public static function getHoursOfDay(int $start = 0, int $stop = 23): array
	{
		$hours = [];
		for ($i = $start; $i <= $stop; $i++)
		{
			$hours[] = $i;
		}
		return $hours;
	}

	public static function getMaxDaysOfMonth(int $month, int $year): int
	{
		switch ($month)
		{
			case 1:
			case 3:
			case 5:
			case 7:
			case 8:
			case 10:
			case 12:
				return 31;
			case 2:
				return $year % 4 === 0 ? 29 : 28;
			case 4:
			case 6:
			case 9:
			case 11:
				return 30;
		}
		return 31;
	}

	public static function getTimeZoneOptions(?string $filepath = null): array
	{
		if (is_null($filepath))
		{
			$filepath = storage_path('app/lists/timezones.php');
		}
		require $filepath;
		return $timezones;
	}
}