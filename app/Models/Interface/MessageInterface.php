<?php

namespace App\Models\Interface;

use App\Models\User;

interface MessageInterface
{
    public function getDate(string $format): string;
    public function getRead(): bool;
    public function getSender(): ?User;
    public function getReceiver(): User;
    public function getSubject(): string;
    public function getBody(): string;
}
