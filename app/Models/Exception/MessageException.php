<?php

namespace App\Models\Exception;

class MessageException extends \Exception
{
     public const MESSAGE_PASSED_NOT_MESSAGE_CODE = 101;
     public const MESSAGE_PASSED_NOT_MESSAGE_MESSAGE = 'An object passed to the Messages array does not implement MessageInterface.';
}
