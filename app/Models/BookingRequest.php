<?php

namespace App\Models;

use App\Mail\BookingRequest as BookingRequestMail;
use App\Models\Interface\MessageInterface;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Mail;

class BookingRequest extends Model implements MessageInterface
{
    use HasFactory, SoftDeletes;

    public $timestamps = false;

    protected $fillable = [
        'start',
        'stop',
        'first_name',
        'last_name',
        'email',
        'phone',
        'notes',
        'camp_id',
        'trainer_id',
        'organization_id',
    ];

    protected $casts = [
        'start' => 'datetime',
        'stop' => 'datetime',
    ];


    // RELATIONSHIPS

    public function camp()
    {
        return $this->belongsTo(Camp::class);
    }

    public function organization()
    {
        return $this->belongsTo(Organization::class);
    }

    public function trainer()
    {
        return $this->belongsTo(Trainer::class);
    }

    // ACCESSORS

    public function getFullNameAttribute(): string
    {
        $name = $this->first_name;
        if ($this->last_name)
        {
            $name.= ' '.$this->last_name;
        }
        return $name;
    }

    // TOTEST
    public function getUserAttribute()
    {
        if ($this->trainer)
        {
            return $this->trainer->user;
        }
        if ($this->organization)
        {
            return $this->organization->user;
        }
        return null;
    }

    // MessageInterface

    public function getDate(string $format = 'n/j/Y') : string
    {
        return '3/25/2020';
    }

    public function getRead(): bool
    {
        return false;
    }

    public function getSender(): ?User
    {
        return null;
    }

    public function getReceiver(): User
    {
        return $this->user;
    }

    public function getSubject(): string
    {
        return __('booking.request_booking');
    }

    public function getBody(): string
    {
        return 'body';
    }

    // MISC

    public function send(string $email, bool $fake = false): BookingRequestMail
    {
        $mail = new BookingRequestMail($this);
        if ($fake)
        {
            Mail::fake();
        }
        Mail::to($email)->send($mail);
        return $mail;
    }
}
