<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Organization extends Model
{
    use HasFactory, SoftDeletes;
    
    public $timestamps = true;

    protected $fillable = [
        'address1',
        'address2',
        'city',
        'country',
        'email',
        'name',
        'phone',
        'state',
        'user_id',
        'zip',
    ];

    protected $casts = [
        'deleted_at' => 'datetime',
    ];

    // RELATIONSHIPS

    public function fields()
    {
        return $this->hasMany(Field::class);
    }

    public function students()
    {
        return $this->hasMany(Student::class);
    }

    public function trainers()
    {
        return $this->hasMany(Trainer::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    // ACCESSORS

    public function getStudentsCountAttribute()
    {
        $count = 0;
        foreach ($this->students as $student)
        {
            $count++;
        }
        foreach ($this->trainers as $trainer)
        {
            foreach ($trainer->students as $student)
            {
                $count++;
            }
        }
        return $count;
    }

    public function getTrainersCountAttribute()
    {
        $count = 0;
        foreach ($this->trainers as $trainer)
        {
            $count++;
        }
        return $count;
    }

    // MISC

    public function hasStudentUser(User $user): bool
    {
        if (is_null($user->students))
        {
            return false;
        }
        foreach ($user->students as $student)
        {
            if ($student->organization_id === $this->id)
            {
                return true;
            }
        }
        return false;
    }

    public function hasTrainerUser(User $user): bool
    {
        if (is_null($user->trainers))
        {
            return false;
        }
        if ($user->id === $this->user_id)
        {
            return true;
        }
        foreach ($user->trainers as $trainer)
        {
            if ($trainer->organization_id === $this->id)
            {
                return true;
            }
        }
        return false;
    }
}
