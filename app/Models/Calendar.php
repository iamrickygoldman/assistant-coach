<?php

namespace App\Models;

class Calendar
{
    public ?Organization $organization;

    public ?Trainer $trainer;

    public function __construct(?Organization $organization = null, ?Trainer $trainer = null)
    {
        $this->organization = $organization;
        $this->trainer = $trainer;
    }

    // ACCESSORS

    // nt
    /**
     * 
     * @return array of allowed schedule intervals in minutes
     * 
    **/
    public function getSchduleIntervals(): array
    {
        return ['30, 60'];
    }

    // nt
    /**
     * 
     * @return object
     * 
     * @start       string military start time for day
     * @stop        string military stop time for day
     * @intervals   array of allowed schedule intervals in minutes
     * @breaks      array of disabled start/stop time ranges
     * @booked      array of booked start/stop time ranges
     * 
    **/
    public function getDaySchedule($date): object
    {
        $range = new \stdClass;
        $range->start = '08:00';
        $range->stop = '17:00';
        $range->intervals = $this->getSchduleIntervals();
        $range->breaks = [];
        $range->booked = [];

        return $range;
    }

    // STATIC

    public static function getDaysLong(bool $locale = true, $indexed = false) : array
    {
        $keys = [
            'sunday',
            'monday',
            'tuesday',
            'wednesday',
            'thursday',
            'friday',
            'saturday'
        ];

        if ($locale)
        {
            $new = [];
            foreach ($keys as &$key)
            {
                if ($indexed)
                {
                    $new[] = __('calendar.'.$key);
                }
                else
                {
                    $new[$key] = __('calendar.'.$key);
                }
            }
            return $new;
        }

        return $keys;
    }

    public static function getDaysShort(bool $locale = true) : array
    {
        $keys = [
            'sun',
            'mon',
            'tue',
            'wed',
            'thu',
            'fri',
            'sat'
        ];
        if ($locale)
        {
            $new = [];
            foreach ($keys as &$key)
            {
                $new[$key] = __('calendar.'.$key);
            }
            return $new;
        }
        return $keys;
    }

    public static function getIntervals(bool $locale = true) : array
    {
        $keys = [
            'hour',
            'day',
            'week',
            'month',
            'year',
            'date'
        ];
        if ($locale)
        {
            $new = [];
            foreach ($keys as &$key)
            {
                $new[$key] = __('calendar.'.$key);
            }
            return $new;
        }
        return $keys;
    }

    public static function getLocale(): array
    {
        $days_long = self::getDaysLong();
        $days_short = self::getDaysShort();
        $intervals = self::getIntervals();
        $months_long = self::getMonthsLong();
        $months_short = self::getMonthsShort();
        $days = [
            'long' => $days_long,
            'short' => $days_short
        ];
        $months = [
            'long' => $months_long,
            'short' => $months_short
        ];
        $booking = [
            'bookingSuccess' => __('booking.request_success'),
            'bookingError' => __('booking.request_error'),
        ];
        return [
            'days' => $days,
            'intervals' => $intervals,
            'months' => $months,
            'booking' => $booking
        ];
    }

    public static function getMonthsLong(bool $locale = true) : array
    {
        $keys = [
            'january',
            'february',
            'march',
            'april',
            'may',
            'june',
            'july',
            'august',
            'september',
            'october',
            'november',
            'december'
        ];
        if ($locale)
        {
            $new = [];
            foreach ($keys as &$key)
            {
                $new[$key] = __('calendar.'.$key);
            }
            return $new;
        }
        return $keys;
    }

    public static function getMonthsShort(bool $locale = true) : array
    {
        $keys = [
            'jan',
            'feb',
            'mar',
            'apr',
            'may',
            'jun',
            'jul',
            'aug',
            'sep',
            'oct',
            'nov',
            'dec'
        ];
        if ($locale)
        {
            $new = [];
            foreach ($keys as &$key)
            {
                $new[$key] = __('calendar.'.$key);
            }
            return $new;
        }
        return $keys;
    }
}
