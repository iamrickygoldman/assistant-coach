<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{
    use HasFactory;

    protected $fillable = [
        'start',
        'stop',
        'camp_id',
        'trainer_id',
        'organization_id',
        'student_id',
    ];

    protected $casts = [
        'start' => 'datetime',
        'stop' => 'datetime',
    ];


    // RELATIONSHIPS

    public function camp()
    {
        return $this->belongsTo(Camp::class);
    }

    public function organization()
    {
        return $this->belongsTo(Organization::class);
    }

    public function student()
    {
        return $this->belongsTo(Student::class);
    }

    public function trainer()
    {
        return $this->belongsTo(Trainer::class);
    }
}
