<?php

namespace App\Models;

use App\Models\Exception\MessageException;
use App\Models\Interface\MessageInterface;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;
use Auth;

class User extends Authenticatable implements MustVerifyEmail
{
    use HasFactory, Notifiable, SoftDeletes;

    const ACCOUNT_STATUS_NONE = 1;
    const ACCOUNT_STATUS_DISABLED = 2;

    public $timestamps = true;

    protected $fillable = [
        'account_status',
        'address1',
        'address2',
        'city',
        'country',
        'email',
        'first_name',
        'language',
        'last_name',
        'password',
        'phone',
        'state',
        'timezone',
        'zip',
    ];

    protected $hidden = [
        'password',
        'remember_token',
    ];

    protected $casts = [
        'deleted_at' => 'datetime',
        'email_verified_at' => 'datetime',
    ];

    // RELATIONSHIPS

    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }

    public function students()
    {
        return $this->hasMany(Student::class);
    }

    public function trainers()
    {
        return $this->hasMany(Trainer::class);
    }

    public function organizations()
    {
        return $this->hasMany(Trainer::class);
    }

    // ACCESSORS

    public function getFullNameAttribute()
    {
        $full_name = $this->first_name;
        if (!is_null($this->last_name))
        {
            $full_name .= ' '.$this->last_name;
        }
        return $full_name;
    }

    public function getOrganizationMembershipsAttribute()
    {
        $organizations = [];
        foreach ($this->trainers as $trainer)
        {
            if ($trainer->organization)
            {
                $organizations[] = $trainer->organization;
            }
        }
        return $organizations;
    }

    // TOTEST
    public function getBookingRequestsAttribute()
    {
        $requests = [];
        foreach ($this->trainers as $trainer)
        {
            foreach ($trainer->booking_requests as $request)
            {
                $requests[] = $request;
            }
        }
        foreach ($this->organizations as $organization)
        {
            foreach ($organization->booking_requests as $request)
            {
                $requests[] = $request;
            }
        }
        return $requests;
    }

    // TOTEST
    public function getMessagesAttribute()
    {
        $messages = $this->getBookingRequestsAttribute();
        foreach ($messages as $message)
        {
            if (!$message instanceof MessageInterface)
            {
                throw new MessageException(MessageException::MESSAGE_PASSED_NOT_MESSAGE_MESSAGE,MessageException::MESSAGE_PASSED_NOT_MESSAGE_CODE);
            }
        }
        return $messages;
    }

    // TOTEST
    public function getUnreadMessagesAttribute()
    {
        $messages = $this->getMessagesAttribute();
        foreach ($messages as $key => $message)
        {
            if ($message->getRead())
            {
                unset($messages[$key]);
            }
        }
        return $messages;
    }

    // MISC

    public function hasRole(int $role_id): bool
    {
        return $this->roles->contains($role_id);
    }

    // STATIC

    public static function currentUser(Request $request): ?User
    {
        $user = Auth::user();

        if ($user && ($user->hasRole(Role::SUPERADMIN_ID) || $user->hasRole(Role::ADMIN_ID)))
        {
            $validated_data = $request->validate([
                'puser_id' => 'nullable|integer',
            ]);
            if (isset($validated_data['puser_id']))
            {
                $request->session()->put('puser_id',$validated_data['puser_id']);
            }
            if ($request->session()->get('puser_id',0) > 0)
            {
                $puser = User::find($request->session()->get('puser_id'));
                if ($puser)
                {
                    return $puser;
                }
            }
        }
        return $user;
    }
}
