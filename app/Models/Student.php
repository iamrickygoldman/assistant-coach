<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Carbon\Carbon;

use App\Mail\InviteStudent;

class Student extends Model
{
    use HasFactory, SoftDeletes;

    public $timestamps = true;

    protected $fillable = [
        'birth_date',
        'birth_day',
        'birth_month',
        'birth_year',
        'field_data',
        'nickname',
        'organization_id',
        'trainer_id',
        'user_id',
    ];

    protected $casts = [
        'deleted_at' => 'datetime',
    ];

    // RELATIONSHIPS

    public function fields()
    {
        return $this->belongsToMany(Field::class)->withPivot('num_val','text_val','dt_val');
    }

    public function organization()
    {
        return $this->belongsTo(Organization::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function trainer()
    {
    	return $this->belongsTo(Trainer::class);
    }

    public function bookings()
    {
        return $this->hasMany(BookingType::class);
    }

    // ACCESSORS

    public function getAgeAttribute()
    {
        if (is_null($this->birth_day) || is_null($this->birth_month) || is_null($this->birth_year))
        {
            return '';
        }
        return Carbon::createFromDate($this->birth_year,$this->birth_month,$this->birth_day)->age;
    }

    public function getBirthDateAttribute()
    {
        if (is_null($this->birth_day) || is_null($this->birth_month) || is_null($this->birth_year))
        {
            return '';
        }
        $bd = Carbon::createFromDate($this->birth_year,$this->birth_month,$this->birth_day);
        return $bd->format('Y-m-d');
    }

    public function getBirthDateDisplayAttribute()
    {
        if (is_null($this->birth_day) || is_null($this->birth_month) || is_null($this->birth_year))
        {
            return '';
        }
        $bd = Carbon::createFromDate($this->birth_year,$this->birth_month,$this->birth_day);
        return $bd->format('M j, Y');
    }

    public function getFieldDataAttribute()
    {
        $data = [];
        foreach ($this->fields as $field)
        {
            if (!isset($data[$field->type]))
            {
                $data[$field->type] = [];
            }
            switch ($field->type)
            {
                case Field::TYPE_DATE:
                    $data[$field->type][$field->id] = Carbon::createFromFormat('Y-m-d H:i:s',$field->pivot->dt_val);
                    break;
                case Field::TYPE_BOOLEAN:
                    $tmp = (int)$field->pivot->num_val;
                    $data[$field->type][$field->id] = $tmp === 1;
                    break;
                case Field::TYPE_NUMBER:
                    $data[$field->type][$field->id] = (float)$field->pivot->num_val;
                    break;
                case Field::TYPE_LIST:
                case Field::TYPE_STRING:
                case Field::TYPE_TEXT:
                default:
                    $data[$field->type][$field->id] = $field->pivot->text_val;
            }
        }
        return $data;
    }

    public function getFieldListAttribute()
    {
        if (!is_null($this->trainer_id))
        {
            return $this->trainer->fields;
        }
        else if (!is_null($this->organization_id))
        {
            return $this->organization->fields;
        }
        return [];
    }

    public function getNameAttribute()
    {
        if ($this->nickname)
        {
            return $this->nickname;
        }
        if (!is_null($this->user))
        {
            return $this->user->full_name;
        }
        return '[No Account or Nickname]';
    }

    // MUTATORS

    public function setBirthDateAttribute($value)
    {
        $bd = Carbon::createFromFormat('Y-m-d',$value);
        $this->attributes['birth_day'] = $bd->day;
        $this->attributes['birth_month'] = $bd->month;
        $this->attributes['birth_year'] = $bd->year;
        unset($this->attributes['birth_date']);
    }

    public function setFieldDataAttribute($value)
    {
        if (is_array($value))
        {
            $num_ids = [];
            $text_ids = [];
            $dt_ids = [];
            $num_vals = [];
            $text_vals = [];
            $dt_vals = [];

            foreach ($value as $type => $fields)
            {
                foreach ($fields as $id => $val)
                {
                    if (is_null($val))
                    {
                        continue;
                    }
                    switch ($type)
                    {
                        case Field::TYPE_DATE:
                            $dt_ids[] = $id;
                            $dt_vals[] = $val;
                            break;
                        case Field::TYPE_BOOLEAN:
                        case Field::TYPE_NUMBER:
                            $num_ids[] = $id;
                            $num_vals[] = $val;
                            break;
                        case Field::TYPE_LIST:
                        case Field::TYPE_STRING:
                        case Field::TYPE_TEXT:
                        default:
                            $text_ids[] = $id;
                            $text_vals[] = $val;
                    }
                }
            }

            $sync = [];
            for ($i = 0; $i < count($num_ids); $i++)
            {
                $sync[$num_ids[$i]] = ['num_val' => $num_vals[$i]];
            }
            for ($i = 0; $i < count($text_ids); $i++)
            {
                $sync[$text_ids[$i]] = ['text_val' => $text_vals[$i]];
            }
            for ($i = 0; $i < count($dt_ids); $i++)
            {
                $sync[$dt_ids[$i]] = ['dt_val' => $dt_vals[$i]];
            }

            $this->fields()->sync($sync);
        }
    }

    // MISC

    public function generateInviteKey(): string
    {
        $key = Hash::make(time().$this->id);
        $this->invite_key = $key;
        $this->save();
        return $key;
    }

    public function hasTrainerUser(User $user): bool
    {
        if (is_null($user->trainers))
        {
            return false;
        }
        foreach ($user->trainers as $trainer)
        {
            if ($trainer->id === $this->trainer_id)
            {
                return true;
            }
            if (!is_null($this->organization_id) && $trainer->organization_id === $this->organization_id)
            {
                return true;
            }
        }
        return false;
    }

    public function sendInvite(string $name, string $email, bool $fake = false): InviteStudent
    {
        $key = $this->generateInviteKey();
        $mail = new InviteStudent($key,$name);
        if ($fake)
        {
            Mail::fake();
        }
        Mail::to($email)->send($mail);
        return $mail;
    }
}
