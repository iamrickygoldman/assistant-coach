<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Role extends Model
{
    use SoftDeletes;

    const SUPERADMIN_ID = 1;
    const ADMIN_ID = 2;
    const TRAINER_ID = 3;
    const STUDENT_ID = 4;

    public $timestamps = true;

    protected $fillable = [
    	'description',
        'title',
    ];

    protected $casts = [
    	'deleted_at' => 'datetime'
    ];

    // RELATIONSHIPS

    public function users()
    {
        return $this->belongsToMany(User::class);
    }
}
