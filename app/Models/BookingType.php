<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Casts\Money;

class BookingType extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'duration',
        'price',
        'description',
        'camp_id',
        'trainer_id',
        'organization_id',
    ];

    protected $casts = [
        'price' => Money::class,
        'deleted_at' => 'datetime',
    ];


    // RELATIONSHIPS

    public function camp()
    {
        return $this->belongsTo(Camp::class);
    }

    public function organization()
    {
        return $this->belongsTo(Organization::class);
    }

    public function trainer()
    {
        return $this->belongsTo(Trainer::class);
    }
}
