<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Casts\FieldOptions;

class Field extends Model
{
    use HasFactory;

    const TYPE_BOOLEAN = 'boolean';
    const TYPE_DATE = 'date';
    const TYPE_LIST = 'list';
    const TYPE_NUMBER = 'number';
    const TYPE_STRING = 'string';
    const TYPE_TEXT = 'text';
    
    public $timestamps = true;

    protected $fillable = [
        'display_students_list',
        'options',
        'organization_id',
        'title',
        'trainer_id',
        'type',
    ];

    protected $casts = [
        'display_students_list' => 'boolean',
        'options' => FieldOptions::class,
    ];

    // RELATIONSHIPS

    public function user()
    {
        return $this->belongsTo(Trainer::class);
    }

    public function organization()
    {
        return $this->belongsTo(Organization::class);
    }

    public function trainer()
    {
    	return $this->belongsTo(Trainer::class);
    }

    // ACCESSORS

    public function getInputNameAttribute()
    {
        return 'field_data['.$this->type.']['.$this->id.']';
    }

    public function getInputOptionsAttribute()
    {
        switch ($this->type)
        {
            case self::TYPE_BOOLEAN:
                return [1 => 'Yes',0 => 'No'];
            case self::TYPE_LIST:
                if (is_array($this->options) && isset($this->options['list']))
                {
                    return $this->options['list'];
                }
        }
        return [];
    }

    public function getInputTypeAttribute()
    {
        switch ($this->type)
        {
            case self::TYPE_BOOLEAN:
                return 'b-form.radio';
            case self::TYPE_DATE:
                return 'b-form.datepicker';
            case self::TYPE_LIST:
                return 'b-form.select';
            case self::TYPE_NUMBER:
                return 'b-form.text';
            case self::TYPE_STRING:
                return 'b-form.text';
            case self::TYPE_TEXT:
                return 'b-form.textarea';
            default:
                return 'b-form-text';
        }
    }

    public function getOptionsListTextAttribute()
    {
        if (is_array($this->options) && isset($this->options['list']))
        {
            return implode(PHP_EOL,$this->options['list']);
        }
        return '';
    }

    public function getTypeTextAttribute()
    {
        if (isset(self::getTypeOptions()[$this->type]))
        {
            return self::getTypeOptions()[$this->type];
        }
        return '';
    }

    // MUTATORS

    public function setOptionsAttribute($value)
    {
        foreach ($value as $k => $v)
        {
            if (is_null($v))
            {
                unset($value[$k]);
            }
        }
        if (empty($value))
        {
            $this->attributes['options'] = null;
        }
        else
        {
            $this->attributes['options'] = json_encode((object)$value);
        }
    }

    // MISC
    
    public function hasTrainerUser(User $user): bool
    {
        if (is_null($user->trainers))
        {
            return false;
        }
        foreach ($user->trainers as $trainer)
        {
            if ($trainer->id === $this->trainer_id)
            {
                return true;
            }
            if (!is_null($this->organization_id) && $trainer->organization_id === $this->organization_id)
            {
                return true;
            }
        }
        return false;
    }

    // STATIC

    public static function getTypeOptions($placeholder = false): array
    {
        $types = [
            self::TYPE_STRING => 'Short Text',
            self::TYPE_TEXT => 'Long Text',
            self::TYPE_LIST => 'Pick List',
            self::TYPE_NUMBER => 'Number',
            self::TYPE_DATE => 'Date',
            self::TYPE_BOOLEAN => 'Yes/No',
        ];
        if ($placeholder)
        {
            $types = ['' => 'Select Type'] + $types;
        }
        return $types;
    }
}
