<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Trainer extends Model
{
    use HasFactory, SoftDeletes;
    
    public $timestamps = true;

    protected $fillable = [
    	'nickname',
        'organization_id',
        'user_id',
    ];

    protected $casts = [
        'deleted_at' => 'datetime',
    ];

    // RELATIONSHIPS

    public function fields()
    {
        return $this->hasMany(Field::class);
    }

    public function organization()
    {
    	return $this->belongsTo(Organization::class);
    }

    public function students()
    {
        return $this->hasMany(Student::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function bookings()
    {
        return $this->hasMany(BookingType::class);
    }

    public function booking_types()
    {
        return $this->hasMany(BookingType::class);
    }

    public function booking_requests()
    {
        return $this->hasMany(BookingRequest::class);
    }

    // ACCESSORS

    public function getNameAttribute()
    {
        if ($this->nickname)
        {
            return $this->nickname;
        }
        return $this->user->full_name;
    }

    public function getMessagesAttribute()
    {
        return $this->booking_requests();
    }

    // STATIC

    public static function getFieldGroups(User $user): array
    {
        $field_groups = [];
        foreach ($user->trainers as $trainer)
        {
            if (is_null($trainer->organization_id))
            {
                $group_name = 'personal|'.$trainer->id;
                if (!isset($field_groups[$group_name]))
                {
                    $field_groups[$group_name] = [];
                }
                foreach ($trainer->fields as $field)
                {
                    $field_groups[$group_name][] = $field;
                }
            }
        }
        foreach ($user->organization_memberships as $organization)
        {
            $group_name = $organization->name . '|'.$organization->id;
            $field_groups[$group_name] = [];
            foreach ($organization->fields as $field)
            {
                $field_groups[$group_name][] = $field;
            }
        }
        return $field_groups;
    }

    public static function getStudentGroups(User $user): array
    {
        $student_groups = [];
        foreach ($user->trainers as $trainer)
        {
            if (is_null($trainer->organization_id))
            {
                $group_name = 'personal|'.$trainer->id;
                if (!isset($student_groups[$group_name]))
                {
                    $student_groups[$group_name] = [];
                }
                foreach ($trainer->students as $student)
                {
                    $student_groups[$group_name][] = $student;
                }
            }
        }
        foreach ($user->organization_memberships as $organization)
        {
            $group_name = $organization->name . '|'.$organization->id;
            $student_groups[$group_name] = [];
            foreach ($organization->students as $student)
            {
                $student_groups[$group_name][] = $student;
            }
        }
        return $student_groups;
    }

    // MISC

    public function generateInviteKey(): string
    {
        $key = Hash::make(time().$this->id);
        $this->invite_key = $key;
        $this->save();
        return $key;
    }
}
