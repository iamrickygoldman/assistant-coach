<?php

namespace App\Providers;

use Illuminate\Cache\RateLimiting\Limit;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\RateLimiter;
use Illuminate\Support\Facades\Route;

use App\Helpers\MultisitesHelper;
use App\Models\Role;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * The path to the "home" route for your application.
     *
     * This is used by Laravel authentication to redirect users after login.
     *
     * @var string
     */
    public const HOME = '/';

    public const LOGGED_IN = '/login/route';

    /**
     * The controller namespace for the application.
     *
     * When present, controller route declarations will automatically be prefixed with this namespace.
     *
     * @var string|null
     */
    // protected $namespace = 'App\\Http\\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        $this->configureRateLimiting();

        $this->routes(function () {
            Route::prefix('api/general')
                ->name('api.')
                ->middleware('api')
                ->namespace($this->namespace)
                ->group(base_path('routes/api/general.php'));

            Route::prefix('api/calendar')
                ->name('api.calendar.')
                ->middleware('api')
                ->namespace($this->namespace)
                ->group(base_path('routes/api/calendar.php'));

            Route::middleware('web')
                ->namespace($this->namespace)
                ->group(base_path('routes/web/all.php'));

            Route::prefix('admin')
                ->name('admin.')
                ->middleware(['web','has_role:'.Role::SUPERADMIN_ID.','.Role::ADMIN_ID])
                ->namespace($this->namespace)
                ->group(base_path('routes/web/admin.php'));

            Route::prefix('coach')
                ->name('coach.')
                ->middleware(['web','has_role:'.Role::TRAINER_ID])
                ->namespace($this->namespace)
                ->group(base_path('routes/web/coach.php'));

            Route::prefix('student')
                ->name('student.')
                ->middleware(['web','has_role:'.Role::STUDENT_ID])
                ->namespace($this->namespace)
                ->group(base_path('routes/web/student.php'));
        });
    }

    /**
     * Configure the rate limiters for the application.
     *
     * @return void
     */
    protected function configureRateLimiting()
    {
        RateLimiter::for('api', function (Request $request) {
            return Limit::perMinute(6000)->by(optional($request->user())->id ?: $request->ip());
        });
    }
}
