const gulp = require('gulp');
const concat = require('gulp-concat');
const sass = require('gulp-sass')(require('sass'));
const sourcemaps = require('gulp-sourcemaps');
const terser = require('gulp-terser');

/*** CONCAT ***/

function concatAdminCSS(cb) {
	gulp.src(['./public/css/admin.css'])
		.pipe(concat('admin-combined.min.css'))
		.pipe(gulp.dest('./public/css'));
	cb();
};

function concatCalendarCSS(cb) {
	gulp.src(['./public/css/calendar.css'])
		.pipe(concat('calendar-combined.min.css'))
		.pipe(gulp.dest('./public/css'));
	cb();
};

function concatCoachCSS(cb) {
	gulp.src(['./public/css/coach.css'])
		.pipe(concat('coach-combined.min.css'))
		.pipe(gulp.dest('./public/css'));
	cb();
};

function concatPublicCSS(cb) {
	gulp.src(['./public/css/public.css'])
		.pipe(concat('public-combined.min.css'))
		.pipe(gulp.dest('./public/css'));
	cb();
};

function concatPrivateCSS(cb) {
	gulp.src(['./public/css/private.css'])
		.pipe(concat('private-combined.min.css'))
		.pipe(gulp.dest('./public/css'));
	cb();
};

function concatStudentCSS(cb) {
	gulp.src(['./public/css/student.css'])
		.pipe(concat('student-combined.min.css'))
		.pipe(gulp.dest('./public/css'));
	cb();
};

function concatAdminJS(cb) {
	return gulp.src([
			'./resources/js/bulma-scripts.js',
			'./resources/js/all.js',
			'./resources/js/admin.js'
		])
		.pipe(concat('admin-combined.js'))
		.pipe(gulp.dest('./public/js/concat'));
};

function concatCalendarJS(cb) {
	return gulp.src([
			'./resources/js/calendar/src/helper.js',
			'./resources/js/calendar/src/localize.js',
			'./resources/js/calendar/src/form.js',
			'./resources/js/calendar/src/structure.js',
			'./resources/js/calendar/src/calendar.js'
		])
		.pipe(concat('calendar-combined.js'))
		.pipe(gulp.dest('./public/js/concat'));
};

function concatCoachJS(cb) {
	return gulp.src([
			'./resources/js/bulma-scripts.js',
			'./resources/js/all.js',
			'./resources/js/coach.js'
		])
		.pipe(concat('coach-combined.js'))
		.pipe(gulp.dest('./public/js/concat'));
};

function concatPublicJS(cb) {
	return gulp.src([
			'./resources/js/bulma-scripts.js',
			'./resources/js/all.js',
			'./resources/js/public.js'
		])
		.pipe(concat('public-combined.js'))
		.pipe(gulp.dest('./public/js/concat'));
};

function concatPrivateJS(cb) {
	return gulp.src([
			'./resources/js/bulma-scripts.js',
			'./resources/js/all.js',
			'./resources/js/private.js'
		])
		.pipe(concat('private-combined.js'))
		.pipe(gulp.dest('./public/js/concat'));
};

function concatStudentJS(cb) {
	return gulp.src([
			'./resources/js/bulma-scripts.js',
			'./resources/js/all.js',
			'./resources/js/student.js'
		])
		.pipe(concat('student-combined.js'))
		.pipe(gulp.dest('./public/js/concat'));
};

/*** CONCAT ***/

/*** SASS ***/

function sassAdmin(cb) {
	gulp.src('./resources/scss/admin/admin.scss')
		.pipe(sourcemaps.init())
    	.pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
		.pipe(sourcemaps.write())
		.pipe(gulp.dest('./public/css'));
	cb();
};

function sassCalendar(cb) {
	gulp.src('./resources/scss/calendar/calendar.scss')
		.pipe(sourcemaps.init())
    	.pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
		.pipe(sourcemaps.write())
		.pipe(gulp.dest('./public/css'));
	cb();
};

function sassCoach(cb) {
	gulp.src('./resources/scss/coach/coach.scss')
		.pipe(sourcemaps.init())
    	.pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
		.pipe(sourcemaps.write())
		.pipe(gulp.dest('./public/css'));
	cb();
};

function sassPublic(cb) {
	gulp.src('./resources/scss/public/public.scss')
		.pipe(sourcemaps.init())
    	.pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
		.pipe(sourcemaps.write())
		.pipe(gulp.dest('./public/css'));
	cb();
};

function sassPrivate(cb) {
	gulp.src('./resources/scss/private/private.scss')
		.pipe(sourcemaps.init())
    	.pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
		.pipe(sourcemaps.write())
		.pipe(gulp.dest('./public/css'));
	cb();
};

function sassStudent(cb) {
	gulp.src('./resources/scss/student/student.scss')
		.pipe(sourcemaps.init())
    	.pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
		.pipe(sourcemaps.write())
		.pipe(gulp.dest('./public/css'));
	cb();
};

/*** SASS ***/

/*** TERSER ***/

function terserAdmin(cb) {
	return gulp.src('./public/js/concat/admin-combined.js')
		.pipe(sourcemaps.init())
		.pipe(terser({
			mangle: false
		}))
		.pipe(sourcemaps.write())
		.pipe(gulp.dest('./public/js'));
}

function terserCalendar(cb) {
	return gulp.src('./public/js/concat/calendar-combined.js')
		.pipe(sourcemaps.init())
		.pipe(terser({
			mangle: false
		}))
		.pipe(sourcemaps.write())
		.pipe(gulp.dest('./public/js'));
}

function terserCoach(cb) {
	return gulp.src('./public/js/concat/coach-combined.js')
		.pipe(sourcemaps.init())
		.pipe(terser({
			mangle: false
		}))
		.pipe(sourcemaps.write())
		.pipe(gulp.dest('./public/js'));
}

function terserPublic(cb) {
	return gulp.src('./public/js/concat/public-combined.js')
		.pipe(sourcemaps.init())
		.pipe(terser({
			mangle: false
		}))
		.pipe(sourcemaps.write())
		.pipe(gulp.dest('./public/js'));
}

function terserPrivate(cb) {
	return gulp.src('./public/js/concat/private-combined.js')
		.pipe(sourcemaps.init())
		.pipe(terser({
			mangle: false
		}))
		.pipe(sourcemaps.write())
		.pipe(gulp.dest('./public/js'));
}

function terserStudent(cb) {
	return gulp.src('./public/js/concat/student-combined.js')
		.pipe(sourcemaps.init())
		.pipe(terser({
			mangle: false
		}))
		.pipe(sourcemaps.write())
		.pipe(gulp.dest('./public/js'));
}

/*** TERSER ***/

/*** WATCH ***/

function watch(cb) {
	gulp.watch([
			'./resources/scss/admin/*.scss',
			'./resources/scss/form.scss'
		],
		gulp.series(sassAdmin,concatAdminCSS));

	gulp.watch([
			'./resources/scss/calendar/*.scss',
			'./resources/scss/form.scss'
		],
		gulp.series(sassCalendar,concatCalendarCSS));

	gulp.watch([
			'./resources/scss/coach/*.scss',
			'./resources/scss/form.scss'
		],
		gulp.series(sassCoach,concatCoachCSS));

	gulp.watch([
			'./resources/scss/public/*.scss',
			'./resources/scss/form.scss'
		],
		gulp.series(sassPublic,concatPublicCSS));

	gulp.watch([
			'./resources/scss/private/*.scss',
			'./resources/scss/form.scss'
		],
		gulp.series(sassPrivate,concatPrivateCSS));

	gulp.watch([
			'./resources/scss/student/*.scss',
			'./resources/scss/form.scss'
		],
		gulp.series(sassStudent,concatStudentCSS));

	gulp.watch([
			'./resources/js/bulma-scripts.js',
			'./resources/js/all.js',
			'./resources/js/admin.js'
		],
		gulp.series(concatAdminJS,terserAdmin));

	gulp.watch([
			'./resources/js/calendar/src/*.js'
		],
		gulp.series(concatCalendarJS,terserCalendar));

	gulp.watch([
			'./resources/js/bulma-scripts.js',
			'./resources/js/all.js',
			'./resources/js/coach.js'
		],
		gulp.series(concatCoachJS,terserCoach));

	gulp.watch([
			'./resources/js/bulma-scripts.js',
			'./resources/js/all.js',
			'./resources/js/public.js'
		],
		gulp.series(concatPublicJS,terserPublic));

	gulp.watch([
			'./resources/js/bulma-scripts.js',
			'./resources/js/all.js',
			'./resources/js/private.js'
		],
		gulp.series(concatPrivateJS,terserPrivate));

	gulp.watch([
			'./resources/js/bulma-scripts.js',
			'./resources/js/all.js',
			'./resources/js/student.js'
		],
		gulp.series(concatStudentJS,terserStudent));

};

/*** WATCH ***/

/*** EXPORTS ***/

exports.concatAdminCSS = concatAdminCSS;
exports.concatCalendarCSS = concatCalendarCSS;
exports.concatCoachCSS = concatCoachCSS;
exports.concatPublicCSS = concatPublicCSS;
exports.concatPrivateCSS = concatPrivateCSS;
exports.concatStudentCSS = concatStudentCSS;

exports.concatAdminJS = concatAdminJS;
exports.concatCalendarJS = concatCalendarJS;
exports.concatCoachJS = concatCoachJS;
exports.concatPublicJS = concatPublicJS;
exports.concatPrivateJS = concatPrivateJS;
exports.concatStudentJS = concatStudentJS;

exports.sassAdmin = sassAdmin;
exports.sassCalendar = sassCalendar;
exports.sassCoach = sassCoach;
exports.sassPublic = sassPublic;
exports.sassPrivate = sassPrivate;
exports.sassStudent = sassStudent;

exports.terserAdmin = terserAdmin;
exports.terserCalendar = terserCalendar;
exports.terserCoach = terserCoach;
exports.terserPublic = terserPublic;
exports.terserPrivate = terserPrivate;
exports.terserStudent = terserStudent;

exports.watch = watch;

exports.adminCSS = gulp.series(sassAdmin,concatAdminCSS);
exports.calendarCSS = gulp.series(sassCalendar,concatCalendarCSS);
exports.coachCSS = gulp.series(sassCoach,concatCoachCSS);
exports.publicCSS = gulp.series(sassPublic,concatPublicCSS);
exports.privateCSS = gulp.series(sassPrivate,concatPrivateCSS);
exports.studentCSS = gulp.series(sassStudent,concatStudentCSS);

exports.adminJS = gulp.series(concatAdminJS,terserAdmin);
exports.calendarJS = gulp.series(concatCalendarJS,terserCalendar);
exports.coachJS = gulp.series(concatCoachJS,terserCoach);
exports.publicJS = gulp.series(concatPublicJS,terserPublic);
exports.privateJS = gulp.series(concatPrivateJS,terserPrivate);
exports.studentJS = gulp.series(concatStudentJS,terserStudent);

exports.css = gulp.parallel(
	gulp.series(sassAdmin,concatAdminCSS),
	gulp.series(sassCalendar,concatCalendarCSS),
	gulp.series(sassCoach,concatCoachCSS),
	gulp.series(sassPublic,concatPublicCSS),
	gulp.series(sassPrivate,concatPrivateCSS),
	gulp.series(sassStudent,concatStudentCSS)
);

exports.js = gulp.parallel(
	gulp.series(concatAdminJS,terserAdmin),
	gulp.series(concatCalendarJS,terserCalendar),
	gulp.series(concatCoachJS,terserCoach),
	gulp.series(concatPublicJS,terserPublic),
	gulp.series(concatPrivateJS,terserPrivate),
	gulp.series(concatStudentJS,terserStudent)
);

exports.default = gulp.parallel(
	gulp.series(sassAdmin,concatAdminCSS),
	gulp.series(sassCalendar,concatCalendarCSS),
	gulp.series(sassCoach,concatCoachCSS),
	gulp.series(sassPublic,concatPublicCSS),
	gulp.series(sassPrivate,concatPrivateCSS),
	gulp.series(sassStudent,concatStudentCSS),

	gulp.series(concatAdminJS,terserAdmin),
	gulp.series(concatCalendarJS,terserCalendar),
	gulp.series(concatCoachJS,terserCoach),
	gulp.series(concatPublicJS,terserPublic),
	gulp.series(concatPrivateJS,terserPrivate),
	gulp.series(concatStudentJS,terserStudent)
);