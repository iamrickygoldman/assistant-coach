<?php

return [
	'address1' => 'Address 1',
	'address2' => 'Address 2',
	'city' => 'City',
	'country' => 'Country',
	'deleted_organization' => 'Organization Deleted',
	'edit_organization' => 'Edit Organization',
	'email' => 'Email',
	'invite_button' => 'Send Invite',
	'invite_email' => 'Coach\'s Email',
	'invite_send' => 'Send Invite',
	'invite_title' => 'Invite Coach',
	'invite_success' => 'You have invited :email to join Assistant Coach.',
	'my_organizations' => 'My Organizations',
	'name' => 'Name',
	'new_organization' => 'New Organization',
	'phone' => 'Phone',
	'saved_organization' => 'Organization Saved',
	'search' => 'Search Organizations',
	'state' => 'State',
    'organizations' => 'Organizations',
    'zip' => 'Zip',

    // Email Invite
    'invite_email_button' => 'Join Now',
    'invite_email_greeting' => 'Hello!',
    'invite_email_receiving' => 'You have been invited by :name to join their team on Assistnt Coach. Click the button to create a new account or link an existing account.',
    'invite_email_regards' => 'Regards,',
    'invite_email_trouble' => 'If you’re having trouble clicking the "Join Now" button, copy and paste the URL below into your web browser:',
];
