<?php

return [
	'admin' => 'Admin',
	'all' => 'All',
	'at' => 'At',
	'back' => 'Back',
	'cancel' => 'Cancel',
	'dashboard' => 'Dashboard',
	'delete' => 'Delete',
	'help' => 'Help',
	'go_to_x' => 'Go To :x',
	'list' => 'List',
	'notes' => 'Notes',
	'save' => 'Save',
	'search' => 'Search',
	'select' => 'Select',
	'settings' => 'Settings',
    'view_all' => 'View All',
];
