<?php

return [
	'calendar' => 'Calendar',

	// Navigation
	'prev' => 'Prev',
	'next' => 'Next',

	// Intervals
	'hour' => 'hour',
	'day' => 'day',
	'week' => 'week',
	'month' => 'month',
	'year' => 'year',
	'date' => 'date',

	// Days
	'sunday' => 'Sunday',
	'monday' => 'Monday',
	'tuesday' => 'Tuesday',
	'wednesday' => 'Wednesday',
	'thursday' => 'Thursday',
	'friday' => 'Friday',
	'saturday' => 'Saturday',

	'sun' => 'Sun',
	'mon' => 'Mon',
	'tue' => 'Tue',
	'wed' => 'Wed',
	'thu' => 'Thu',
	'fri' => 'Fri',
	'sat' => 'Sat',

	// Months
	'january' => 'January',
	'february' => 'February',
	'march' => 'March',
	'april' => 'April',
	'may' => 'May',
	'june' => 'June',
	'july' => 'July',
	'august' => 'August',
	'september' => 'September',
	'october' => 'October',
	'november' => 'November',
	'december' => 'December',

	'jan' => 'Jan',
	'feb' => 'Feb',
	'mar' => 'Mar',
	'apr' => 'Apr',
	'may' => 'May',
	'jun' => 'Jun',
	'jul' => 'Jul',
	'aug' => 'Aug',
	'sep' => 'Sep',
	'oct' => 'Oct',
	'nov' => 'Nov',
	'dec' => 'Dec',
];
