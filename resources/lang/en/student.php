<?php

return [
	'date_of_birth' => 'Date of Birth',
	'deleted_student' => 'Student Deleted',
	'edit_student' => 'Edit Student',
	'edit_student_name' => 'Edit Student - :name',
	'invite_button' => 'Send Invite',
	'invite_email' => 'Student\'s Email',
	'invite_send' => 'Send Invite',
	'invite_title' => 'Invite Student',
	'invite_success' => 'You have invited :email to join Assistant Coach.',
	'my_students' => 'My Students',
	'new_student' => 'New Student',
	'nickname' => 'Nickname',
	'saved_student' => 'Student Saved',
	'search' => 'Search Students',
	'student' => 'Student',
    'students' => 'Students',

    // Email Invite
    'invite_email_button' => 'Join Now',
    'invite_email_receiving' => 'You have been invited by :name to join Assistnt Coach. Joining is free, and will allow you to connect with :name and access resources to help you track progress and learn. Click the button to create a new account or link an existing account.',
];
