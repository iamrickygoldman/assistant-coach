<?php

return [
    'greeting' => 'Hello!',
    'regards' => 'Regards,',
    'trouble' => 'If you’re having trouble clicking the "Join Now" button, copy and paste the URL below into your web browser:',
];
