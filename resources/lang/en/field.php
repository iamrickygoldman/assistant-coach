<?php

return [
	'column_title' => 'Title',
	'column_type' => 'Type',
	'deleted_field' => 'Field Deleted',
	'display_list' => 'Display this field in the students list view.',
	'edit_field' => 'Edit Field',
	'fields' => 'Fields',
	'list' => 'List Items',
	'list_description' => 'Put each option on its own line.',
	'my_fields' => 'My Fields',
	'new_field' => 'New Field',
	'saved_field' => 'Field Saved',
	'search' => 'Search Fields',
	'title' => 'Title',
	'type' => 'Type',
];
