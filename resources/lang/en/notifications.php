<?php

return [
    'cannot_edit_organization' => 'You are not the owner of this organization and cannot edit it.',
    'expired_invite' => 'The invitation has expired. Please request a new one.',
    'invalid_address' => 'Invalid Address',
    'invite_student_has_user' => 'This student already has a user assigned.',
];
