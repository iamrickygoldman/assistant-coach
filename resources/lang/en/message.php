<?php

return [
    'messages' => 'Messages',
    'messages_unread_x' => ':count Unread Messages',
];
