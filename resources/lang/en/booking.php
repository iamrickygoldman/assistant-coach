<?php

return [
	'available' => 'Available',
	'booked' => 'Booked',
	'request_booking' => 'Request Booking',
	'request_error' => 'Something went wrong with your booking. Please try again later.',
	'request_success' => 'Your booking request has submitted successfully.',
	'start' => 'Start',
	'stop' => 'Stop',

	// Email Request
    'request_email_button' => 'View in Admin',
    'request_email_heading' => ':name has requested a booking with the following details.',
];
