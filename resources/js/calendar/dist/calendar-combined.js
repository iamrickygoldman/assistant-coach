ACCal = window.ACCal || {};

ACCal.Helper = {
	loadingCount: 0,
	hideLoading: function() {
		hideLoading();
	},
	showLoading: function() {
		showLoading();
	},
	ajax: function(params) {
		ajax(params);
	},
	hasClass: function(ele,cls) {
		return !!ele.className.match(new RegExp('(\\s|^)'+cls+'(\\s|$)'));
	},
	addClass: function(ele,cls) {
		if (!this.hasClass(ele,cls)) (ele.className += " "+cls).trim();
		return ele;
	},
	removeClass: function(ele,cls) {
		if (this.hasClass(ele,cls)) {
			let reg = new RegExp('(\\s|^)'+cls+'(\\s|$)');
			ele.className=ele.className.replace(reg,' ').trim();
		}
		return ele;
	},
	isObject: function(e){
		return typeof e === 'object' && !Array.isArray(e) && e !== null;
	}
}

function ajax(params) {
	let xhr = new XMLHttpRequest();

	if (typeof params.loading !== 'boolean') {
		params.loading = true;
	}
	if (params.loading) {
		ACCal.Helper.loadingCount++;
		if (ACCal.Helper.loadingCount > 0) {
			ACCal.Helper.showLoading();
		}
	}
	if (typeof params.url === 'undefined') {
		params.url = '';
	}
	if (typeof params.type === 'undefined') {
		params.type = 'GET';
	}
	params.type = params.type.toUpperCase();
	if (typeof params.dataType === 'undefined') {
		params.dataType = 'text';
	}
	xhr.responseType = 'text';
	if (!ACCal.Helper.isObject(params.headers)) {
		params.headers = {};
	}
	const headersKeys = Object.keys(params.headers);
	headersKeys.forEach((key, index) => {
		xhr.setRequestHeader(key, params.headers[key]);
	});

	if (typeof params.load === 'function') {
		xhr.onload = function(){
			if (xhr.status >= 200 && xhr.status < 300) {
				let response = xhr.responseText;
				if (params.dataType === 'json') {
					response = JSON.parse(response);
				}
				params.load(response, xhr.status, xhr.statusText);
			}
			else {
				params.error(xhr);
			}
			if (params.loading) {
				ACCal.Helper.loadingCount--;
				if (ACCal.Helper.loadingCount === 0) {
					ACCal.Helper.hideLoading();
				}
			}
		}
	}
	if (typeof params.error === 'function') {
		xhr.onerror = function(){
			params.error(xhr);
		}
	}

	if (typeof params.timeout === 'undefined') {
		xhr.timeout = 10000;
	}
	else {
		xhr.timeout = params.timeout;
	}
	if (!ACCal.Helper.isObject(params.data)) {
		params.data = {};
	}
	const dataKeys = Object.keys(params.data);
	let first = true;
	if (params.type === 'GET') {
		dataKeys.forEach((key, index) => {
			if (first) {
				params.url += '?';
				first = false;
			}
			else {
				params.url += '&';
			}
			params.url += key + '=' + params.data[key];
		});
	}

	xhr.open(params.type, params.url);

	if (params.type === 'POST') {
		let formData = new FormData();
		dataKeys.forEach((key, index) => {
			formData.append(key, params.data[key]);
		});
		xhr.send(formData);
	}
	else {
		xhr.send();
	}
}

function hideLoading() {
	document.querySelectorAll('.ac-loading-overlay').forEach(overlay => ACCal.Helper.removeClass(ACCal.Helper.addClass(overlay, 'is-hidden'), 'is-flex'));
}

function showLoading() {
	document.querySelectorAll('.ac-loading-overlay').forEach(overlay => ACCal.Helper.removeClass(ACCal.Helper.addClass(overlay, 'is-flex'), 'is-hidden'));
}
ACCal = window.ACCal || {};

ACCal.Localize = {
	days: {
		long: ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"],
		short: ["Sun","Mon","Tue","Wed","Thu","Fri","Sat"],
	},
	intervals: ['hour','day','week','month','year','date'],
	months: {
		long: ["January","February","March","April","May","June","July","August","September","October","November","December"],
		short: ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"],
	},
	initialize: function(){
		var self = this;
		ACCal.Helper.ajax({
			type: "get",
			url: "/api/calendar/localize",
			headers: {},
			data: {},
			dataType: 'json',
			load: function(data){
				self.days = data.days;
				self.intervals = data.intervals;
				self.months = data.months;
				self.initialized = true;
			},
			error: function(error){
				console.log(error);
			}
		});
	},
	initialized: false,
}
ACCal = window.ACCal || {};

ACCal.Structure = {
	next: function(){
		next();
	},
	initialize: function(){
		initialize();
	},

	getUpdateResourcesParams: function(){
		return getUpdateResourcesParams();
	},
	getUpdateFrameParams: function(){
		return getUpdateFrameParams();
	},
	getUpdateHourParams: function(){
		return getUpdateHourParams();
	},
	getUpdateDayParams: function(){
		return getUpdateDayParams();
	},
	getUpdateWeekParams: function(){
		return getUpdateWeekParams();
	},
	getUpdateMonthParams: function(){
		return getUpdateMonthParams();
	},
	getUpdateYearParams: function(){
		return getUpdateYearParams();
	},

	readyResources: false,
	readyFrame: false,
	readyHour: false,
	readyDay: false,
	readyWeek: false,
	readyMonth: false,
	readyYear: false,

	updateResources: function(){
		ACCal.Helper.ajax(this.getUpdateResourcesParams());
	},
	updateFrame: function(){
		ACCal.Helper.ajax(this.getUpdateFrameParams());
	},
	updateHour: function(){
		ACCal.Helper.ajax(this.getUpdateHourParams());
	},
	updateDay: function(){
		ACCal.Helper.ajax(this.getUpdateDayParams());
	},
	updateWeek: function(){
		ACCal.Helper.ajax(this.getUpdateWeekParams());
	},
	updateMonth: function(){
		ACCal.Helper.ajax(this.getUpdateMonthParams());
	},
	updateYear: function(){
		ACCal.Helper.ajax(this.getUpdateYearParams());
	},

	addTogglePanelsListener: function(){
		addTogglePanelsListener();
	}
}

function next(){
	if (!ACCal.Structure.readyResources)
	{
		ACCal.Structure.updateResources();
	}
	else if (!ACCal.Structure.readyFrame)
	{
		ACCal.Structure.updateFrame();
	}
	else if (!ACCal.Structure.readyHour)
	{
		ACCal.Structure.updateHour();
	}
	else if (!ACCal.Structure.readyDay)
	{
		ACCal.Structure.updateDay();
	}
	else if (!ACCal.Structure.readyWeek)
	{
		ACCal.Structure.updateWeek();
	}
	else if (!ACCal.Structure.readyMonth)
	{
		ACCal.Structure.updateMonth();
	}
	else if (!ACCal.Structure.readyYear)
	{
		ACCal.Structure.updateYear();
	}
}

function initialize(){
	ACCal.Structure.next();
}

function getUpdateResourcesParams(){
	ACCal.Structure.readyResources = false;
	return {
		type: "get",
		url: "/api/calendar/base-url",
		headers: {},
		data: {},
		dataType: 'json',
		load: function(data){
			document.head.insertAdjacentHTML('beforeend','<link href="'+data.url+'/css/calendar-combined.min.css" rel="stylesheet" type="text/css">');
			ACCal.Structure.readyResources = true;
			ACCal.Structure.next();
		},
		error: function(error){
			console.log(error);
		},
	};
}

function getUpdateFrameParams(){
	ACCal.Structure.readyFrame = false;
	return {
		type: "get",
		url: "/api/calendar/frame",
		headers: {},
		data: {},
		dataType: 'html',
		load: function(html){
			let cal = document.getElementsByClassName('ac-calendar');
			for (let i = 0; i < cal.length; i++) {
				cal[i].insertAdjacentHTML('beforeend',html);
			}
			ACCal.Structure.addTogglePanelsListener();
			ACCal.Structure.readyFrame = true;
			ACCal.Structure.next();
		},
		error: function(error){
			console.log(error);
		},
	};
}

function getUpdateHourParams(){
	ACCal.Structure.readyHour = false;
	return {
		type: "get",
		url: "/api/calendar/hour",
		headers: {},
		data: {},
		dataType: 'html',
		load: function(html){
			ACCal.Structure.readyHour = true;
			ACCal.Structure.next();
		},
		error: function(error){
			console.log(error);
		},
	};
}

function getUpdateDayParams(){
	ACCal.Structure.readyDay = false;
	return {
		type: "get",
		url: "/api/calendar/day",
		headers: {},
		data: {},
		dataType: 'html',
		load: function(html){
			ACCal.Structure.readyDay = true;
			ACCal.Structure.next();
		},
		error: function(error){
			console.log(error);
		},
	};
}

function getUpdateWeekParams(){
	ACCal.Structure.readyWeek = false;
	return {
		type: "get",
		url: "/api/calendar/week",
		headers: {},
		data: {},
		dataType: 'html',
		load: function(html){
			let week = document.getElementsByClassName('ac-week');
			for (let i = 0; i < week.length; i++) {
				week[i].insertAdjacentHTML('afterbegin',html);
			}
			ACCal.Structure.readyWeek = true;
			ACCal.Structure.next();
		},
		error: function(error){
			console.log(error);
		},
	};
}

function getUpdateMonthParams(){
	ACCal.Structure.readyMonth = false;
	return {
		type: "get",
		url: "/api/calendar/month",
		headers: {},
		data: {},
		dataType: 'html',
		load: function(html){
			ACCal.Structure.readyMonth = true;
			ACCal.Structure.next();
		},
		error: function(error){
			console.log(error);
		},
	};
}

function getUpdateYearParams(){
	ACCal.Structure.readyYear = false;
	return {
		type: "get",
		url: "/api/calendar/year",
		headers: {},
		data: {},
		dataType: 'html',
		load: function(html){
			ACCal.Structure.readyYear = true;
			ACCal.Structure.next();
		},
		error: function(error){
			console.log(error);
		},
	};
}

function addTogglePanelsListener(){
	document.querySelectorAll(".ac-calendar .panel-toggle").forEach(toggle => toggle.addEventListener("click", togglePanels));
}
function togglePanels(e){
	document.querySelectorAll(".ac-calendar .panel-toggle").forEach(toggle => ACCal.Helper.removeClass(toggle.parentNode,'is-active'));
	ACCal.Helper.addClass(e.target.parentNode,'is-active');
	document.querySelectorAll(".ac-calendar .panel-block-toggle").forEach(block => ACCal.Helper.addClass(block,'toggle-hidden'));
	ACCal.Helper.removeClass(document.querySelector(".ac-calendar .panel-block-toggle[data-toggle=\""+e.target.dataset.toggle+"\"]"),'toggle-hidden');
}
ACCal = window.ACCal || {};

ACCal.Localize.initialize();

ACCal.Structure.initialize();