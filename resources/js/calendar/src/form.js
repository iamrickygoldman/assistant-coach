ACCal = window.ACCal || {};

ACCal.Form = {
	addSubmitFormListener: function() {
		document.getElementById("request-booking-form").addEventListener("submit", function(e){
			e.preventDefault();
			ACCal.Helper.ajax({
				type: "post",
				url: "/api/calendar/booking/request",
				headers: {},
				data: {
					start: document.getElementById('ac_start').value,
					camp_id: document.getElementById('ac_camp_id').value,
					trainer_id: document.getElementById('ac_trainer_id').value,
					organization_id: document.getElementById('ac_organization_id').value,
					first_name: document.getElementById('ac_first_name').value,
					last_name: document.getElementById('ac_last_name').value,
					email: document.getElementById('ac_email').value,
					phone: document.getElementById('ac_phone').value,
					notes: document.getElementById('ac_notes').value,
				},
				dataType: 'json',
				load: function(data){
					console.log(data);
					ACCal.Structure.hideModal();
					if (data.success) {
						ACCal.Structure.showNotification(ACCal.Localize.booking.bookingSuccess, 'success');
					}
					else {
						ACCal.Structure.showNotification(data.error, 'danger');
					}
				},
				error: function(error){
					console.log(error);
					ACCal.Structure.hideModal();
					ACCal.Structure.showNotification(ACCal.Localize.booking.bookingError, 'danger');
				},
			});;
		});
	}
}