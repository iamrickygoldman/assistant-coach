ACCal = window.ACCal || {};

ACCal.Localize = {
	days: {
		long: ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"],
		short: ["Sun","Mon","Tue","Wed","Thu","Fri","Sat"],
	},
	intervals: ['hour','day','week','month','year','date'],
	months: {
		long: ["January","February","March","April","May","June","July","August","September","October","November","December"],
		short: ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"],
	},
	booking: {
		formSuccess: 'Your booking request has submitted successfully.',
		formError: 'Something went wrong with your booking. Please try again later.',
	},
	initialize: function(){
		var self = this;
		ACCal.Helper.ajax({
			type: "get",
			url: "/api/calendar/localize",
			headers: {},
			data: {},
			dataType: 'json',
			load: function(data){
				self.days = data.days;
				self.intervals = data.intervals;
				self.months = data.months;
				self.booking = data.booking;
				self.initialized = true;
			},
			error: function(error){
				console.log(error);
			}
		});
	},
	initialized: false,
}