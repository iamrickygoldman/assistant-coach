ACCal = window.ACCal || {};

ACCal.Helper = {
	loadingCount: 0,
	hideLoading: function() {
		hideLoading();
	},
	showLoading: function() {
		showLoading();
	},
	ajax: function(params) {
		ajax(params);
	},
	hasClass: function(ele,cls) {
		return !!ele.className.match(new RegExp('(\\s|^)'+cls+'(\\s|$)'));
	},
	addClass: function(ele,cls) {
		if (!this.hasClass(ele,cls)) (ele.className += " "+cls).trim();
		return ele;
	},
	removeClass: function(ele,cls) {
		if (this.hasClass(ele,cls)) {
			let reg = new RegExp('(\\s|^)'+cls+'(\\s|$)');
			ele.className=ele.className.replace(reg,' ').trim();
		}
		return ele;
	},
	isObject: function(e){
		return typeof e === 'object' && !Array.isArray(e) && e !== null;
	}
}

function ajax(params) {
	let xhr = new XMLHttpRequest();

	if (typeof params.loading !== 'boolean') {
		params.loading = true;
	}
	if (params.loading) {
		ACCal.Helper.loadingCount++;
		if (ACCal.Helper.loadingCount > 0) {
			ACCal.Helper.showLoading();
		}
	}
	if (typeof params.url === 'undefined') {
		params.url = '';
	}
	if (typeof params.type === 'undefined') {
		params.type = 'GET';
	}
	params.type = params.type.toUpperCase();
	if (typeof params.dataType === 'undefined') {
		params.dataType = 'text';
	}
	xhr.responseType = 'text';
	if (!ACCal.Helper.isObject(params.headers)) {
		params.headers = {};
	}
	const headersKeys = Object.keys(params.headers);
	headersKeys.forEach((key, index) => {
		xhr.setRequestHeader(key, params.headers[key]);
	});

	if (typeof params.load === 'function') {
		xhr.onload = function(){
			if (xhr.status >= 200 && xhr.status < 300) {
				let response = xhr.responseText;
				if (params.dataType === 'json') {
					response = JSON.parse(response);
				}
				params.load(response, xhr.status, xhr.statusText);
			}
			else {
				params.error(xhr);
			}
			if (params.loading) {
				ACCal.Helper.loadingCount--;
				if (ACCal.Helper.loadingCount === 0) {
					ACCal.Helper.hideLoading();
				}
			}
		}
	}
	if (typeof params.error === 'function') {
		xhr.onerror = function(){
			params.error(xhr);
		}
	}

	if (typeof params.timeout === 'undefined') {
		xhr.timeout = 10000;
	}
	else {
		xhr.timeout = params.timeout;
	}
	if (!ACCal.Helper.isObject(params.data)) {
		params.data = {};
	}
	const dataKeys = Object.keys(params.data);
	let first = true;
	if (params.type === 'GET') {
		dataKeys.forEach((key, index) => {
			if (first) {
				params.url += '?';
				first = false;
			}
			else {
				params.url += '&';
			}
			params.url += key + '=' + params.data[key];
		});
	}

	xhr.open(params.type, params.url);

	if (params.type === 'POST') {
		let formData = new FormData();
		dataKeys.forEach((key, index) => {
			formData.append(key, params.data[key]);
		});
		xhr.send(formData);
	}
	else {
		xhr.send();
	}
}

function hideLoading() {
	document.querySelectorAll('.ac-loading-overlay').forEach(overlay => ACCal.Helper.removeClass(ACCal.Helper.addClass(overlay, 'is-hidden'), 'is-flex'));
}

function showLoading() {
	document.querySelectorAll('.ac-loading-overlay').forEach(overlay => ACCal.Helper.removeClass(ACCal.Helper.addClass(overlay, 'is-flex'), 'is-hidden'));
}