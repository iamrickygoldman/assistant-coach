ACCal = window.ACCal || {};

ACCal.Structure = {
	next: function(){
		next();
	},
	initialize: function(){
		initialize();
	},

	getUpdateResourcesParams: function(){
		return getUpdateResourcesParams();
	},
	getUpdateFrameParams: function(){
		return getUpdateFrameParams();
	},
	getUpdateDayParams: function(){
		return getUpdateDayParams();
	},
	getUpdateWeekParams: function(){
		return getUpdateWeekParams();
	},
	getUpdateMonthParams: function(){
		return getUpdateMonthParams();
	},
	getUpdateFormParams: function(){
		return getUpdateFormParams();
	},

	readyResources: false,
	readyFrame: false,
	readyDay: false,
	readyWeek: false,
	readyMonth: false,
	readyForm: false,

	selectedDate: '',
	selectedTime: '',
	offsetWeek: 0,
	offsetMonth: 0,

	updateResources: function(){
		ACCal.Helper.ajax(this.getUpdateResourcesParams());
	},
	updateFrame: function(){
		ACCal.Helper.ajax(this.getUpdateFrameParams());
	},
	updateDay: function(){
		ACCal.Helper.ajax(this.getUpdateDayParams());
	},
	updateWeek: function(){
		ACCal.Helper.ajax(this.getUpdateWeekParams());
	},
	updateMonth: function(){
		ACCal.Helper.ajax(this.getUpdateMonthParams());
	},
	updateForm: function(){
		ACCal.Helper.ajax(this.getUpdateFormParams());
	},

	addTogglePanelsListener: function(){
		addTogglePanelsListener();
	},
	togglePanels: function(e){
		togglePanels(e);
	},
	addShowModalListener: function(){
		addShowModalListener();
	},
	showModal: function(){
		showModal();
	},
	addHideModalListener: function(){
		addHideModalListener();
	},
	hideModal: function(){
		hideModal();
	},
	addSubmitFormListener: function(){
		addSubmitFormListener();
	},
	showNotification: function(message, type){
		showNotification(message, type);
	},
	addHideNotificationListener: function(){
		addHideNotificationListener();
	},
	hideNotification: function(e){
		hideNotification(e);
	}
}

function next(){
	if (!ACCal.Structure.readyResources)
	{
		ACCal.Structure.updateResources();
	}
	else if (!ACCal.Structure.readyFrame)
	{
		ACCal.Structure.updateFrame();
	}
	else if (!ACCal.Structure.readyWeek)
	{
		ACCal.Structure.updateWeek();
	}
	else if (!ACCal.Structure.readyMonth)
	{
		ACCal.Structure.updateMonth();
	}
}

function initialize(){
	ACCal.Structure.next();
}

function getUpdateResourcesParams(){
	ACCal.Structure.readyResources = false;
	return {
		type: "get",
		url: "/api/calendar/base-url",
		headers: {},
		data: {},
		dataType: 'json',
		load: function(data){
			document.head.insertAdjacentHTML('beforeend','<link href="'+data.url+'/css/calendar-combined.min.css" rel="stylesheet" type="text/css">');
			ACCal.Structure.readyResources = true;
			ACCal.Structure.next();
		},
		error: function(error){
			console.log(error);
		},
	};
}

function getUpdateFrameParams(){
	ACCal.Structure.readyFrame = false;
	return {
		type: "get",
		url: "/api/calendar/frame",
		headers: {},
		data: {},
		dataType: 'html',
		load: function(html){
			let cal = document.getElementsByClassName('ac-calendar');
			for (let i = 0; i < cal.length; i++) {
				cal[i].insertAdjacentHTML('beforeend',html);
			}
			ACCal.Structure.addTogglePanelsListener();
			ACCal.Structure.addHideNotificationListener()
			ACCal.Structure.readyFrame = true;
			ACCal.Structure.next();
		},
		error: function(error){
			console.log(error);
		},
	};
}

function getUpdateDayParams(){
	ACCal.Structure.readyDay = false;
	ACCal.Helper.showLoading();
	return {
		type: "get",
		url: "/api/calendar/day",
		headers: {},
		data: {
			date: ACCal.Structure.selectedDate,
		},
		dataType: 'html',
		load: function(html){
			let day = document.getElementById('ac-modal-content');
			day.innerHTML = html;
			ACCal.Structure.addShowModalListener();
			ACCal.Structure.readyDay = true;
			ACCal.Structure.showModal();
			ACCal.Structure.addHideModalListener();
			ACCal.Helper.hideLoading();
		},
		error: function(error){
			console.log(error);
		},
	};
}

function getUpdateWeekParams(){
	ACCal.Structure.readyWeek = false;
	ACCal.Helper.showLoading();
	return {
		type: "get",
		url: "/api/calendar/week",
		headers: {},
		data: {
			offset: ACCal.Structure.offsetWeek,
		},
		dataType: 'html',
		load: function(html){
			let week = document.getElementsByClassName('ac-week');
			for (let i = 0; i < week.length; i++) {
				week[i].innerHTML = html;
			}
			ACCal.Structure.addShowModalListener();
			ACCal.Structure.readyWeek = true;
			ACCal.Helper.hideLoading();
			ACCal.Structure.next();
		},
		error: function(error){
			console.log(error);
		},
	};
}

function getUpdateMonthParams(){
	ACCal.Structure.readyMonth = false;
	return {
		type: "get",
		url: "/api/calendar/month",
		headers: {},
		data: {
			offset: ACCal.Structure.offsetMonth,
		},
		dataType: 'html',
		load: function(html){
			let month = document.getElementsByClassName('ac-month');
			for (let i = 0; i < month.length; i++) {
				month[i].innerHTML = html;
			}
			ACCal.Structure.addShowModalListener();
			ACCal.Structure.readyMonth = true;
			ACCal.Helper.hideLoading();
			ACCal.Structure.next();
		},
		error: function(error){
			console.log(error);
		},
	};
}

function getUpdateFormParams(){
	ACCal.Structure.readyForm = false;
	ACCal.Helper.showLoading();
	return {
		type: "get",
		url: "/api/calendar/form",
		headers: {},
		data: {
			date: ACCal.Structure.selectedDate,
			time: ACCal.Structure.selectedTime,
		},
		dataType: 'html',
		load: function(html){
			let form = document.getElementById('ac-modal-content');
			form.innerHTML = html;
			ACCal.Structure.readyForm = true;
			ACCal.Structure.showModal();
			ACCal.Structure.addHideModalListener();
			ACCal.Structure.addShowModalListener();
			ACCal.Structure.addSubmitFormListener();
			ACCal.Helper.hideLoading();
		},
		error: function(error){
			console.log(error);
		},
	};
}

function addTogglePanelsListener(){
	document.querySelectorAll(".ac-calendar .panel-toggle").forEach(toggle => toggle.addEventListener("click", function(e){
		ACCal.Structure.togglePanels(e);
	}));
}
function togglePanels(e){
	document.querySelectorAll(".ac-calendar .panel-toggle").forEach(toggle => ACCal.Helper.removeClass(toggle.parentNode,'is-active'));
	ACCal.Helper.addClass(e.target.parentNode,'is-active');
	document.querySelectorAll(".ac-calendar .panel-block-toggle").forEach(block => ACCal.Helper.addClass(block,'toggle-hidden'));
	ACCal.Helper.removeClass(document.querySelector(".ac-calendar .panel-block-toggle[data-toggle=\""+e.target.dataset.toggle+"\"]"),'toggle-hidden');
}

function addShowModalListener(){
	document.querySelectorAll(".ac-calendar .modal-show").forEach(toggle => toggle.addEventListener("click", function(e){
		if (ACCal.Helper.hasClass(this,'open-day'))
		{
			ACCal.Structure.selectedDate = this.getAttribute('data-date');
			ACCal.Structure.updateDay();
		}
		else if (ACCal.Helper.hasClass(this,'open-form'))
		{
			ACCal.Structure.selectedDate = this.getAttribute('data-date');
			ACCal.Structure.selectedTime = this.getAttribute('data-time');
			ACCal.Structure.updateForm();
		}
	}));
}
function showModal(){
	ACCal.Helper.addClass(document.getElementById("ac-calendar-modal"),'is-active');
}

function addHideModalListener(){
	document.querySelectorAll(".ac-calendar .modal-hide").forEach(toggle => toggle.addEventListener("click", hideModal));
}
function hideModal(){
	ACCal.Helper.removeClass(document.getElementById("ac-calendar-modal"),'is-active');
}
function addSubmitFormListener(){
	ACCal.Form.addSubmitFormListener();
}
function showNotification(message, type){
	if (typeof type == 'undefined') {
		type = 'info';
	}
	(document.querySelectorAll('.form-notification') || []).forEach((notification) => {
		ACCal.Helper.removeClass(notification.parentNode,'is-hidden');
		notification.className = 'notification form-notification is-'+type;
	});
	(document.querySelectorAll('.form-notification .content') || []).forEach((content) => {
		content.innerHTML = message;
	});
}
function addHideNotificationListener(){
	(document.querySelectorAll('.notification .delete') || []).forEach((e) => {
		ACCal.Structure.hideNotification(e);
	});
}
function hideNotification(e){
	const notification = e.parentNode;
	const panel = notification.parentNode;
	e.addEventListener('click', () => {
		ACCal.Helper.addClass(panel,'is-hidden');
	});
}