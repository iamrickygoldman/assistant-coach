jQuery(function($){
	$(document).ready(function($){
		// Panels
		$('.panel-toggle').on('click',function(){
			$('.panel-toggle').removeClass('is-active');
			$(this).addClass('is-active');
			if ($(this).data('toggle') === 'all')
			{
				$('.panel-block-toggle').removeClass('toggle-hidden');
			}
			else
			{
				$('.panel-block-toggle').addClass('toggle-hidden');
				$('.panel-block-toggle[data-toggle="'+$(this).data('toggle')+'"]').removeClass('toggle-hidden');
			}
			$('input[name="panel-search"]').trigger('input');
		});
		$('input[name="panel-search"]').on('input',function(){
			var search = $(this).val().toLowerCase();
			$('.panel-block-toggle').addClass('search-hidden').each(function(){
				if ($(this).data('search').toLowerCase().includes(search))
				{
					$(this).removeClass('search-hidden');
				}
			});
		});

		// Modals
		$('.modal-show').on('click',function(){
			$('.modal[data-modal="'+$(this).data('modal')+'"]').addClass('is-active');
		});
		$('.modal-hide').on('click',function(){
			var modalName = $(this).data('modal');
			if (typeof modalName === 'string' && $(this).data('modal') != '')
			{
				$('.modal[data-modal="'+$(this).data('modal')+'"]').removeClass('is-active');
			}
			else
			{
				$(this).closest('.modal').removeClass('is-active');
			}
		});
		$('.modal-background').on('click',function(){
			$('.modal').removeClass('is-active');
		});
	});
});