@extends('emails.base')

@section('content')

<h3 style="font-family:Arial,sans-serif">{{__('email.greeting')}}</h3>
<p>{{__('student.invite_email_receiving', ['name' => $inviter_name])}}</p>
<p style="text-align:center">
	<a style="display:inline-block;background-color:#8d63a2;color:#fff;padding:0.5em 1em;border-radius:4px;line-height:1.5;font-size:1.25em;margin:30px auto;font-family:Arial,sans-serif;text-decoration:none;font-weight:medium" href="{{$url}}">{{__('student.invite_email_button')}}</a>
</p>
<p>{{__('email.regards')}}<br>{{env('APP_NAME')}}</p>

@endsection

@section('footer')

<p>{{__('email.trouble')}} <a href="{{$url}}">{{$url}}</a></p>

@endsection