<table border="0" cellpadding="0" cellspacing="0" align="center" style="background:#f2f2ed;width:100%">
<thead></thead>
<tbody>
<tr>
	<td style="text-align:center;padding:65px 0">
    	<img src="/images/logo.png" width="283" height="25" alt="{{__('brand.name')}}" style="margin:0 auto"/>
	</td>
</tr>
<tr>
	<td>
	<table width="570" border="0" cellpadding="0" cellspacing="0" align="center" style="background:#fff">
	<thead></thead>
	<tbody>
	<tr>
		<td style="padding:30px">
	    	@yield('content')
		</td>
	</tr>
	<tr>
		<td><hr style="border-color:#f2f2ed"/></td>
	</tr>
	<tr>
		<td style="padding:30px">
	    	@yield('footer')
		</td>
	</tr>
	</tbody>
	</table>
	</td>
</tr>
<tr>
	<td style="text-align:center;padding:30px 0 65px">
		&copy; {{date('Y')}} {{__('brand.name')}}
	</td>
</tr>
</tbody>
</table>