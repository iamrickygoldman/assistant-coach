<p><em>And the embers never fade in my city by the lake<br>
The place where I was born<br>
As the wind-up toys wind down<br>
Muffling the sound of a life hidden underground<br>
Believe, believe in me, believe<br>
That you can change, that you're not stuck in vain<br>
We're not the same we're different tonite<br>
We'll crucify the insincere tonite<br>
We'll make things right and we'll feel it all tonite<br>
The impossible is possible tonite<br>
Believe in me like i believe in you tonite</em></p>