@extends('emails.base')

@section('content')

<h3 style="font-family:Arial,sans-serif">{{__('user.reset_email_greeting')}}</h3>
<p>{{__('user.reset_email_receiving')}}</p>
<p style="text-align:center">
	<a style="display:inline-block;background-color:#8d63a2;color:#fff;padding:0.5em 1em;border-radius:4px;line-height:1.5;font-size:1.25em;margin:30px auto;font-family:Arial,sans-serif;text-decoration:none;font-weight:medium" href="{{$url}}">{{__('user.reset_email_button')}}</a>
</p>
<p>{{__('user.reset_email_expire')}}</p>
<p>{{__('user.reset_email_no_action')}}</p>
<p>{{__('user.reset_email_regards')}}<br>{{env('APP_NAME')}}</p>

@endsection

@section('footer')

<p>{{__('user.reset_email_trouble')}} <a href="{{$url}}">{{$url}}</a></p>

@endsection