@extends('emails.base')

@section('content')

<h3 style="font-family:Arial,sans-serif">{{__('email.greeting')}}</h3>
<p>{{__('booking.request_email_heading', ['name' => $br->full_name])}}</p>
<dl>
@if (isset($br->start))
	<dt>{{__('booking.start')}}</dt>
	<dd>{{$br->start}}</dd>
@endif
@if (isset($br->stop))
	<dt>{{__('booking.stop')}}</dt>
	<dd>{{$br->stop}}</dd>
@endif
@if (isset($br->email))
	<dt>{{__('user.email')}}</dt>
	<dd>{{$br->email}}</dd>
@endif
@if (isset($br->phone))
	<dt>{{__('user.phone')}}</dt>
	<dd>{{$br->phone}}</dd>
@endif
@if (isset($br->notes))
	<dt>{{__('misc.notes')}}</dt>
	<dd>{{$br->notes}}</dd>
@endif
</dl>
<p style="text-align:center">
	<a style="display:inline-block;background-color:#8d63a2;color:#fff;padding:0.5em 1em;border-radius:4px;line-height:1.5;font-size:1.25em;margin:30px auto;font-family:Arial,sans-serif;text-decoration:none;font-weight:medium" href="{{$url}}">{{__('booking.request_email_button')}}</a>
</p>
<p>{{__('email.regards')}}<br>{{env('APP_NAME')}}</p>

@endsection

@section('footer')

<p>{{__('email.trouble')}} <a href="{{$url}}">{{$url}}</a></p>

@endsection