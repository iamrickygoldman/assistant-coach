<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/flatpickr@4.6.9/dist/flatpickr.min.css">
<script src="//cdn.jsdelivr.net/npm/flatpickr@4.6.9/dist/flatpickr.min.js"></script>
<script>
    if (typeof flatpickr == 'undefined')
    {
        document.write('<link rel="stylesheet" href="/css/lib/flatpickr.min.css?v=4.6.9">');
        document.write('<script src="/js/lib/flatpickr.min.js?v=4.6.9">\x3C/script>');
    }
</script>