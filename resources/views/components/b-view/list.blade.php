@if (!is_null($title))
<h1 class="has-text-centered">{{__($title)}}</h1>
@endif

<div class="table-container">
<table class="table is-fullwidth is-striped is-hoverable">
    <thead>
        <tr class="has-background-dark">
        @foreach ($columns as $column)
            <th>
                <a class="has-text-light" href="{{route($route,array_merge($query,['sort' => $column->key, 'direction' => $column->direction]))}}">
                <span class="icon-text">
                    <span>{{$column->title}}</span>
                @if ($column->sort)
                    <x-b-element.icon icon="chevron-{{$column->arrow}}" color="light" size="small"/>
                @endif
                </span>
            </th>
        @endforeach
        </tr>
    </thead>
    <tbody>
    @foreach ($items as $item)
        <tr>
        @foreach ($columns as $column)
            <td>
            @if (!is_null($edit))
                <a href="{{route($edit,['id' => $item->id])}}">
            @endif
                    {{isset($item->{$column->key}) ? $item->{$column->key} : ''}}
                </td>
            @if (!is_null($edit))
                </a>
            @endif
        @endforeach
        </tr>
    @endforeach
    </tbody>
</table>
</div>