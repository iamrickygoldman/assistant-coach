<div class="{{ $class }}">
@if ($close)
	<button class="delete"></button>
@endif
	<p class="content">{{ $slot }}</p>
</div>