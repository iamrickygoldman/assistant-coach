@if (!is_null($link))
<a class="{{$class}}" href="{{$link}}"{!!!is_null($title) ? ' title="'.$title.'"' : ''!!}>
@else
<span class="{{$class}}"{!!!is_null($title) ? ' title="'.$title.'"' : ''!!}>
@endif
	<svg width="{{$width}}" height="{{$height}}" baseProfile="full" version="1.2">
		<defs>
			<mask id="svgmask{{$count}}" maskUnits="userSpaceOnUse" maskContentUnits="userSpaceOnUse" transform="scale(1)">
				<image width="{{$width}}" height="{{$height}}" xlink:href="/images/ionicons/{{$icon}}.svg"></image>
			</mask>
		</defs>
		<rect mask="url(#svgmask{{$count}})" width="{{$width}}" height="{{$height}}" y="0" x="0" style="fill:rgb(255,255,255);"></rect>
	</svg>
@if (!is_null($link))
</a>
@else
</span>
@endif