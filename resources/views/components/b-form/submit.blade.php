@if ($cclass != '')
<div class="{{ $cclass }}">
@endif
	{{ Form::button($slot, $atts) }}
@if ($cclass != '')
</div>
@endif