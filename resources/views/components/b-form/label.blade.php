@if ($cclass != '')
<div class="{{ $cclass }}">
@endif
	{{ Form::label($name,$text, $atts) }}
@if ($cclass != '')
</div>
@endif