@if ($cclass != '')
<div class="{{ $cclass }}">
@endif
@if (isset($errors) && $show_error && $errors->has($name))
	<div class="field mb-1">
@else
	<div class="field">
@endif
@if (!is_null($label))
	<x-b-form.label :name="$fid" :text="$label" :atts="$latts"/>
@endif
@switch ($type)
@case ('email')
	{{ Form::email($name,$value,$atts) }}
@break
@case ('tel')
	{{ Form::tel($name,$value,$atts) }}
@break
@default
	{{ Form::text($name,$value,$atts) }}
@endswitch
	</div>
@if (!is_null($description))
    <x-b-form.description :class="$dclass">{{$description}}</x-b-form.description>
@endif
@if ($show_error)
	<x-b-form.error :name="$name"/>
@endif
@if ($cclass != '')
</div>
@endif