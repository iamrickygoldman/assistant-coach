@if ($cclass != '')
<div class="{{ $cclass }}">
@endif
@if (isset($errors) && $show_error && $errors->has($name))
	<div class="field mb-1">
@else
	<div class="field">
@endif
@if (!is_null($label))
	<x-b-form.label :name="$fid" :text="$label" :atts="$latts"/>
@endif
	{{ Form::password($name,$atts) }}
	</div>
@if (!is_null($description))
    <x-b-form.description :class="$dclass">{{$description}}</x-b-form.description>
@endif
@if ($show_error)
	<x-b-form.error :name="$name"/>
@endif
@if ($cclass != '')
</div>
@endif