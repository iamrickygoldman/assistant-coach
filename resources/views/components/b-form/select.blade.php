@if ($cclass != '')
<div class="{{ $cclass }}">
@endif
@if (isset($errors) && $show_error && $errors->has($name))
	<div class="field mb-1">
@else
	<div class="field">
@endif
@if (!is_null($label))
	<x-b-form.label :name="$name" :text="$label" :atts="$latts"/>
@endif
	<div class="{{ $wclass }}">
		{{ Form::select($name,$options,$value,$atts,$oatts,$ogatts) }}
	</div>
	</div>
@if (!is_null($description))
    <x-b-form.description :class="$dclass">{{$description}}</x-b-form.description>
@endif
@if ($show_error)
	<x-b-form.error :name="$name"/>
@endif
@if ($cclass != '')
</div>
@endif