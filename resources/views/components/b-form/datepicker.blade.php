@if ($cclass != '')
<div class="{{ $cclass }}">
@endif
@if (isset($errors) && $show_error && $errors->has($name))
    <div class="field mb-1">
@else
    <div class="field">
@endif
@if (!is_null($label))
    <x-b-form.label :name="$fid" :text="$label" :atts="$latts"/>
@endif
    <div class="field has-addons">
        <div class="control has-icons-right" style="width:100%">
        {{ Form::text($name,$value,$atts) }}
        <x-b-element.icon icon="calendar" color="dark" class="icon is-right"/>
        </div>
    </div>
    </div>
@if (!is_null($description))
    <x-b-form.description :class="$dclass">{{$description}}</x-b-form.description>
@endif
@if ($show_error)
    <x-b-form.error :name="$name"/>
@endif
@if ($cclass != '')
</div>
@endif

@push('scripts')
<script>
$(document.getElementById('{{$name}}')).flatpickr({
    altFormat: '{{$format}}',
    altInput: true,
});
</script>
@endpush