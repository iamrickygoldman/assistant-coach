@if ($cclass != '')
<div class="{{ $cclass }}">
@endif
	<label class="{{ $latts['class'] }}">
		{{ Form::checkbox($name,$value,$checked,$atts) }}
		<span>{{ $label }}</span>
	</label>
@if (!is_null($description))
    <x-b-form.description :class="$dclass">{{$description}}</x-b-form.description>
@endif
@if ($show_error)
	<x-b-form.error :name="$name"/>
@endif
@if ($cclass != '')
</div>
@endif