@if ($cclass != '')
<div class="{{ $cclass }}">
@endif
@if (!is_null($label))
	<x-b-form.label :name="$name" :text="$label" :atts="$latts"/>
@endif
@php $count = 0; @endphp
@foreach ($options as $val => $text)
@php $atts['id'] = $name.'_'.++$count; @endphp
	<label class="{{ $rclass }}">
	@if ($val === $value)
		{{ Form::radio($name,$val,true,$atts) }}
	@else
		{{ Form::radio($name,$val,false,$atts) }}
	@endif
		<span>{{ $text }}</span>
	</label>
@endforeach
@if (!is_null($description))
    <x-b-form.description :class="$dclass">{{$description}}</x-b-form.description>
@endif
@if ($show_error)
	<x-b-form.error :name="$name"/>
@endif
@if ($cclass != '')
</div>
@endif