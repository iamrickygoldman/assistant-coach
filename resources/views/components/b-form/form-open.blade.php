@if (is_null($model))
{{ Form::open(
    [
        'url' => $url,
        'method' => $method,
        'class' => $class,
        'id' => $fid,
        'route' => $route,
        'action' => $action,
        'files' => $files,
    ]
) }}
@else
{{ Form::model($model,
    [
        'url' => $url,
        'method' => $method,
        'class' => $class,
        'id' => $fid,
        'route' => $route,
        'action' => $action,
        'files' => $files,
    ]
) }}
@endif