@if (isset($errors))
@error($name)
<div class="{{ $class }}">
    <p>{{ $errors->first($name) }}</p>
</div>
@enderror
@endif