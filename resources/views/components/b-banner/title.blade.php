<section class="hero content is-primary is-small">
	<div class="hero-body">
		<div class="container">
			<p class="title has-text-centered">{{ $slot }}</p>
		</div>
	</div>
</section>