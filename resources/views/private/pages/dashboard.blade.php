@extends('private.layouts.main')

@section('title',__('brand.name').' | '.__('misc.dashboard'))

@section('content')
<div class="columns is-desktop mb-6">
@if (in_array('admin',$roles))
	<div class="column mt-6">
		<div class="card">
			<div class="card-content has-background-secondary py-6">
				<div class="title is-large has-text-primary-dark has-text-centered">{{__('misc.admin')}}</div>
			</div>
			<footer class="card-footer">
				<a class="button is-secondary is-light is-fullwidth" href="{{route('admin.dashboard')}}">{{__('misc.go_to_x', ['x' => __('misc.dashboard')])}}</a>
			</footer>
		</div>
	</div>
@endif
@if (in_array('coach',$roles))
	<div class="column mt-6">
		<div class="card">
			<div class="card-content has-background-grey-dark py-6">
				<div class="title is-large has-text-light has-text-centered">{{__('coach.coach')}}</div>
			</div>
			<footer class="card-footer">
				<a class="button is-tertiary is-light is-fullwidth" href="{{route('coach.dashboard')}}">{{__('misc.go_to_x', ['x' => __('misc.dashboard')])}}</a>
			</footer>
		</div>
	</div>
@endif
@if (in_array('student',$roles))
	<div class="column mt-6">
		<div class="card">
			<div class="card-content has-background-primary-light py-6">
				<div class="title is-large has-text-primary-dark has-text-centered">{{__('student.student')}}</div>
			</div>
			<footer class="card-footer">
				<a class="button is-primary is-fullwidth" href="{{route('student.dashboard')}}">{{__('misc.go_to_x', ['x' => __('misc.dashboard')])}}</a>
			</footer>
		</div>
	</div>
@endif
</div>
@endsection
