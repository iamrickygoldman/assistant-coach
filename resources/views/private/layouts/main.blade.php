<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
	<title>@yield('title')</title>
	
    @include('private.modules.head-content')

    @include('private.modules.styles')
    @yield('inline-styles')
</head>
<body>
    @include('private.modules.header')

	@yield('toolbar')

	<div id="content" class="content page-@yield('page-class')">	
	    <div class="container">

	    	@include('private.modules.alerts')

	    	@yield('content')
	    </div>
	</div>

    @include('private.modules.footer')
    @include('private.modules.scripts')
    @yield('inline-scripts')
</body>
</html>