<header>
	<nav class="navbar is-spaced is-light" role="navigation" aria-label="main naviagation">
		<div class="navbar-brand">
			<a class="navbar-item" href="/">
				<img src="/images/logo.png" width="283" height="25" alt="{{__('brand.name')}}">
			</a>

			<a role="button" class="navbar-burger" aria-label="menu" aria-expanded="false" data-target="main-menu">
				<span aria-hidden="true"></span>
				<span aria-hidden="true"></span>
				<span aria-hidden="true"></span>
			</a>
		</div>
		<div class="navbar-menu" id="main-menu">
			<div class="navbar-start">
			@if (in_array('admin',$roles))
				<a href="{{route('admin.dashboard')}}" class="navbar-item has-text-weight-semibold">{{__('misc.admin')}}</a>
			@endif
			@if (in_array('coach',$roles))
				<a href="{{route('coach.dashboard')}}" class="navbar-item has-text-weight-semibold">{{__('coach.coach')}}</a>
			@endif
			@if (in_array('student',$roles))
				<a href="{{route('student.dashboard')}}" class="navbar-item has-text-weight-semibold">{{__('student.student')}}</a>
			@endif
			</div>
			<div class="navbar-end">
				<a href="{{route('messages')}}" class="messages navbar-item is-hidden-touch" title="{{__('message.messages_unread_x',['count' => count($user->unread_messages)])}}"><x-b-element.icon icon="mail" color="dark" class="icon is-left"/>
				@if (!empty($user->unread_messages))
					<span class="messages-indicator button is-secondary is-rounded is-small">{{count($user->unread_messages)}}
				@endif
				</a>
				<div class="navbar-item has-dropdown is-hoverable">
					<a class="navbar-link">{{$user->full_name}}</a>
					<div class="navbar-dropdown">
						<a class="navbar-item">{{__('misc.settings')}}</a>
						<a class="navbar-item">{{__('user.linked_accounts')}}</a>
						<hr class="navbar-divider">
						<a class="navbar-item" href="{{route('logout')}}"><strong>{{__('user.logout')}}</strong></a>
					</div>
				</div>
			</div>
		</div>
	</nav>
</header>