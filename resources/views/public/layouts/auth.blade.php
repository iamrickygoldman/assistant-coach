<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
	<title>@yield('title')</title>
	
    @include('public.modules.head-content')

    @include('public.modules.styles')
    @yield('inline-styles')
</head>
<body>
	<header>
		<div class="container">
			<div class="column mt-4 has-text-centered">
				<a href="/">
					<img src="/images/logo.png" width="283" height="25" alt="{{__('brand.name')}}">
				</a>
			</div>
		</div>
	</header>

	<div id="content" class="content page-@yield('page-class')">	

	    	@include('public.modules.alerts')

	    	@yield('content')
	</div>

    @include('public.modules.footer')
    @include('public.modules.scripts')
    @yield('inline-scripts')
</body>
</html>