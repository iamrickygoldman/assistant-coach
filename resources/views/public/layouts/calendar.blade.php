<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
	<title>@yield('title')</title>
	
    @include('public.modules.head-content')

    @include('public.modules.styles')
    @yield('inline-styles')
</head>
<body>
    @include('public.modules.header')

	@yield('toolbar')

	<div id="content" class="content page-@yield('page-class')">	
	    <div class="container">

	    	@include('public.modules.alerts')

	    	@yield('content')
	    </div>
	</div>

    @include('public.modules.footer')

    @include('public.modules.scripts')

    @yield('inline-scripts')
</body>
</html>