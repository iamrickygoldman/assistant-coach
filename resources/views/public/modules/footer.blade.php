<footer class="footer">
	<div class="container">
		<p>{!!__('brand.copyright', ['year' => date('Y')])!!}</p>
	</div>
</footer>