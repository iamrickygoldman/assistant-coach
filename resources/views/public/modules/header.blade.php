<header>
	<div class="container">
		<nav class="navbar" role="navigation" aria-label="main navigation">
			<div class="navbar-brand">
				<a class="navbar-item" href="/">
					<img src="/images/logo.png" width="283" height="25">
				</a>
			</div>
			<div class="navbar-end">
				<div class="navbar-item">
					<div class="buttons">
					@guest
						<a class="button is-primary" href="{{route('register')}}">
							<strong>Sign Up</strong>
						</a>
						<a class="button is-light" href="{{route('login')}}">Log In</a>
					@endguest
					@auth
						<a class="button is-light" href="{{route('logout')}}">Log Out</a>
					@endauth
					</div>
				</div>
			</div>
		</nav>
	</div>
<header>