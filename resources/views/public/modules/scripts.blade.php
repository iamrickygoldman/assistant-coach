{{--
	Include Javscript common to all pages here.
--}}

<x-assets.jquery/>

<script src="{{asset(Helper::autoversion('/js/concat/public-combined.js'))}}" type="text/javascript"></script>

@stack('scripts')