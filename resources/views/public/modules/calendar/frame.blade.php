<div class="panel-wrap is-relative">
	<div class="ac-loading-overlay is-overlay is-flex is-justify-content-center is-align-content-center has-background-light">
		<img src="/images/loading.gif" alt="loading">
	</div>
	<div class="panel">
		<p class="panel-heading">{{__('calendar.calendar')}}</p>
		<div class="tabs is-centered is-boxed is-medium">
			<ul>
				<li><a class="panel-toggle" data-toggle="week">{{$locale['intervals']['week']}}</a></li>
				<li class="is-active"><a class="panel-toggle" data-toggle="month">{{$locale['intervals']['month']}}</a></li>
			</ul>
		</div>
		<div class="panel-block">
			<x-b-element.notification type="danger" class="notification form-notification is-hidden">No message</x-b-element.notification>
		</div>
		<div class="panel-block panel-block-toggle ac-week toggle-hidden" data-toggle="week"></div>
		<div class="panel-block panel-block-toggle ac-month" data-toggle="month"></div>
	</div>
</div>

<div class="modal" id="ac-calendar-modal">
	<div class="ac-loading-modal-overlay is-overlay is-flex is-justify-content-center is-align-content-center has-background-light"></div>
	<div class="modal-background"></div>
	<div class="modal-card scroll" id="ac-modal-content"></div>
</div>