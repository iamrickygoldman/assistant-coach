<x-b-form.form-open route="api.calendar.booking.request" fid="request-booking-form"/>
<header class="modal-card-head">
	<p class="modal-card-title m-0">{{__('booking.request_booking')}}</p>
	<a href="javascript:void(0);" class="delete modal-hide" data-date="{{$day->date_full}}" aria-label="close"></a>
</header>
<section class="modal-card-body">
	<x-b-form.hidden name="start" fid="ac_start" value="{{$day->date_full}} {{$time->time_full}}"/>
@if ($trainer)
	<x-b-form.hidden name="organization_id" fid="ac_organization_id" value="{{$trainer->organization_id}}"/>
	<x-b-form.hidden name="trainer_id" fid="ac_trainer_id" value="{{$trainer->id}}"/>
@else
	<x-b-form.hidden name="organization_id" fid="ac_organization_id" value="{{$organization->id}}"/>
@endif
	<x-b-form.hidden name="camp_id" fid="ac_camp_id"/>

	<div class="columns is-multiline">

		<div class="column is-full">
			<p class="is-size-5"><strong>{{$day->date}} {{strtolower(__('misc.at'))}} {{$time->time}}</strong></p>
		</div>

		<x-b-form.text name="first_name" fid="ac_first_name" label="{{__('user.first_name')}}" cclass="column is-half" required="true"/>

		<x-b-form.text name="last_name" fid="ac_last_name" label="{{__('user.last_name')}}" cclass="column is-half"/>

		<x-b-form.email name="email" fid="ac_email" label="{{__('user.email')}}" cclass="column is-half" required="true"/>

		<x-b-form.tel name="phone" fid="ac_phone" label="{{__('user.phone')}}" cclass="column is-half"/>

		<x-b-form.textarea name="notes" fid="ac_notes" label="{{__('misc.notes')}}" cclass="column is-full"/>

	</div>
</section>
<footer class="modal-card-foot">
	<div class="columns">

		<x-b-form.submit class="is-tertiary">{{__('misc.save')}}</x-b-form.submit>

		<div class="column">
			<a href="javascript:void(0);" class="button modal-show open-day" data-date="{{$day->date_full}}" aria-label="back">{{__('misc.back')}}</a>
		</div>

	</div>
</footer>
<x-b-form.form-close/>