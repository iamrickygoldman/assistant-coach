<table class="table">
	<thead>
		<tr>
			<th>
				<a class="button is-outlined month-prev" href="javascript:void(0);" title="{{__('calendar.prev')}}" onclick="window.ACCal.Structure.offsetMonth={{$offset-1}};window.ACCal.Structure.updateMonth()">
					<x-b-element.icon icon="arrow-back" class="icon" slug="month"/>
					<span>{{__('calendar.prev')}}</span>
				</a>
			</th>
			<th class="has-text-centered" colspan="5">{{$month}}</th>
			<th style="text-align:right">
				<a class="button is-outlined month-next" href="javascript:void(0);" title="{{__('calendar.next')}}" onclick="window.ACCal.Structure.offsetMonth={{$offset+1}};window.ACCal.Structure.updateMonth()">
					<span>{{__('calendar.next')}}</span>
					<x-b-element.icon icon="arrow-forward" class="icon" slug="month"/>
				</a>
			</th>
		<tr>
	</thead>
</table>

<table class="table is-bordered">
	<thead>
		@foreach ($locale['days']['short'] as $day)
			<th>{{$day}}</th>
		@endforeach
		</tr>
	</thead>
	<tbody>
	
@foreach ($schedule as $week)
	<tr>
	@foreach ($week as $day)
	@if ($day->prev)
		<td><span class="prev">{{$day->date}}</span></td>
	@elseif ($day->next)
		<td><span class="next">{{$day->date}}</span></td>
	@else
		<td><a href="javascript:void(0);" class="modal-show open-day" data-date="{{$day->date_full}}">{{$day->date}}</a></td>
	@endif
	@endforeach
	</tr>
@endforeach
	</tbody>
</table>