<table class="table is-bordered">
	<thead>
		<tr>
			<th>
				<a class="button is-outlined week-prev" href="javascript:void(0);" title="{{__('calendar.prev')}}" onclick="window.ACCal.Structure.offsetWeek={{$offset-1}};window.ACCal.Structure.updateWeek()">
					<x-b-element.icon icon="arrow-back" class="icon" slug="week"/>
					<span>{{__('calendar.prev')}}</span>
				</a>
			</th>
		@foreach ($schedule as $day)
			<th><a href="javascript:void(0);" class="modal-show open-day" data-date="{{$day->date_full}}">{{$day->date}}<br>{{$day->day}}</a></th>
		@endforeach
			<th style="text-align:right">
				<a class="button is-outlined week-next" href="javascript:void(0);" title="{{__('calendar.next')}}" onclick="window.ACCal.Structure.offsetWeek={{$offset+1}};window.ACCal.Structure.updateWeek()">
					<span>{{__('calendar.next')}}</span>
					<x-b-element.icon icon="arrow-forward" class="icon" slug="week"/>
				</a>
			</th>
		</tr>
	</thead>
	<tbody>
	
@foreach ($day->hours_of_day as $hour)
	<tr>
		<td>{{Helper::convertHourToString(hour: $hour, separator: ' ')}}</td>
	@foreach ($schedule as $day)
		<td>&mdash;</td>
	@endforeach
		<td></td>
	</tr>
@endforeach
	</tbody>
</table>