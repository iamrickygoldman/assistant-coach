<header class="modal-card-head">
	<p class="modal-card-title m-0">{{$day->day}}, {{$day->date}}</p>
	<button class="delete modal-hide" aria-label="close"></button>
</header>
<section class="modal-card-body">
	<table class="table is-bordered">
		<tbody>
		
	@foreach ($day->hours_of_day as $hour)
		<tr>
			<td>{{Helper::convertHourToString(hour: $hour, separator: ' ')}}</td>
			<td><a href="javascript:void(0);" class="modal-show open-form" data-date="{{$day->date_full}}" data-time="{{Helper::convertHourToString(hour: $hour, military: true, leading_zero: true)}}:00">{{__('booking.available')}}</a></td>
		</tr>
	@endforeach
		</tbody>
	</table>
</section>
<footer class="modal-card-foot">
	<button class="button modal-hide">{{__('misc.cancel')}}</button>
</footer>