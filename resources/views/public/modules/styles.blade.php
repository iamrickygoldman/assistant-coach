{{--
	Include Styles common to all pages here.
--}}

<link href="https://fonts.googleapis.com/css?family=PT+Serif:400,400i,700,700i|Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet">

<link href="{{asset(Helper::autoversion('/css/public-combined.min.css'))}}" rel="stylesheet" type="text/css">

@stack('styles')