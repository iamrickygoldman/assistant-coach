@extends('public.layouts.calendar')

@section('title',__('brand.name').' | '.__('calendar.calendar'))

@section('content')
<div style="margin:3rem 0">
	<div class="ac-calendar"></div>
</div>
@endsection

@section('inline-scripts')
@push('scripts')
	<script src="{{asset(Helper::autoversion('/js/calendar-combined.js'))}}" type="text/javascript"></script>
@endpush
@endsection

@section('inline-styles')
@push('scripts')
	<link href="{{asset(Helper::autoversion('/css/calendar-combined.min.css'))}}" rel="stylesheet" type="text/css">
@endpush
@endsection