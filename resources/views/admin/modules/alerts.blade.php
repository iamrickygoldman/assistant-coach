@foreach (Helper::getNotifications() as $type => $class)
	@if (Session::has($type))
	<x-b-element.notification :type="$class">{{Session::get($type)}}</x-b-element.notification>
	@endif
@endforeach