<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
	<title>@yield('title')</title>
	
    @include('admin.modules.head-content')

    @include('admin.modules.styles')
    @yield('inline-styles')
</head>
<body>
    @include('admin.modules.header')

	@yield('toolbar')

	<div id="content" class="content page-@yield('page-class')">	
	    <div class="container">

	    	@include('admin.modules.alerts')

	    	@yield('content')
	    </div>
	</div>

    @include('admin.modules.footer')
    @include('admin.modules.scripts')
    @yield('inline-scripts')
</body>
</html>