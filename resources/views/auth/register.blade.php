@extends('public.layouts.auth')

@section('title',__('brand.name').' | '.__('user.register'))

@section('content')
<div class="container pt-5 pb-6">
<x-b-form.form-open route="register" class="box w-small"/>

<div class="blocks">
    <h1 class="has-text-centered">{{__('user.register')}}</h1>

    <x-b-form.email name="email" required="1" label="{{__('user.email')}}"/>
    
    <x-b-form.text name="first_name" required="1" label="{{__('user.first_name')}}"/>

    <x-b-form.text name="last_name" required="1" label="{{__('user.last_name')}}"/>

    <x-b-form.password name="password" required="1" new_password="1" label="{{__('user.password')}}"/>

    <x-b-form.password name="password_confirmation" required="1" new_password="1" label="{{__('user.password_confirm')}}"/>

    <x-b-form.submit class="is-tertiary is-medium is-fullwidth mt-4">{{__('user.sign_up')}}</x-b-form.submit>

    <div class="column">
        <p>{{__('user.have_account')}} <a href="{{route('login')}}">{{__('user.login')}}</a></p>
    </div>

</div>

<x-b-form.form-close/>
</div>
@endsection