@extends('public.layouts.auth')

@section('title',__('brand.name').' | '.__('user.login'))

@section('content')
<div class="container pt-5 pb-6">
<div class="box w-small">

<div class="blocks">
    <h1 class="has-text-centered">{{__('user.verify_email')}}</h1>

@if (session('resent'))
    <x-b-element.notification type="success">{{__('user.verification_has_sent')}}</x-b-element.notification>
@endif

    <div class="column content is-medium mb-0">
        <p>{{__('user.verification_before_proceeding')}}</p>
        <p>{{__('user.verification_did_not_receive_email')}}</p>
    </div>

<x-b-form.form-open route="verification.resend"/>

    <x-b-form.submit class="is-tertiary is-medium is-fullwidth">{{__('user.verification_request_email')}}</x-b-form.submit>

<x-b-form.form-close/>

</div>
</div>
</div>
@endsection