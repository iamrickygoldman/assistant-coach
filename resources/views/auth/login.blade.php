@extends('public.layouts.auth')

@section('title',__('brand.name').' | '.__('user.login'))

@section('content')
<div class="container pt-5 pb-6">
<x-b-form.form-open route="login" class="box w-small"/>

<div class="blocks">
    <h1 class="has-text-centered is-hidden">{{__('user.login')}}</h1>

    <x-b-form.email name="email" required="1" star="0" label="{{__('user.email')}}"/>

    <x-b-form.password name="password" required="1" star="0" label="{{__('user.password')}}"/>

    <x-b-form.submit class="is-tertiary is-medium is-fullwidth mt-4">{{__('user.login')}}</x-b-form.submit>

    <x-b-form.checkbox name="remember" label="{{ __('user.remember_me') }}"/>

    <div class="column">
        <p><a href="{{route('password.request')}}">{{__('user.forgot_password')}}</a></p>
    </div>

    <div class="column">
        <p>{{__('user.dont_have_account')}} <a href="{{route('register')}}">{{__('user.sign_up')}}</a></p>
    </div>

</div>

<x-b-form.form-close/>
</div>
@endsection