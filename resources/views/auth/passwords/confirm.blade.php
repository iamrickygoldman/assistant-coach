@extends('public.layouts.auth')

@section('title',__('brand.name').' | '.__('user.login'))

@section('content')
<div class="container pt-5 pb-6">
<x-b-form.form-open route="password.confirm" class="box w-small"/>

<div class="blocks">
    <h1 class="has-text-centered is-hidden">{{__('user.confirm_password')}}</h1>

    <div class="column">
        <p>{{__('user.please_confirm_password')}}</p>
    </div>

    <x-b-form.password name="password" required="1" star="0" label="{{__('user.password')}}"/>

    <x-b-form.submit class="is-tertiary is-medium is-fullwidth mt-4">{{__('user.confirm_password')}}</x-b-form.submit>

@if (Route::has('password.request'))
    <div class="column">
        <p><a href="{{route('password.request')}}">{{__('user.forgot_password')}}</a></p>
    </div>
@endif

</div>

<x-b-form.form-close/>
</div>
@endsection
