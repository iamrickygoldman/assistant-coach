@extends('public.layouts.auth')

@section('title',__('brand.name').' | '.__('user.password_reset'))

@section('content')
<div class="container pt-5 pb-6">
<x-b-form.form-open route="password.email" class="box w-small"/>

<div class="blocks">
    <h1 class="has-text-centered is-hidden">{{__('user.password_reset')}}</h1>

@if (session('status'))
    <x-b-element.notification type="success">{{ session('status') }}</x-b-element.notification>
@endif

    <x-b-form.email name="email" required="1" star="0" label="{{__('user.email')}}"/>

    <x-b-form.submit class="is-tertiary is-medium is-fullwidth mt-4">{{__('Send Password Reset Link')}}</x-b-form.submit>

    <div class="column">
        <p><a href="{{route('login')}}">{{__('user.back_to_login')}}</a></p>
    </div>

    <div class="column">
        <p>{{__('user.dont_have_account')}} <a href="{{route('register')}}">{{__('user.sign_up')}}</a></p>
    </div>

</div>

<x-b-form.form-close/>
</div>
@endsection