@extends('public.layouts.auth')

@section('title',__('brand.name').' | '.__('user.login'))

@section('content')
<div class="container pt-5 pb-6">
<x-b-form.form-open route="password.update" class="box w-small"/>

<x-b-form.hidden name="token" :value="$token"/>

<div class="blocks">
    <h1 class="has-text-centered is-hidden">{{__('user.reset_password')}}</h1>

    <x-b-form.email name="email" required="1" star="0" label="{{__('user.email')}}"/>

    <x-b-form.password name="password" required="1" star="0" new_password="1" label="{{__('user.password')}}"/>

    <x-b-form.password name="password_confirmation" required="1" star="0" new_password="1" label="{{__('user.password_confirm')}}"/>

    <x-b-form.submit class="is-tertiary is-medium is-fullwidth mt-4">{{__('user.reset_password')}}</x-b-form.submit>

</div>

<x-b-form.form-close/>
</div>
@endsection