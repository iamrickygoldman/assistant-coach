@extends('coach.layouts.main')

@if (is_null($field))
@section('title',__('brand.name').' | '.__('field.new_field'))
@else
@section('title',__('brand.name').' | '.__('field.edit_field'))
@endif

@section('banner')

<x-b-banner.title>{{is_null($field) ? __('field.new_field') : __('field.edit_field')}}</x-b-banner.title>

@endsection

@section('content')

<nav class="level">
	<div class="level-left">
		<div class="level-item">
			<a class="button is-secondary" href="{{route('coach.field.list')}}" title="{{__('misc.back')}}">
				<x-b-element.icon icon="arrow-back" color="secondary" class="icon is-inverted"/>
				<span>{{__('misc.back')}}</span>
			</a>
		</div>
	</div>
	<div class="level-right">
	@if (!is_null($field))
		<div class="level-item">
			<a class="button is-tertiary" href="{{route('coach.field.new', ['o' => $list])}}" title="{{__('field.new_field')}}">
				<x-b-element.icon icon="create-o" color="tertiary" class="icon is-light"/>
				<span>{{__('field.new_field')}}</span>
			</a>
		</div>
		<div class="level-item">
			<a class="button is-danger" href="{{route('coach.field.delete', ['id' => $field->id])}}" title="{{__('misc.delete')}}">
				<x-b-element.icon icon="trash" color="danger" class="icon is-light"/>
				<span>{{__('misc.delete')}}</span>
			</a>
		</div>
	@endif
	</div>
</nav>

<x-b-form.form-open route="coach.field.save" class="edit-form w-small" :model="$field"/>

@if (!is_null($field))
<x-b-form.hidden name="id"/>
<x-b-form.hidden name="organization_id"/>
<x-b-form.hidden name="trainer_id"/>
@else
<x-b-form.hidden name="organization_id" value="{{$organization_id}}"/>
<x-b-form.hidden name="trainer_id" value="{{$trainer_id}}"/>
@endif

<div class="blocks">

	<x-b-form.text name="title" required="1" star="0" label="{{__('field.title')}}"/>

	<x-b-form.select name="type" required="1" star="0" label="{{__('field.type')}}" :options="$types"/>

	<div id="conditional_type">
		<x-b-form.textarea name="options_list_text" label="{{__('field.list')}}" description="{{__('field.list_description')}}"/>
	</div>

	<x-b-form.checkbox name="display_students_list" label="{{ __('field.display_list') }}"/>

	<x-b-form.submit class="is-tertiary is-medium is-full mt-4">{{__('misc.save')}}</x-b-form.submit>

</div>

<x-b-form.form-close/>

@endsection

@push('scripts')
<script>
{
	var typeSelect = $('#type');
	var listInput = $('#conditional_type');
	displayTypes();
	typeSelect.on('change',function(){
		displayTypes();
	});
	function displayTypes(){
		listInput.hide();
		if (typeSelect.val() === '{{$list_option}}')
		{
			listInput.show();
		}
	}
}
</script>
@endpush