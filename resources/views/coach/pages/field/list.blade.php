@extends('coach.layouts.main')

@section('title',__('brand.name').' | '.__('field.fields'))

@section('content')

<nav class="level">
	<div class="level-left">
		<div class="level-item">
			<x-b-form.form-open route="coach.field.list" method="GET"/>
			<div class="field has-addons">
				<div class="control has-icons-left">
					<x-b-form.text name="panel-search" cclass="" placeholder="{{__('field.search')}}"/>
					<x-b-element.icon icon="search" color="dark" class="icon is-left"/>
				</div>
				<div class="control">
					<x-b-form.submit class="is-primary" cclass="">{{__('misc.search')}}</x-b-form.submit>
				</div>
			</div>
			<x-b-form.form-close/>
		</div>
	</div>
	<div class="level-right">
		<div class="level-item">
			<a class="button is-tertiary" href="{{route('coach.field.new', ['o' => $list])}}" title="{{__('field.new_field')}}">
				<x-b-element.icon icon="create-o" color="tertiary" class="icon is-light"/>
				<span>{{__('field.new_field')}}</span>
			</a>
		</div>
		<div class="level-item">
			<a class="button is-primary" href="{{route('coach.field.settings')}}" title="{{__('misc.settings')}}">
				<x-b-element.icon icon="settings" color="primary" class="icon is-light"/>
				<span>{{__('misc.settings')}}</span>
			</a>
		</div>
	</div>
</nav>

<x-b-view.grouped-list :groups="$field_groups" :items="$all_fields" :columns="$columns" route="coach.field.list" edit="coach.field.edit" :list="$list" :query="$query" title="field.my_fields" />

@endsection