@extends('coach.layouts.main')

@section('title',__('brand.name').' | '.__('organization.organizations'))

@section('content')

<nav class="level">
	<div class="level-left">
		<div class="level-item">
			<x-b-form.form-open route="coach.organization.list" method="GET"/>
			<div class="field has-addons">
				<div class="control has-icons-left">
					<x-b-form.text name="panel-search" cclass="" placeholder="{{__('organization.search')}}"/>
					<x-b-element.icon icon="search" color="dark" class="icon is-left"/>
				</div>
				<div class="control">
					<x-b-form.submit class="is-primary" cclass="">{{__('misc.search')}}</x-b-form.submit>
				</div>
			</div>
			<x-b-form.form-close/>
		</div>
	</div>
	<div class="level-right">
		<div class="level-item">
			<a class="button is-tertiary" href="{{route('coach.organization.new')}}" title="{{__('organization.new_organization')}}">
				<x-b-element.icon icon="create-o" color="tertiary" class="icon is-light"/>
				<span>{{__('organization.new_organization')}}</span>
			</a>
		</div>
		<div class="level-item">
			<a class="button is-primary" href="{{route('coach.organization.settings')}}" title="{{__('misc.settings')}}">
				<x-b-element.icon icon="settings" color="primary" class="icon is-light"/>
				<span>{{__('misc.settings')}}</span>
			</a>
		</div>
	</div>
</nav>

<x-b-view.grouped-list :items="$organizations" :columns="$columns" route="coach.organization.list" edit="coach.organization.edit" :query="$query" title="organization.my_organizations" />

@endsection