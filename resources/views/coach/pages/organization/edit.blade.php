@extends('coach.layouts.main')

@if (is_null($organization))
@section('title',__('brand.name').' | '.__('organization.new_organization'))
@else
@section('title',__('brand.name').' | '.__('organization.edit_organization'))
@endif

@section('banner')

<x-b-banner.title>{{is_null($organization) ? __('organization.new_organization') : __('organization.edit_organization')}}</x-b-banner.title>

@endsection

@section('content')

<nav class="level">
	<div class="level-left">
		<div class="level-item">
			<a class="button is-secondary" href="{{route('coach.organization.list')}}" title="{{__('misc.back')}}">
				<x-b-element.icon icon="arrow-back" color="secondary" class="icon is-inverted"/>
				<span>{{__('misc.back')}}</span>
			</a>
		</div>
	</div>
	<div class="level-right">
	@if (!is_null($organization))
		<div class="level-item">
			<a class="button is-danger" href="{{route('coach.organization.delete', ['id' => $organization->id])}}" title="{{__('misc.delete')}}">
				<x-b-element.icon icon="trash" color="danger" class="icon is-light"/>
				<span>{{__('misc.delete')}}</span>
			</a>
		</div>
	@endif
	</div>
</nav>

<x-b-form.form-open route="coach.organization.save" class="edit-form" :model="$organization"/>

@if (!is_null($organization))
<x-b-form.hidden name="id"/>
@endif

<div class="columns is-multiline">

	<x-b-form.text name="name" label="{{__('organization.name')}}" cclass="column is-half"/>

	<x-b-form.email name="email" label="{{__('organization.email')}}" cclass="column is-half"/>

	<x-b-form.tel name="phone" label="{{__('organization.phone')}}" cclass="column is-half"/>

	<x-b-form.text name="address1" label="{{__('organization.address1')}}" cclass="column is-half"/>

	<x-b-form.text name="address2" label="{{__('organization.address2')}}" cclass="column is-half"/>

	<x-b-form.text name="city" label="{{__('organization.city')}}" cclass="column is-half"/>

	<x-b-form.text name="state" label="{{__('organization.state')}}" cclass="column is-half"/>

	<x-b-form.text name="zip" label="{{__('organization.zip')}}" cclass="column is-half"/>

	<x-b-form.text name="country" label="{{__('organization.country')}}" cclass="column is-half"/>

</div>
<div class="columns">

	<x-b-form.submit class="is-tertiary is-medium is-full mt-4" cclass="column is-offset-one-third is-one-third">{{__('misc.save')}}</x-b-form.submit>

</div>

<x-b-form.form-close/>

@endsection