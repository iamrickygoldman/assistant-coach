@extends('coach.layouts.main')

@section('title',__('brand.name').' | '.__('misc.dashboard'))

@section('content')
<div class="columns">
	<div class="column is-3-widescreen is-4-desktop">
		<nav class="panel is-primary panel-students">
			<p class="panel-heading">{{__('student.students')}}</p>
			<div class="panel-block">
				<div class="control has-icons-left">
					<x-b-form.text name="panel-search" cclass=""/>
					<x-b-element.icon icon="search" color="dark" class="icon is-left"/>
				</div>
			</div>
			@if (count($student_groups) > 1)
			<p class="panel-tabs is-flex-wrap-wrap">
				<a class="is-active panel-toggle" data-toggle="all">All</a>
			@foreach ($student_groups as $group => $students)
				@if (Helper::getNameFromGroup($group) == 'personal')
				<a class="panel-toggle" data-toggle="{{$group}}">{{__('coach.personal')}}</a>
				@else
				<a class="panel-toggle" data-toggle="{{$group}}">{{Helper::getNameFromGroup($group)}}</a>
				@endif
			@endforeach
			</p>
			@endif
			<div class="height-overflow-wrap">
			@foreach ($student_groups as $group => $students)
				@foreach ($students as $student)
				<a class="panel-block panel-block-toggle" data-toggle="{{$group}}" data-search="{{$student->name}}" href="{{route('coach.student.edit',['id' => $student->id])}}">
					<x-b-element.icon icon="user" color="dark" class="panel-icon" size="small"/>
					{{$student->name}}
				</a>
				@endforeach
			@endforeach
			</div>
			<div class="panel-block">
				<a class="button is-tertiary is-outlined is-fullwidth" href="{{route('coach.student.list')}}">{{__('misc.view_all')}}</a>
			</div>
		</nav>
	</div>
</div>
@endsection
