@extends('coach.layouts.main')

@section('title',__('brand.name').' | '.__('student.students'))

@section('content')

<nav class="level">
	<div class="level-left">
		<div class="level-item">
			<x-b-form.form-open route="coach.student.list" method="GET"/>
			<div class="field has-addons">
				<div class="control has-icons-left">
					<x-b-form.text name="panel-search" cclass="" placeholder="{{__('student.search')}}"/>
					<x-b-element.icon icon="search" color="dark" class="icon is-left"/>
				</div>
				<div class="control">
					<x-b-form.submit class="is-primary" cclass="">{{__('misc.search')}}</x-b-form.submit>
				</div>
			</div>
			<x-b-form.form-close/>
		</div>
	</div>
	<div class="level-right">
		<div class="level-item">
			<a class="button is-tertiary" href="{{route('coach.student.new', ['o' => $list])}}" title="{{__('student.new_student')}}">
				<x-b-element.icon icon="create-o" color="tertiary" class="icon is-light"/>
				<span>{{__('student.new_student')}}</span>
			</a>
		</div>
		<div class="level-item">
			<a class="button is-primary" href="{{route('coach.student.settings')}}" title="{{__('misc.settings')}}">
				<x-b-element.icon icon="settings" color="primary" class="icon is-light"/>
				<span>{{__('misc.settings')}}</span>
			</a>
		</div>
	</div>
</nav>

<x-b-view.grouped-list :groups="$student_groups" :items="$all_students" :columns="$columns" route="coach.student.list" edit="coach.student.edit" :list="$list" :query="$query" title="student.my_students" />

@endsection