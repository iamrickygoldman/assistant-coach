@extends('coach.layouts.main')

@if (is_null($student))
@section('title',__('brand.name').' | '.__('student.new_student'))
@else
@section('title',__('brand.name').' | '.__('student.edit_student'))
@endif

@section('banner')

<x-b-banner.title>{{is_null($student) ? __('student.new_student') : __('student.edit_student_name',['name' => $student->user->full_name])}}</x-b-banner.title>

@endsection

@section('content')

<nav class="level">
	<div class="level-left">
		<div class="level-item">
			<a class="button is-secondary" href="{{route('coach.student.list')}}" title="{{__('misc.back')}}">
				<x-b-element.icon icon="arrow-back" color="secondary" class="icon is-inverted"/>
				<span>{{__('misc.back')}}</span>
			</a>
		</div>
	</div>
	<div class="level-right">
	@if (!is_null($student))
		<div class="level-item">
			<a class="button is-tertiary" href="{{route('coach.student.new', ['o' => $list])}}" title="{{__('student.new_student')}}">
				<x-b-element.icon icon="create-o" color="tertiary" class="icon is-light"/>
				<span>{{__('student.new_student')}}</span>
			</a>
		</div>
		<div class="level-item">
			<a class="button is-info modal-show" href="javascript:void(0);" title="{{__('student.invite_button')}}" data-modal="invite">
				<x-b-element.icon icon="create-o" color="tertiary" class="icon is-light"/>
				<span>{{__('student.invite_button')}}</span>
			</a>
		</div>
		<div class="level-item">
			<a class="button is-danger" href="{{route('coach.student.delete', ['id' => $student->id])}}" title="{{__('misc.delete')}}">
				<x-b-element.icon icon="trash" color="danger" class="icon is-light"/>
				<span>{{__('misc.delete')}}</span>
			</a>
		</div>
	@endif
	</div>
</nav>

@if (!is_null($student))
<div class="modal" data-modal="invite">
	<x-b-form.form-open :url="$invite_action" class="edit-form w-small"/>
	<div class="modal-background"></div>
	<div class="modal-card">
		<header class="modal-card-head">
			<p class="modal-card-title">{{__('student.invite_title')}}</p>
			<button class="delete modal-hide" aria-label="close"></button>
		</header>
		<section class="modal-card-body">
			<x-b-form.hidden name="trainer_id" :value="$trainer_id"/>
			<x-b-form.hidden name="organization_id" :value="$organization_id"/>
			<x-b-form.email name="email" :label="$invite_email_label" required="1"/>
		</section>
		<footer class="modal-card-foot">
			<x-b-form.submit class="is-tertiary" cclass="">{{__('student.invite_send')}}</x-b-form.submit>
			<button class="button modal-hide">{{__('misc.cancel')}}</button>
		</footer>
	</div>
	<x-b-form.form-close/>
</div>
@endif

<x-b-form.form-open route="coach.student.save" class="edit-form" :model="$student"/>

@if (!is_null($student))
<x-b-form.hidden name="id"/>
<x-b-form.hidden name="organization_id"/>
<x-b-form.hidden name="trainer_id"/>
@else
<x-b-form.hidden name="organization_id" value="{{$organization_id}}"/>
<x-b-form.hidden name="trainer_id" value="{{$trainer_id}}"/>
@endif

<div class="columns is-multiline">

	<x-b-form.text name="nickname" label="{{__('student.nickname')}}" cclass="column is-half"/>

	<x-b-form.datepicker name="birth_date" label="{{__('student.date_of_birth')}}" cclass="column is-half"/>

@foreach ($field_list as $field)
	<x-dynamic-component :component="$field->input_type" :name="$field->input_name" :label="$field->title" :options="$field->input_options" :value="$field->input_value" cclass="column is-half" />
@endforeach

</div>
<div class="columns">

@if (is_null($student))
	<x-b-form.email name="invite_email" label="{{__('student.invite_email')}}" cclass="column is-half"/>
@endif

</div>
<div class="columns">

	<x-b-form.submit class="is-tertiary is-medium is-full mt-4" cclass="column is-offset-one-third is-one-third">{{__('misc.save')}}</x-b-form.submit>

</div>

<x-b-form.form-close/>

@endsection