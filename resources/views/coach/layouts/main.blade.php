<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
	<title>@yield('title')</title>
	
    @include('coach.modules.head-content')

    @include('coach.modules.styles')
    @yield('inline-styles')
</head>
<body>
    @include('coach.modules.header')

	@yield('banner')

	@yield('toolbar')

	<div id="content" class="content page-@yield('page-class')">
		<div class="container pt-5 pb-6 px-4">

	    	@include('coach.modules.alerts')

	    	@yield('content')
	    	
	    </div>
	</div>

    @include('coach.modules.footer')
    @include('coach.modules.scripts')
    @yield('inline-scripts')
</body>
</html>