{{--
	Include general <head> content common to all pages here.
--}}

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="robots" content="noindex,nofollow">

<link rel="shortcut icon" type="image/ico" href="/favicon.ico">