{{--
	Include Javscript common to all pages here.
--}}

<x-assets.jquery/>

<x-assets.flatpickr/>

<script src="{{asset(Helper::autoversion('/js/concat/coach-combined.js'))}}" type="text/javascript"></script>

@stack('scripts')