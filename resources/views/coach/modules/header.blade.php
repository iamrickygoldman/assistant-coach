<header>
	<nav class="navbar is-spaced is-dark" role="navigation" aria-label="main naviagation">
		<div class="navbar-brand">
			<a class="navbar-item" href="/">
				<img src="/images/logo.png" width="283" height="25" alt="{{__('brand.name')}}">
			</a>

			<a role="button" class="navbar-burger" aria-label="menu" aria-expanded="false" data-target="main-menu">
				<span aria-hidden="true"></span>
				<span aria-hidden="true"></span>
				<span aria-hidden="true"></span>
			</a>
		</div>
		<div class="navbar-menu" id="main-menu">
			<div class="navbar-start">
				<a href="{{route('coach.dashboard')}}" class="navbar-item">{{__('misc.dashboard')}}</a>
				<div class="navbar-item has-dropdown is-hoverable">
					<a class="navbar-link" href="{{route('coach.organization.list')}}">{{__('organization.organizations')}}</a>
					<div class="navbar-dropdown">
						<a class="navbar-item">{{__('misc.list')}}</a>
						<a class="navbar-item">{{__('misc.list')}}</a>
						<a class="navbar-item">{{__('misc.list')}}</a>
						<hr class="navbar-divider">
						<a class="navbar-item">{{__('misc.help')}}</a>
					</div>
				</div>
				<div class="navbar-item has-dropdown is-hoverable">
					<a class="navbar-link" href="{{route('coach.student.list')}}">{{__('student.students')}}</a>
					<div class="navbar-dropdown">
						<a class="navbar-item">{{__('misc.list')}}</a>
						<a class="navbar-item">{{__('misc.list')}}</a>
						<a class="navbar-item">{{__('misc.list')}}</a>
						<hr class="navbar-divider">
						<a class="navbar-item">{{__('misc.help')}}</a>
					</div>
				</div>
				<a href="{{route('coach.field.list')}}" class="navbar-item">{{__('field.fields')}}</a>
			</div>
			<div class="navbar-end">
				<a href="{{route('messages')}}" class="messages navbar-item is-hidden-touch" title="{{__('message.messages_unread_x',['count' => count($user->unread_messages)])}}"><x-b-element.icon icon="mail" color="light" class="icon is-left"/>
				@if (!empty($user->unread_messages))
					<span class="messages-indicator button is-secondary is-rounded is-small">{{count($user->unread_messages)}}
				@endif
				</a>
				<div class="navbar-item has-dropdown is-hoverable">
					<a class="navbar-link">{{$user->full_name}}</a>
					<div class="navbar-dropdown">
						<a class="navbar-item">{{__('misc.settings')}}</a>
						<a class="navbar-item">{{__('user.linked_accounts')}}</a>
						<hr class="navbar-divider">
						<a class="navbar-item" href="{{route('logout')}}"><strong>{{__('user.logout')}}</strong></a>
					</div>
				</div>
			</div>
		</div>
	</nav>
</header>