<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
	<title>@yield('title')</title>
	
    @include('student.modules.head-content')

    @include('student.modules.styles')
    @yield('inline-styles')
</head>
<body>
    @include('student.modules.header')

	@yield('toolbar')

	<div id="content" class="content page-@yield('page-class')">	
	    	@include('student.modules.alerts')

	    	@yield('content')
	    </div>
	</div>

    @include('student.modules.footer')
    @include('student.modules.scripts')
    @yield('inline-scripts')
</body>
</html>