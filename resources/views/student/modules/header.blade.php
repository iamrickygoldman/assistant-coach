<header>
	<nav class="navbar is-spaced is-light" role="navigation" aria-label="main naviagation">
		<div class="navbar-brand">
			<a class="navbar-item" href="/">
				<img src="/images/logo.png" width="283" height="25" alt="{{__('brand.name')}}">
			</a>

			<a role="button" class="navbar-burger" aria-label="menu" aria-expanded="false" data-target="main-menu">
				<span aria-hidden="true"></span>
				<span aria-hidden="true"></span>
				<span aria-hidden="true"></span>
			</a>
		</div>
		<div class="navbar-menu" id="main-menu">
			<div class="navbar-start">
				<a class="navbar-item">Home</a>
				<a class="navbar-item">Home</a>
				<div class="navbar-item has-dropdown is-hoverable">
					<a class="navbar-link">Students</a>
					<div class="navbar-dropdown">
						<a class="navbar-item">List</a>
						<a class="navbar-item">List</a>
						<a class="navbar-item">List</a>
						<hr class="navbar-divider">
						<a class="navbar-item">Help</a>
					</div>
				</div>
			</div>
			<div class="navbar-end">
				<div class="navbar-item has-dropdown is-hoverable">
					<a class="navbar-link">{{$user->full_name}}</a>
					<div class="navbar-dropdown">
						<a class="navbar-item">Settings</a>
						<a class="navbar-item">Linked Accounts</a>
						<hr class="navbar-divider">
						<a class="navbar-item" href="{{route('logout')}}"><strong>{{__('user.logout')}}</strong></a>
					</div>
				</div>
			</div>
		</div>
	</nav>
</header>