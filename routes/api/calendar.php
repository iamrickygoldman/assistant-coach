<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\Public\BookingController;
use App\Http\Controllers\Public\CalendarController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/base-url', [CalendarController::class, 'apiGetBaseUrl'])->name('base-url');

Route::get('/localize', [CalendarController::class, 'apiGetLocalize'])->name('localize');

Route::get('/frame', [CalendarController::class, 'apiGetFrame'])->name('frame');

Route::get('/day', [CalendarController::class, 'apiGetDay'])->name('day');

Route::get('/week', [CalendarController::class, 'apiGetWeek'])->name('week');

Route::get('/month', [CalendarController::class, 'apiGetMonth'])->name('month');

Route::get('/form', [CalendarController::class, 'apiGetForm'])->name('form');

Route::post('/booking/request', [BookingController::class, 'apiRequestBooking'])->name('booking.request');