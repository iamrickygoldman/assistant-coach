<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Private\DashboardController;
use App\Http\Controllers\Private\MessageController;
use App\Http\Controllers\Student\StudentController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
Route::get('/logout', 'App\Http\Controllers\Auth\LoginController@logout');
Route::get('/email/verify', 'App\Http\Controllers\Auth\VerificationController@show')->name('verification.notice');
Route::get('email/verify/{id}/{hash}', 'App\Http\Controllers\Auth\VerificationController@verify')->name('verification.verify');
Route::post('email/resend', 'App\Http\Controllers\Auth\VerificationController@resend')->name('verification.resend');
Route::get('login/route', 'App\Http\Controllers\Auth\LoginController@routeLogin')->name('login.route');

Route::middleware('verified')->group(function(){

    Route::get('/dashboard', [DashboardController::class, 'showDashboard'])->name('dashboard');

    Route::get('/messages', [MessageController::class, 'showMessages'])->name('messages');

});

Route::get('/student/join/{key}', [StudentController::class, 'showJoin'])->name('student.join');

// Calendar
Route::get('/calendar', 'App\Http\Controllers\Public\CalendarController@showCalendar')->name('calendar');