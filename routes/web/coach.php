<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Coach\DashboardController;
use App\Http\Controllers\Coach\FieldController;
use App\Http\Controllers\Coach\OrganizationController;
use App\Http\Controllers\Coach\StudentController;

/*
|--------------------------------------------------------------------------
| Coach Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/dashboard', [DashboardController::class, 'showDashboard'])->name('dashboard');

// Organizations
Route::get('/organizations', [OrganizationController::class, 'showOrganizationList'])->name('organization.list');

Route::get('/organization/view/{id}', [OrganizationController::class, 'showOrganizationView'])->name('organization.view');

Route::get('/organization/new', [OrganizationController::class, 'showOrganizationNew'])->name('organization.new');

Route::get('/organization/edit/{id}', [OrganizationController::class, 'showOrganizationEdit'])->name('organization.edit');

Route::get('/organization/view/{id}', [OrganizationController::class, 'showOrganizationView'])->name('organization.view');

Route::post('/organization/save', [OrganizationController::class, 'saveOrganization'])->name('organization.save');

Route::get('/organization/delete/{id}', [OrganizationController::class, 'deleteOrganization'])->name('organization.delete');

Route::get('/organization/settings', [OrganizationController::class, 'showOrganizationSettings'])->name('organization.settings');

Route::post('/organization/invite/{id}', [OrganizationController::class, 'inviteOrganization'])->name('organization.invite');

// Students
Route::get('/students', [StudentController::class, 'showStudentList'])->name('student.list');

Route::get('/student/view/{id}', [StudentController::class, 'showStudentView'])->name('student.view');

Route::get('/student/new', [StudentController::class, 'showStudentNew'])->name('student.new');

Route::get('/student/edit/{id}', [StudentController::class, 'showStudentEdit'])->name('student.edit');

Route::post('/student/save', [StudentController::class, 'saveStudent'])->name('student.save');

Route::get('/student/delete/{id}', [StudentController::class, 'deleteStudent'])->name('student.delete');

Route::get('/student/settings', [StudentController::class, 'showStudentSettings'])->name('student.settings');

Route::post('/student/invite/{id}', [StudentController::class, 'inviteStudent'])->name('student.invite');

// Fields
Route::get('/fields', [FieldController::class, 'showFieldList'])->name('field.list');

Route::get('/field/new', [FieldController::class, 'showFieldNew'])->name('field.new');

Route::get('/field/edit/{id}', [FieldController::class, 'showFieldEdit'])->name('field.edit');

Route::post('/field/save', [FieldController::class, 'saveField'])->name('field.save');

Route::get('/field/delete/{id}', [FieldController::class, 'deleteField'])->name('field.delete');

Route::get('/field/settings', [FieldController::class, 'showFieldSettings'])->name('field.settings');