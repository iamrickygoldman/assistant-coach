<?php

namespace Tests\Unit;

use Tests\UnitCase;

use App\Models\Calendar;

class CalendarTest extends UnitCase
{
    // STATIC

    public function test_get_days_long()
    {
        $days = Calendar::getDaysLong(false);
        $this->assertEquals('sunday',$days[0]);
        $this->assertEquals('monday',$days[1]);
        $this->assertEquals('tuesday',$days[2]);
        $this->assertEquals('wednesday',$days[3]);
        $this->assertEquals('thursday',$days[4]);
        $this->assertEquals('friday',$days[5]);
        $this->assertEquals('saturday',$days[6]);
        $days = Calendar::getDaysLong();
        $this->assertEquals('Sunday',$days['sunday']);
        $this->assertEquals('Monday',$days['monday']);
        $this->assertEquals('Tuesday',$days['tuesday']);
        $this->assertEquals('Wednesday',$days['wednesday']);
        $this->assertEquals('Thursday',$days['thursday']);
        $this->assertEquals('Friday',$days['friday']);
        $this->assertEquals('Saturday',$days['saturday']);
    }

    public function test_get_days_short()
    {
        $days = Calendar::getDaysShort(false);
        $this->assertEquals('sun',$days[0]);
        $this->assertEquals('mon',$days[1]);
        $this->assertEquals('tue',$days[2]);
        $this->assertEquals('wed',$days[3]);
        $this->assertEquals('thu',$days[4]);
        $this->assertEquals('fri',$days[5]);
        $this->assertEquals('sat',$days[6]);
        $days = Calendar::getDaysShort();
        $this->assertEquals('Sun',$days['sun']);
        $this->assertEquals('Mon',$days['mon']);
        $this->assertEquals('Tue',$days['tue']);
        $this->assertEquals('Wed',$days['wed']);
        $this->assertEquals('Thu',$days['thu']);
        $this->assertEquals('Fri',$days['fri']);
        $this->assertEquals('Sat',$days['sat']);
    }

    public function test_get_intervals()
    {
        $intervals = Calendar::getIntervals(false);
        $this->assertEquals('hour',$intervals[0]);
        $this->assertEquals('day',$intervals[1]);
        $this->assertEquals('week',$intervals[2]);
        $this->assertEquals('month',$intervals[3]);
        $this->assertEquals('year',$intervals[4]);
        $this->assertEquals('date',$intervals[5]);
        $intervals = Calendar::getIntervals();
        $this->assertEquals('hour',$intervals['hour']);
        $this->assertEquals('day',$intervals['day']);
        $this->assertEquals('week',$intervals['week']);
        $this->assertEquals('month',$intervals['month']);
        $this->assertEquals('year',$intervals['year']);
        $this->assertEquals('date',$intervals['date']);
    }

    public function test_get_locale()
    {
        $locale = Calendar::getLocale();

        $this->assertEquals('Sunday',$locale['days']['long']['sunday']);
        $this->assertEquals('Monday',$locale['days']['long']['monday']);
        $this->assertEquals('Tuesday',$locale['days']['long']['tuesday']);
        $this->assertEquals('Wednesday',$locale['days']['long']['wednesday']);
        $this->assertEquals('Thursday',$locale['days']['long']['thursday']);
        $this->assertEquals('Friday',$locale['days']['long']['friday']);
        $this->assertEquals('Saturday',$locale['days']['long']['saturday']);

        $this->assertEquals('Sun',$locale['days']['short']['sun']);
        $this->assertEquals('Mon',$locale['days']['short']['mon']);
        $this->assertEquals('Tue',$locale['days']['short']['tue']);
        $this->assertEquals('Wed',$locale['days']['short']['wed']);
        $this->assertEquals('Thu',$locale['days']['short']['thu']);
        $this->assertEquals('Fri',$locale['days']['short']['fri']);
        $this->assertEquals('Sat',$locale['days']['short']['sat']);

        $this->assertEquals('January',$locale['months']['long']['january']);
        $this->assertEquals('February',$locale['months']['long']['february']);
        $this->assertEquals('March',$locale['months']['long']['march']);
        $this->assertEquals('April',$locale['months']['long']['april']);
        $this->assertEquals('May',$locale['months']['long']['may']);
        $this->assertEquals('June',$locale['months']['long']['june']);
        $this->assertEquals('July',$locale['months']['long']['july']);
        $this->assertEquals('August',$locale['months']['long']['august']);
        $this->assertEquals('September',$locale['months']['long']['september']);
        $this->assertEquals('October',$locale['months']['long']['october']);
        $this->assertEquals('November',$locale['months']['long']['november']);
        $this->assertEquals('December',$locale['months']['long']['december']);

        $this->assertEquals('Jan',$locale['months']['short']['jan']);
        $this->assertEquals('Feb',$locale['months']['short']['feb']);
        $this->assertEquals('Mar',$locale['months']['short']['mar']);
        $this->assertEquals('Apr',$locale['months']['short']['apr']);
        $this->assertEquals('May',$locale['months']['short']['may']);
        $this->assertEquals('Jun',$locale['months']['short']['jun']);
        $this->assertEquals('Jul',$locale['months']['short']['jul']);
        $this->assertEquals('Aug',$locale['months']['short']['aug']);
        $this->assertEquals('Sep',$locale['months']['short']['sep']);
        $this->assertEquals('Oct',$locale['months']['short']['oct']);
        $this->assertEquals('Nov',$locale['months']['short']['nov']);
        $this->assertEquals('Dec',$locale['months']['short']['dec']);
    }

    public function test_get_months_long()
    {
        $months = Calendar::getMonthsLong(false);
        $this->assertEquals('january',$months[0]);
        $this->assertEquals('february',$months[1]);
        $this->assertEquals('march',$months[2]);
        $this->assertEquals('april',$months[3]);
        $this->assertEquals('may',$months[4]);
        $this->assertEquals('june',$months[5]);
        $this->assertEquals('july',$months[6]);
        $this->assertEquals('august',$months[7]);
        $this->assertEquals('september',$months[8]);
        $this->assertEquals('october',$months[9]);
        $this->assertEquals('november',$months[10]);
        $this->assertEquals('december',$months[11]);
        $months = Calendar::getMonthsLong();
        $this->assertEquals('January',$months['january']);
        $this->assertEquals('February',$months['february']);
        $this->assertEquals('March',$months['march']);
        $this->assertEquals('April',$months['april']);
        $this->assertEquals('May',$months['may']);
        $this->assertEquals('June',$months['june']);
        $this->assertEquals('July',$months['july']);
        $this->assertEquals('August',$months['august']);
        $this->assertEquals('September',$months['september']);
        $this->assertEquals('October',$months['october']);
        $this->assertEquals('November',$months['november']);
        $this->assertEquals('December',$months['december']);
    }

    public function test_get_months_short()
    {
        $months = Calendar::getMonthsShort(false);
        $this->assertEquals('jan',$months[0]);
        $this->assertEquals('feb',$months[1]);
        $this->assertEquals('mar',$months[2]);
        $this->assertEquals('apr',$months[3]);
        $this->assertEquals('may',$months[4]);
        $this->assertEquals('jun',$months[5]);
        $this->assertEquals('jul',$months[6]);
        $this->assertEquals('aug',$months[7]);
        $this->assertEquals('sep',$months[8]);
        $this->assertEquals('oct',$months[9]);
        $this->assertEquals('nov',$months[10]);
        $this->assertEquals('dec',$months[11]);
        $months = Calendar::getMonthsShort();
        $this->assertEquals('Jan',$months['jan']);
        $this->assertEquals('Feb',$months['feb']);
        $this->assertEquals('Mar',$months['mar']);
        $this->assertEquals('Apr',$months['apr']);
        $this->assertEquals('May',$months['may']);
        $this->assertEquals('Jun',$months['jun']);
        $this->assertEquals('Jul',$months['jul']);
        $this->assertEquals('Aug',$months['aug']);
        $this->assertEquals('Sep',$months['sep']);
        $this->assertEquals('Oct',$months['oct']);
        $this->assertEquals('Nov',$months['nov']);
        $this->assertEquals('Dec',$months['dec']);
    }
}
