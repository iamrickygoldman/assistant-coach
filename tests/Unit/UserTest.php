<?php

namespace Tests\Unit;

use Tests\UnitCase;

use App\Models\Organization;
use App\Models\Role;
use App\Models\Trainer;
use App\Models\User;

class UserTest extends UnitCase
{
    // ACCESSORS

    public function test_get_full_name_attribute()
    {
        $user = User::factory(1)->make(['first_name' => 'Richard', 'last_name' => null])->first();
        $this->assertEquals('Richard',$user->full_name);

        $user->last_name = 'Goldman';
        $this->assertEquals('Richard Goldman',$user->full_name);
    }

    public function test_get_organization_memberships_attribute()
    {
        $user = User::factory(1)->create(['first_name' => 'Richard', 'last_name' => null])->first();
        $organization1 = Organization::factory(1)->create()->first();
        $organization2 = Organization::factory(1)->create()->first();

        $this->assertIsArray($user->organization_memberships);
        $this->assertEmpty($user->organization_memberships);

        $trainer1 = Trainer::factory(1)->create(['user_id' => $user->id, 'organization_id' => null])->first();
        $user->refresh();
        $this->assertIsArray($user->organization_memberships);
        $this->assertEmpty($user->organization_memberships);

        $trainer2 = Trainer::factory(1)->create(['user_id' => $user->id, 'organization_id' => $organization1->id])->first();
        $user->refresh();
        $this->assertIsArray($user->organization_memberships);
        $this->assertCount(1,$user->organization_memberships);

        $trainer3 = Trainer::factory(1)->create(['user_id' => $user->id, 'organization_id' => $organization1->id])->first();
        $user->refresh();
        $this->assertIsArray($user->organization_memberships);
        $this->assertCount(2,$user->organization_memberships);

        $trainer4 = Trainer::factory(1)->create(['user_id' => $user->id, 'organization_id' => $organization2->id])->first();
        $user->refresh();
        $this->assertIsArray($user->organization_memberships);
        $this->assertCount(3,$user->organization_memberships);
        foreach ($user->organization_memberships as $membership)
        {
            $this->assertInstanceOf(Organization::class, $membership);
        }
    }

    // MISC

    public function test_has_role()
    {
    	$user = User::factory(1)->create(['first_name' => 'Richard', 'last_name' => null])->first();

        $user->roles()->attach(Role::SUPERADMIN_ID);
        $this->assertTrue($user->hasRole(Role::SUPERADMIN_ID));

        $user->roles()->attach(Role::TRAINER_ID);
        $user->refresh();
        $this->assertTrue($user->hasRole(Role::TRAINER_ID));
        $this->assertTrue($user->hasRole(Role::SUPERADMIN_ID));
    }

    // STATIC

    public function test_current_user()
    {
        // Tested in Tests\Feature\Auth\PUserTest
        $this->assertTrue(true);
    }
}
