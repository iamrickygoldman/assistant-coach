<?php

namespace Tests\Unit;

use Tests\UnitCase;

use App\Models\Organization;
use App\Models\Student;
use App\Models\Trainer;
use App\Models\User;

class OrganizationTest extends UnitCase
{
    // ACCESSORS

    public function test_get_students_count_attribute()
    {
        $organization1 = Organization::factory(1)->create()->first();
        $organization2 = Organization::factory(1)->create()->first();
        $organization3 = Organization::factory(1)->create()->first();
        $trainer1 = Trainer::factory(1)->create(['organization_id' => $organization2->id])->first();
        $trainer2 = Trainer::factory(1)->create(['organization_id' => $organization3->id])->first();
        $trainer3 = Trainer::factory(1)->create(['organization_id' => $organization3->id])->first();
        Student::factory(1)->create(['organization_id' => $organization1->id, 'trainer_id' => null]);
        Student::factory(4)->create(['trainer_id' => $trainer1->id, 'organization_id' => null]);
        Student::factory(2)->create(['organization_id' => $organization3->id, 'trainer_id' => null])->first();
        Student::factory(3)->create(['trainer_id' => $trainer2->id, 'organization_id' => null]);
        Student::factory(2)->create(['trainer_id' => $trainer3->id, 'organization_id' => null]);

        $this->assertEquals(1, $organization1->students_count);
        $this->assertEquals(4, $organization2->students_count);
        $this->assertEquals(7, $organization3->students_count);
    }

    public function test_get_trainers_count_attribute()
    {
        $organization1 = Organization::factory(1)->create()->first();
        $organization2 = Organization::factory(1)->create()->first();
        Trainer::factory(1)->create(['organization_id' => $organization1->id]);
        Trainer::factory(3)->create(['organization_id' => $organization2->id]);

        $this->assertEquals(1, $organization1->trainers_count);
        $this->assertEquals(3, $organization2->trainers_count);
    }

    // MISC

    public function test_has_student_user()
    {
        $user1 = User::factory(1)->create()->first();
        $user2 = User::factory(1)->create()->first();
        $organization1 = Organization::factory(1)->create()->first();
        $organization2 = Organization::factory(1)->create()->first();
        $trainer1 = Trainer::factory(1)->create(['organization_id' => $organization1->id])->first();
        $student1 = Student::factory(1)->create(['user_id' => $user1,'organization_id' => $organization2->id, 'trainer_id' => null])->first();
        $student2 = Student::factory(1)->create(['user_id' => $user2,'trainer_id' => $trainer1->id, 'organization_id' => null])->first();
        $this->assertFalse($organization1->hasStudentUser($user1));
        $this->assertTrue($organization2->hasStudentUser($user1));
        $this->assertFalse($organization1->hasStudentUser($user2));
        $this->assertFalse($organization2->hasStudentUser($user2));

        $user3 = User::factory(1)->create()->first();
        $student3 = Trainer::factory(1)->create(['user_id' => $user3->id])->first();
        $this->assertFalse($organization1->hasStudentUser($user3));

        $user4 = User::factory(1)->create()->first();
        $this->assertFalse($organization1->hasStudentUser($user4));
    }

    public function test_has_trainer_user()
    {
    	$user1 = User::factory(1)->create()->first();
    	$organization1 = Organization::factory(1)->create()->first();
    	$organization2 = Organization::factory(1)->create()->first();
        $trainer1 = Trainer::factory(1)->create(['user_id' => $user1->id,'organization_id' => $organization1->id])->first();
        $this->assertTrue($organization1->hasTrainerUser($user1));
        $this->assertFalse($organization2->hasTrainerUser($user1));

        $user2 = User::factory(1)->create()->first();
        $trainer2 = Trainer::factory(1)->create(['user_id' => $user2->id])->first();
        $this->assertFalse($organization1->hasTrainerUser($user2));

        $user3 = User::factory(1)->create()->first();
        $this->assertFalse($organization1->hasTrainerUser($user3));
    }
}
