<?php

namespace Tests\Unit;

use Tests\UnitCase;

use App\Models\Field;
use App\Models\Organization;
use App\Models\Role;
use App\Models\Student;
use App\Models\Trainer;
use App\Models\User;

class TrainerTest extends UnitCase
{
    // ACCESSORS

    public function test_get_name_attribute()
    {
        $user = User::factory(1)->create(['first_name' => 'Richard', 'last_name' => null])->first();
        $trainer = Trainer::factory(1)->create(['nickname' => null,'user_id' => $user->id])->first();
        $this->assertEquals('Richard',$trainer->name);

        $user->last_name = 'Goldman';
        $user->save();
        $trainer->refresh();
        $this->assertEquals('Richard Goldman',$trainer->name);

        $trainer->nickname = 'Ricky';
        $trainer->save();
        $this->assertEquals('Ricky',$trainer->name);

        $user->last_name = null;
        $user->save();
        $trainer->refresh();
        $this->assertEquals('Ricky',$trainer->name);
    }
    
    // STATIC
    
    public function test_get_field_groups()
    {
        // User with 1 trainer.
        $user = User::factory(1)->create()->first();
        $user->roles()->attach(Role::TRAINER_ID);
        $trainer1 = Trainer::factory(1)->create(['user_id' => $user->id,'organization_id' => null])->first();
        $field1 = Field::factory(1)->create(['trainer_id' => $trainer1->id, 'organization_id' => null])->first();
        $field2 = Field::factory(1)->create(['trainer_id' => $trainer1->id, 'organization_id' => null])->first();

        $field_groups = Trainer::getFieldGroups($user);
        $this->assertIsArray($field_groups);
        $this->assertCount(1,$field_groups);
        $key = 'personal|'.$trainer1->id;
        $this->assertArrayHasKey($key,$field_groups);
        $this->assertIsArray($field_groups[$key]);
        $this->assertCount(2,$field_groups[$key]);
        foreach ($field_groups[$key] as $field)
        {
        	$this->assertInstanceOf(Field::class,$field);
        }

        // User with multiple trainers.
        $trainer2 = Trainer::factory(1)->create(['user_id' => $user->id,'organization_id' => null])->first();
        $field3 = Field::factory(1)->create(['trainer_id' => $trainer2->id, 'organization_id' => null])->first();

        $user->refresh();
        $field_groups = Trainer::getFieldGroups($user);
        $this->assertIsArray($field_groups);
        $this->assertCount(2,$field_groups);
        $key = 'personal|'.$trainer1->id;
        $this->assertArrayHasKey($key,$field_groups);
        $this->assertIsArray($field_groups[$key]);
        $this->assertCount(2,$field_groups[$key]);
        foreach ($field_groups[$key] as $field)
        {
            $this->assertInstanceOf(Field::class,$field);
        }

        // User with trainer in organization.
        $organization1 = Organization::factory(1)->create(['name' => 'Test Org'])->first();
        $trainer3 = Trainer::factory(1)->create(['user_id' => $user->id,'organization_id' => $organization1->id])->first();
        $field4 = Field::factory(1)->create(['organization_id' => $organization1->id, 'trainer_id' => null])->first();

        $user->refresh();
        $field_groups = Trainer::getFieldGroups($user);
        $this->assertIsArray($field_groups);
        $this->assertCount(3,$field_groups);
        $key = 'personal|'.$trainer1->id;
        $this->assertArrayHasKey($key,$field_groups);
        $this->assertIsArray($field_groups[$key]);
        $this->assertCount(2,$field_groups[$key]);
        foreach ($field_groups[$key] as $field)
        {
            $this->assertInstanceOf(Field::class,$field);
        }
        $key = 'personal|'.$trainer2->id;
        $this->assertArrayHasKey($key,$field_groups);
        $this->assertIsArray($field_groups[$key]);
        $this->assertCount(1,$field_groups[$key]);
        foreach ($field_groups[$key] as $field)
        {
            $this->assertInstanceOf(Field::class,$field);
        }
        $key = 'Test Org|'.$organization1->id;
        $this->assertIsArray($field_groups[$key]);
        $this->assertCount(1,$field_groups[$key]);
        foreach ($field_groups[$key] as $field)
        {
        	$this->assertInstanceOf(Field::class,$field);
        }

        // User with multiple trainers in organizations.
        $organization2 = Organization::factory(1)->create(['name' => 'Check Org'])->first();
        $trainer4 = Trainer::factory(1)->create(['user_id' => $user->id,'organization_id' => $organization2->id])->first();
        $field5 = Field::factory(1)->create(['organization_id' => $organization2->id, 'trainer_id' => null])->first();
        $field6 = Field::factory(1)->create(['organization_id' => $organization2->id, 'trainer_id' => null])->first();

        $user->refresh();
        $field_groups = Trainer::getFieldGroups($user);
        $this->assertIsArray($field_groups);
        $this->assertCount(4,$field_groups);
        $key = 'personal|'.$trainer1->id;
        $this->assertArrayHasKey($key,$field_groups);
        $this->assertIsArray($field_groups[$key]);
        $this->assertCount(2,$field_groups[$key]);
        foreach ($field_groups[$key] as $field)
        {
            $this->assertInstanceOf(Field::class,$field);
        }
        $key = 'personal|'.$trainer2->id;
        $this->assertArrayHasKey($key,$field_groups);
        $this->assertIsArray($field_groups[$key]);
        $this->assertCount(1,$field_groups[$key]);
        foreach ($field_groups[$key] as $field)
        {
            $this->assertInstanceOf(Field::class,$field);
        }
        $key = 'Test Org|'.$organization1->id;
        $this->assertIsArray($field_groups[$key]);
        $this->assertCount(1,$field_groups[$key]);
        foreach ($field_groups[$key] as $field)
        {
            $this->assertInstanceOf(Field::class,$field);
        }
        $key = 'Check Org|'.$organization2->id;
        $this->assertIsArray($field_groups[$key]);
        $this->assertCount(2,$field_groups[$key]);
        foreach ($field_groups[$key] as $field)
        {
        	$this->assertInstanceOf(Field::class,$field);
        }
    }

    public function test_get_student_groups()
    {
        // User with 1 trainer.
        $user = User::factory(1)->create()->first();
        $user->roles()->attach(Role::TRAINER_ID);
        $trainer1 = Trainer::factory(1)->create(['user_id' => $user->id,'organization_id' => null])->first();
        $student1 = Student::factory(1)->create(['trainer_id' => $trainer1->id, 'organization_id' => null])->first();
        $student2 = Student::factory(1)->create(['trainer_id' => $trainer1->id, 'organization_id' => null])->first();

        $student_groups = Trainer::getStudentGroups($user);
        $this->assertIsArray($student_groups);
        $this->assertCount(1,$student_groups);
        $key = 'personal|'.$trainer1->id;
        $this->assertArrayHasKey($key,$student_groups);
        $this->assertIsArray($student_groups[$key]);
        $this->assertCount(2,$student_groups[$key]);
        foreach ($student_groups[$key] as $student)
        {
            $this->assertInstanceOf(Student::class,$student);
        }

        // User with multiple trainers.
        $trainer2 = Trainer::factory(1)->create(['user_id' => $user->id,'organization_id' => null])->first();
        $student3 = Student::factory(1)->create(['trainer_id' => $trainer2->id, 'organization_id' => null])->first();

        $user->refresh();
        $student_groups = Trainer::getStudentGroups($user);
        $this->assertIsArray($student_groups);
        $this->assertCount(2,$student_groups);
        $key = 'personal|'.$trainer1->id;
        $this->assertArrayHasKey($key,$student_groups);
        $this->assertIsArray($student_groups[$key]);
        $this->assertCount(2,$student_groups[$key]);
        foreach ($student_groups[$key] as $student)
        {
            $this->assertInstanceOf(Student::class,$student);
        }
        $key = 'personal|'.$trainer2->id;
        $this->assertArrayHasKey($key,$student_groups);
        $this->assertIsArray($student_groups[$key]);
        $this->assertCount(1,$student_groups[$key]);
        foreach ($student_groups[$key] as $student)
        {
            $this->assertInstanceOf(Student::class,$student);
        }

        // User with trainer in organization.
        $organization1 = Organization::factory(1)->create(['name' => 'Test Org'])->first();
        $trainer3 = Trainer::factory(1)->create(['user_id' => $user->id,'organization_id' => $organization1->id])->first();
        $student4 = Student::factory(1)->create(['organization_id' => $organization1->id, 'trainer_id' => null])->first();

        $user->refresh();
        $student_groups = Trainer::getStudentGroups($user);
        $this->assertIsArray($student_groups);
        $this->assertCount(3,$student_groups);
        $key = 'personal|'.$trainer1->id;
        $this->assertArrayHasKey($key,$student_groups);
        $this->assertIsArray($student_groups[$key]);
        $this->assertCount(2,$student_groups[$key]);
        foreach ($student_groups[$key] as $student)
        {
            $this->assertInstanceOf(Student::class,$student);
        }
        $key = 'personal|'.$trainer2->id;
        $this->assertArrayHasKey($key,$student_groups);
        $this->assertIsArray($student_groups[$key]);
        $this->assertCount(1,$student_groups[$key]);
        foreach ($student_groups[$key] as $student)
        {
            $this->assertInstanceOf(Student::class,$student);
        }
        $key = 'Test Org|'.$organization1->id;
        $this->assertIsArray($student_groups[$key]);
        $this->assertCount(1,$student_groups[$key]);
        foreach ($student_groups[$key] as $student)
        {
            $this->assertInstanceOf(Student::class,$student);
        }

        // User with multiple trainers in organizations.
        $organization2 = Organization::factory(1)->create(['name' => 'Check Org'])->first();
        $trainer4 = Trainer::factory(1)->create(['user_id' => $user->id,'organization_id' => $organization2->id])->first();
        $student5 = Student::factory(1)->create(['organization_id' => $organization2->id, 'trainer_id' => null])->first();
        $student6 = Student::factory(1)->create(['organization_id' => $organization2->id, 'trainer_id' => null])->first();

        $user->refresh();
        $student_groups = Trainer::getStudentGroups($user);
        $this->assertIsArray($student_groups);
        $this->assertCount(4,$student_groups);
        $key = 'personal|'.$trainer1->id;
        $this->assertArrayHasKey($key,$student_groups);
        $this->assertIsArray($student_groups[$key]);
        $this->assertCount(2,$student_groups[$key]);
        foreach ($student_groups[$key] as $student)
        {
            $this->assertInstanceOf(Student::class,$student);
        }
        $key = 'personal|'.$trainer2->id;
        $this->assertArrayHasKey($key,$student_groups);
        $this->assertIsArray($student_groups[$key]);
        $this->assertCount(1,$student_groups[$key]);
        foreach ($student_groups[$key] as $student)
        {
            $this->assertInstanceOf(Student::class,$student);
        }
        $key = 'Test Org|'.$organization1->id;
        $this->assertIsArray($student_groups[$key]);
        $this->assertCount(1,$student_groups[$key]);
        foreach ($student_groups[$key] as $student)
        {
            $this->assertInstanceOf(Student::class,$student);
        }
        $key = 'Check Org|'.$organization2->id;
        $this->assertIsArray($student_groups[$key]);
        $this->assertCount(2,$student_groups[$key]);
        foreach ($student_groups[$key] as $student)
        {
            $this->assertInstanceOf(Student::class,$student);
        }
    }

    // MISC

    // public function test_generate_invite_key(){}
}
