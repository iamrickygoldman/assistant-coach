<?php

namespace Tests\Unit;

use Tests\UnitCase;

use App\Helpers\ValuesHelper;

class ValuesHelperTest extends UnitCase
{

    public function test_get_hours_of_day()
    {
        $hours = ValuesHelper::getHoursOfDay();
        $this->assertIsArray($hours);
        $this->assertCount(24,$hours);
        $this->assertEquals(0,$hours[0]);
        $this->assertEquals(23,$hours[23]);
        $hours = ValuesHelper::getHoursOfDay(start: 8, stop: 16);
        $this->assertIsArray($hours);
        $this->assertCount(9,$hours);
        $this->assertEquals(8,$hours[0]);
        $this->assertEquals(16,$hours[8]);
    }

    public function test_get_max_days_of_month()
    {
    	$days = ValuesHelper::getMaxDaysOfMonth(1,2036);
        $this->assertEquals(31,$days);

    	$days = ValuesHelper::getMaxDaysOfMonth(2,2021);
        $this->assertEquals(28,$days);

        $days = ValuesHelper::getMaxDaysOfMonth(2,2020);
        $this->assertEquals(29,$days);

        $days = ValuesHelper::getMaxDaysOfMonth(3,2041);
        $this->assertEquals(31,$days);

        $days = ValuesHelper::getMaxDaysOfMonth(4,2016);
        $this->assertEquals(30,$days);

        $days = ValuesHelper::getMaxDaysOfMonth(5,2017);
        $this->assertEquals(31,$days);

        $days = ValuesHelper::getMaxDaysOfMonth(6,2005);
        $this->assertEquals(30,$days);

        $days = ValuesHelper::getMaxDaysOfMonth(7,1876);
        $this->assertEquals(31,$days);

        $days = ValuesHelper::getMaxDaysOfMonth(8,2221);
        $this->assertEquals(31,$days);

        $days = ValuesHelper::getMaxDaysOfMonth(9,1987);
        $this->assertEquals(30,$days);

        $days = ValuesHelper::getMaxDaysOfMonth(10,2000);
        $this->assertEquals(31,$days);

        $days = ValuesHelper::getMaxDaysOfMonth(11,1986);
        $this->assertEquals(30,$days);

        $days = ValuesHelper::getMaxDaysOfMonth(12,2025);
        $this->assertEquals(31,$days);
    }

    public function test_get_time_zone_options()
    {
    	$options = ValuesHelper::getTimeZoneOptions($this->getStorageDir().DIRECTORY_SEPARATOR.'app'.DIRECTORY_SEPARATOR.'lists'.DIRECTORY_SEPARATOR.'timezones.php');
    	$this->assertIsArray($options);
    	foreach ($options as $region => $zones)
    	{
    		$this->assertMatchesRegularExpression('/^[a-zA-Z]+$/',$region,'Failed with: '.$region);
    		foreach ($zones as $name => $offset)
    		{
    			$this->assertMatchesRegularExpression('/^[a-zA-Z]+\/[a-zA-Z_-]+$/',$name,'Failed with: '.$name);
    			$this->assertMatchesRegularExpression('/^[a-zA-Z_-]+ [+-](1[012]|[0-9]):-?[034][05]$/',$offset,'Failed with: '.$offset);
    		}
    	}
    }
}
