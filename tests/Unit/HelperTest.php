<?php

namespace Tests\Unit;

use Tests\UnitCase;

use App\Helpers\Helper;

class HelperTest extends UnitCase
{
    public function test_array_without_key()
    {
        $array1 = [
            'fruit' => 'apple',
            'seeds' => 5,
            'skin' => false,
        ];
        $array2 = Helper::arrayWithoutKey($array1,'skin');
        $this->assertIsArray($array2);
        $this->assertArrayHasKey('fruit',$array2);
        $this->assertArrayHasKey('seeds',$array2);
        $this->assertArrayNotHasKey('skin',$array2);
        $this->assertArrayHasKey('fruit',$array1);
        $this->assertArrayHasKey('seeds',$array1);
        $this->assertArrayHasKey('skin',$array1);
    }

    public function test_autoversion()
    {
    	$versioned = Helper::autoversion('/css/coach.css',$this->getPublicDir());
        $this->assertMatchesRegularExpression('/^\/css\/coach\.css\?[0-9]{10}$/',$versioned,'Failed with: '.$versioned);
    }

    public function test_convert_hour_to_string()
    {
        $string = Helper::convertHourToString(11);
        $this->assertEquals("11am",$string);

        $string = Helper::convertHourToString(hour: 12, separator: ' ');
        $this->assertEquals("12 pm",$string);

        $string = Helper::convertHourToString(hour: 13);
        $this->assertEquals("1pm",$string);

        $string = Helper::convertHourToString(hour: 15, military: true);
        $this->assertEquals("15",$string);

        $string = Helper::convertHourToString(hour: 0, military: true, separator: '-');
        $this->assertEquals("0",$string);

        $string = Helper::convertHourToString(hour: -1, military: true, separator: '-');
        $this->assertEquals("-1",$string);

        $string = Helper::convertHourToString(hour: 24);
        $this->assertEquals("24",$string);
    }

    public function test_get_id_from_group()
    {
        $group_name = 'Test Group|73';
        $this->assertEquals(73,Helper::getIdFromGroup($group_name));
    }

    public function test_get_name_from_group()
    {
        $group_name = 'Test Group|73';
        $this->assertEquals('Test Group',Helper::getNameFromGroup($group_name));
    }

    public function test_get_notifications()
    {
        $notifications = Helper::getNotifications();
        $this->assertIsArray($notifications);
        foreach ($notifications as $key => $label)
        {
            $this->assertIsString($key,'Failed with: '.$key);
            $this->assertIsString($label,'Failed with: '.$label);
        }
    }
}
