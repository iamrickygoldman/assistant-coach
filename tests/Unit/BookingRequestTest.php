<?php

namespace Tests\Unit;

use Tests\UnitCase;
use Illuminate\Support\Facades\Mail;

use App\Models\BookingRequest;
use App\Mail\BookingRequest as BookingRequestMail;

class BookingRequestTest extends UnitCase
{
    //ACCESSORS

    public function test_get_full_name_attribute()
    {
        $br = BookingRequest::factory(1)->create(['first_name' => 'Tesst', 'last_name' => null])->first();
        $this->assertEquals('Tesst', $br->full_name);
        $br->last_name = 'Testerson';
        $this->assertEquals('Tesst Testerson', $br->full_name);
    }

    // MISC

    public function test_send()
    {
        $br = BookingRequest::factory(1)->create(['notes' => 'Test Notes'])->first();

        // Test render and sending to inactive email address
        $mail = $br->send($this->fake_email, false);
        $html = $mail->render();
        $this->assertTrue($mail->hasTo($this->fake_email));
        $this->assertStringContainsString('Test Notes', $html);

        // Test sending fake
        $mail = $br->send($this->real_email, true);
        Mail::assertSent(BookingRequestMail::class, function($mail) {
            $mail->build();
            $this->assertTrue($mail->hasTo($this->real_email));
            $this->assertEquals('emails.booking.request', $mail->view);
            return true;
        });
    }
}
