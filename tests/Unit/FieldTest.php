<?php

namespace Tests\Unit;

use Tests\UnitCase;

use App\Models\Field;
use App\Models\Organization;
use App\Models\Trainer;
use App\Models\User;

class FieldTest extends UnitCase
{
    // ACCESSORS

    public function test_get_input_name_attribute()
    {
        $field = Field::factory(1)->make()->first();

        $this->assertEquals('field_data['.$field->type.']['.$field->id.']',$field->input_name);
    }

    public function test_get_input_options_attribute()
    {
        $field = Field::factory(1)->make(['type' => Field::TYPE_BOOLEAN])->first();
        $this->assertIsArray($field->input_options);
        $this->assertArrayHasKey(1,$field->input_options);
        $this->assertArrayHasKey(0,$field->input_options);

        $field = Field::factory(1)->make(['type' => Field::TYPE_LIST])->first();
        $this->assertIsArray($field->input_options);

        $field = Field::factory(1)->make(['type' => Field::TYPE_NUMBER])->first();
        $this->assertIsArray($field->input_options);
        $this->assertEmpty($field->input_options);
    }

    public function test_get_input_type_attribute()
    {
        $field = Field::factory(1)->make(['type' => Field::TYPE_BOOLEAN])->first();
        $this->assertEquals('b-form.radio',$field->input_type);

        $field = Field::factory(1)->make(['type' => 'fake'])->first();
        $this->assertEquals('b-form-text',$field->input_type);
    }

    public function test_get_options_list_text_attribute()
    {
        $field = Field::factory(1)->make(['type' => Field::TYPE_LIST])->first();
        $list = ['test','check','Look Over'];
        $field->options = [
            'list' => implode(PHP_EOL,$list)
        ];
        $this->assertEquals('test'.PHP_EOL.'check'.PHP_EOL.'Look Over',$field->options_list_text);
    }

    public function test_get_type_text_attribute()
    {
        $field1 = Field::factory(1)->make(['type' => Field::TYPE_NUMBER])->first();
        $this->assertEquals('Number',$field1->type_text);

        $field2 = Field::factory(1)->make(['type' => 'fake'])->first();
        $this->assertEquals('',$field2->type_text);
    }

    // MUTATORS

    public function test_set_options_attribute()
    {
        $field = Field::factory(1)->create(['type' => Field::TYPE_LIST])->first();

        $list = ['test','check','Look Over'];
        $field->options = [
            'list' => implode(PHP_EOL,$list)
        ];
        $field->save();
        $this->assertEquals('{"list":"test\ncheck\nLook Over"}',$field->getAttributes()['options']);

        $field->options = [
            'list' => implode(PHP_EOL,$list),
            'test' => 'boogaloo'
        ];
        $field->save();
        $this->assertEquals('{"list":"test\ncheck\nLook Over","test":"boogaloo"}',$field->getAttributes()['options']);

        $field->options = [];
        $field->save();
        $this->assertNull($field->getAttributes()['options']);
    }

    // MISC

    public function test_has_trainer_user()
    {
        $user1 = User::factory(1)->create()->first();
        $user2 = User::factory(1)->create()->first();
        $organization1 = Organization::factory(1)->create()->first();
        $trainer1 = Trainer::factory(1)->create(['user_id' => $user1->id,'organization_id' => $organization1->id])->first();
        $trainer2 = Trainer::factory(1)->create(['user_id' => $user1->id])->first();
        $trainer3 = Trainer::factory(1)->create(['user_id' => $user2->id])->first();
        $field1 = Field::factory(1)->create(['organization_id' => $organization1->id, 'trainer_id' => null])->first();
        $field2 = Field::factory(1)->create(['trainer_id' => $trainer2->id, 'organization_id' => null])->first();
        $field3 = Field::factory(1)->create(['trainer_id' => $trainer3->id, 'organization_id' => null])->first();
        $field4 = Field::factory(1)->create(['trainer_id' => 1, 'organization_id' => null])->first();
        
        $this->assertTrue($field1->hasTrainerUser($user1));
        $this->assertFalse($field1->hasTrainerUser($user2));
        $this->assertTrue($field2->hasTrainerUser($user1));
        $this->assertFalse($field2->hasTrainerUser($user2));
        $this->assertFalse($field3->hasTrainerUser($user1));
        $this->assertTrue($field3->hasTrainerUser($user2));
        $this->assertFalse($field4->hasTrainerUser($user1));
        $this->assertFalse($field4->hasTrainerUser($user2));
    }

    // STATIC

    public function test_get_type_options()
    {
        $fields = Field::getTypeOptions();
        $this->assertIsArray($fields);
        $this->assertArrayNotHasKey('',$fields);

        $fields = Field::getTypeOptions(true);
        $this->assertIsArray($fields);
        $this->assertArrayHasKey('',$fields);

        $fields = Field::getTypeOptions(false);
        $this->assertIsArray($fields);
        $this->assertArrayNotHasKey('',$fields);
    }
}
