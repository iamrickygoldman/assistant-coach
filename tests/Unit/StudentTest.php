<?php

namespace Tests\Unit;

use Tests\UnitCase;
use Illuminate\Support\Facades\Mail;
use Carbon\Carbon;

use App\Mail\InviteStudent;
use App\Models\Field;
use App\Models\Organization;
use App\Models\Student;
use App\Models\Trainer;
use App\Models\User;

class StudentTest extends UnitCase
{
    //ACCESSORS

    public function test_get_age_attribute()
    {
        $student = Student::factory(1)->make(['birth_year' => null, 'birth_month' => null, 'birth_day' => null])->first();
        $this->assertEquals('',$student->age);

        $student->birth_day = 1;
        $student->birth_month = null;
        $student->birth_year = null;
        $this->assertEquals('',$student->age);

        $student->birth_day = null;
        $student->birth_month = 1;
        $student->birth_year = null;
        $this->assertEquals('',$student->age);

        $student->birth_day = null;
        $student->birth_month = null;
        $student->birth_year = 2000;
        $this->assertEquals('',$student->age);

        $student->birth_day = 1;
        $student->birth_month = 1;
        $student->birth_year = null;
        $this->assertEquals('',$student->age);

        $student->birth_day = 1;
        $student->birth_month = null;
        $student->birth_year = 2000;
        $this->assertEquals('',$student->age);

        $student->birth_day = null;
        $student->birth_month = 1;
        $student->birth_year = 2000;
        $this->assertEquals('',$student->age);

        $date = Carbon::now()->subYears(10);
        $student->birth_day = $date->day;
        $student->birth_month = $date->month;
        $student->birth_year = $date->year;
        $this->assertEquals(10,$student->age);
    }

    public function test_get_birth_date_attribute()
    {
        $student = Student::factory(1)->make(['birth_year' => null, 'birth_month' => null, 'birth_day' => null])->first();
        $this->assertEquals('',$student->birth_date);

        $student->birth_day = 1;
        $student->birth_month = null;
        $student->birth_year = null;
        $this->assertEquals('',$student->birth_date);

        $student->birth_day = null;
        $student->birth_month = 1;
        $student->birth_year = null;
        $this->assertEquals('',$student->birth_date);

        $student->birth_day = null;
        $student->birth_month = null;
        $student->birth_year = 2000;
        $this->assertEquals('',$student->birth_date);

        $student->birth_day = 1;
        $student->birth_month = 1;
        $student->birth_year = null;
        $this->assertEquals('',$student->birth_date);

        $student->birth_day = 1;
        $student->birth_month = null;
        $student->birth_year = 2000;
        $this->assertEquals('',$student->birth_date);

        $student->birth_day = null;
        $student->birth_month = 1;
        $student->birth_year = 2000;
        $this->assertEquals('',$student->birth_date);

        $student->birth_day = 1;
        $student->birth_month = 2;
        $student->birth_year = 2000;
        $this->assertEquals('2000-02-01',$student->birth_date);
    }

    public function test_get_birth_date_display_attribute()
    {
        $student = Student::factory(1)->make(['birth_year' => null, 'birth_month' => null, 'birth_day' => null])->first();
        $this->assertEquals('',$student->birth_date_display);

        $student->birth_day = 1;
        $student->birth_month = null;
        $student->birth_year = null;
        $this->assertEquals('',$student->birth_date_display);

        $student->birth_day = null;
        $student->birth_month = 1;
        $student->birth_year = null;
        $this->assertEquals('',$student->birth_date_display);

        $student->birth_day = null;
        $student->birth_month = null;
        $student->birth_year = 2000;
        $this->assertEquals('',$student->birth_date_display);

        $student->birth_day = 1;
        $student->birth_month = 1;
        $student->birth_year = null;
        $this->assertEquals('',$student->birth_date_display);

        $student->birth_day = 1;
        $student->birth_month = null;
        $student->birth_year = 2000;
        $this->assertEquals('',$student->birth_date_display);

        $student->birth_day = null;
        $student->birth_month = 1;
        $student->birth_year = 2000;
        $this->assertEquals('',$student->birth_date_display);

        $student->birth_day = 1;
        $student->birth_month = 1;
        $student->birth_year = 2000;
        $this->assertEquals('Jan 1, 2000',$student->birth_date_display);
    }

    public function test_get_field_data_attribute()
    {
        $trainer = Trainer::factory(1)->create()->first();
        $organization = Organization::factory(1)->create()->first();
        $student1 = Student::factory(1)->create(['trainer_id' => $trainer->id])->first();
        $student2 = Student::factory(1)->create(['organization_id' => $organization->id])->first();
        $field1 = Field::factory(1)->create(['trainer_id' => $trainer->id, 'organization_id' => null, 'type' => Field::TYPE_BOOLEAN])->first();
        $field2 = Field::factory(1)->create(['organization_id' => $organization->id, 'trainer_id' => null, 'type' => Field::TYPE_NUMBER])->first();
        $field3 = Field::factory(1)->create(['organization_id' => $organization->id, 'trainer_id' => null, 'type' => Field::TYPE_STRING])->first();
        $field4 = Field::factory(1)->create(['organization_id' => $organization->id, 'trainer_id' => null, 'type' => Field::TYPE_DATE])->first();
        $student1->update([
            'field_data' => [
                Field::TYPE_BOOLEAN => [
                    $field1->id => true
                ]
            ]
        ]);
        $now = Carbon::now();
        $student2->update([
            'field_data' => [
                Field::TYPE_NUMBER => [
                    $field2->id => 41
                ],
                Field::TYPE_STRING => [
                    $field3->id => 'forty-one'
                ],
                Field::TYPE_DATE => [
                    $field4->id => $now
                ]
            ]
        ]);

        $this->assertIsArray($student1->field_data);
        $this->assertIsArray($student2->field_data);
        $this->assertEquals(1,count($student1->field_data));
        $this->assertEquals(3,count($student2->field_data));
        $this->assertTrue($student1->field_data[Field::TYPE_BOOLEAN][$field1->id]);
        $this->assertEquals(41,$student2->field_data[Field::TYPE_NUMBER][$field2->id]);
        $this->assertEquals('forty-one',$student2->field_data[Field::TYPE_STRING][$field3->id]);
        $this->assertEquals($now->format('Y-m-d H:i:s'),$student2->field_data[Field::TYPE_DATE][$field4->id]->format('Y-m-d H:i:s'));
    }

    public function test_get_field_list_attribute()
    {
        $trainer = Trainer::factory(1)->create()->first();
        $organization = Organization::factory(1)->create()->first();
        $student1 = Student::factory(1)->create(['trainer_id' => $trainer->id])->first();
        $student2 = Student::factory(1)->create(['organization_id' => $organization->id])->first();

        $this->assertEmpty($student1->field_list);
        $this->assertEmpty($student2->field_list);

        $field1 = Field::factory(1)->create(['trainer_id' => $trainer->id, 'organization_id' => null])->first();
        $field2 = Field::factory(1)->create(['organization_id' => $organization->id, 'trainer_id' => null])->first();
        $field3 = Field::factory(1)->create(['organization_id' => $organization->id, 'trainer_id' => null])->first();
        $student1->refresh();
        $student2->refresh();

        $this->assertEquals(1,count($student1->field_list));
        $this->assertEquals(2,count($student2->field_list));
        $this->assertEquals($field1->id,$student1->field_list->first()->id);
        $this->assertEquals($field2->id,$student2->field_list->first()->id);
    }

    public function test_get_name_attribute()
    {
        $user = User::factory(1)->create(['first_name' => 'Richard', 'last_name' => null])->first();
        $student = Student::factory(1)->create(['nickname' => null,'user_id' => $user->id])->first();
        $this->assertEquals('Richard',$student->name);

        $user->last_name = 'Goldman';
        $user->save();
        $student->refresh();
        $this->assertEquals('Richard Goldman',$student->name);

        $student->nickname = 'Ricky';
        $student->save();
        $this->assertEquals('Ricky',$student->name);

        $user->last_name = null;
        $user->save();
        $student->refresh();
        $this->assertEquals('Ricky',$student->name);
    }

    // MUTATORS

    public function test_set_birth_date_attribute()
    {
        $student = Student::factory(1)->make()->first();
        $student->birth_date = '2000-01-02';
        $this->assertEquals(2000,$student->birth_year);
        $this->assertEquals(1,$student->birth_month);
        $this->assertEquals(2,$student->birth_day);
    }

    public function test_set_field_data_attribute()
    {
        // Same as test_get_field_data_attribute()
        $this->assertTrue(true);
    }

    // MISC

    public function test_send_invite()
    {
        $student = Student::factory(1)->create()->first();

        // Test render and sending to inactive email address
        $mail = $student->sendInvite('Tesst Testerson', $this->fake_email, false);
        $html = $mail->render();
        $this->assertTrue($mail->hasTo($this->fake_email));
        $this->assertStringContainsString('Tesst Testerson', $html);
        $this->assertStringContainsString(route('student.join',['key' => $student->invite_key]), $html);

        // Test sending fake
        $mail = $student->sendInvite('Tesst Testerson', $this->real_email, true);
        Mail::assertSent(InviteStudent::class, function($mail) {
            $mail->build();
            $this->assertTrue($mail->hasTo($this->real_email));
            $this->assertEquals('emails.invite.student', $mail->view);
            return true;
        });
    }

    // public function test_generate_invite_key(){}

    public function test_has_trainer_user()
    {
        $user1 = User::factory(1)->create()->first();
        $user2 = User::factory(1)->create()->first();
        $organization1 = Organization::factory(1)->create()->first();
        $trainer1 = Trainer::factory(1)->create(['user_id' => $user1->id,'organization_id' => $organization1->id])->first();
        $trainer2 = Trainer::factory(1)->create(['user_id' => $user1->id])->first();
        $trainer3 = Trainer::factory(1)->create(['user_id' => $user2->id])->first();
        $student1 = Student::factory(1)->create(['organization_id' => $organization1->id, 'trainer_id' => null])->first();
        $student2 = Student::factory(1)->create(['trainer_id' => $trainer2->id, 'organization_id' => null])->first();
        $student3 = Student::factory(1)->create(['trainer_id' => $trainer3->id, 'organization_id' => null])->first();
        
        $this->assertTrue($student1->hasTrainerUser($user1));
        $this->assertFalse($student1->hasTrainerUser($user2));
        $this->assertTrue($student2->hasTrainerUser($user1));
        $this->assertFalse($student2->hasTrainerUser($user2));
        $this->assertFalse($student3->hasTrainerUser($user1));
        $this->assertTrue($student3->hasTrainerUser($user2));
    }
}
