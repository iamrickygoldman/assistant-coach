<?php

namespace Tests;

use Tests\TestCase;

class UnitCase extends TestCase
{
    protected function getStorageDir()
    {
        return str_replace('tests','storage',__DIR__);
    }

    protected function getPublicDir()
    {
        return str_replace('tests','public',__DIR__);
    }
}
