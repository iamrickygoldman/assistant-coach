<?php

namespace Tests;

use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;

use App\Models\Field;
use App\Models\Organization;
use App\Models\Role;
use App\Models\Student;
use App\Models\Trainer;
use App\Models\User;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    use DatabaseTransactions;

    protected string $real_email = 'iamrickygoldman@gmail.com';

    protected string $fake_email = 'fake@iamricky.com';

    private array $counts = [];

    /*
    protected function setUp(): void
    {
        parent::setUp();
    }

    protected function tearDown(): void
    {
    	parent::tearDown();
    }
    */

    protected function getAdminUser(): User
    {
        $user = User::factory(1)->create()->first();
        $user->roles()->attach(Role::ADMIN_ID);
        return $user;
    }

    protected function getSuperAdminUser(): User
    {
        $user = User::factory(1)->create()->first();
        $user->roles()->attach(Role::SUPERADMIN_ID);
        return $user;
    }

    protected function getStudentUser(): User
    {
        $user = User::factory(1)->create()->first();
        $user->roles()->attach(Role::STUDENT_ID);
        return $user;
    }

    protected function getStudentTrainerUser(): User
    {
        $user = User::factory(1)->create()->first();
        $user->roles()->attach([Role::STUDENT_ID,Role::TRAINER_ID]);
        return $user;
    }

    protected function getTrainerUser(): User
    {
        $user = User::factory(1)->create()->first();
        $user->roles()->attach(Role::TRAINER_ID);
        return $user;
    }
}
