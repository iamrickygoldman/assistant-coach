<?php

namespace Tests\Feature\Auth;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\FeatureCase;

use App\Models\User;

class PUserTest extends FeatureCase
{
    public function test_puser()
    {
        $admin = $this->getSuperAdminUser();
        $trainer = $this->getTrainerUser();
        $response = $this->actingAs($admin)
                         ->get(route('coach.dashboard',['puser_id' => $trainer->id]));
        $response->assertStatus(200);

        $admin = $this->getSuperAdminUser();
        $trainer = $this->getTrainerUser();
        $response = $this->actingAs($admin)
                         ->get(route('admin.dashboard',['puser_id' => $trainer->id]));
        $response->assertStatus(302);

    }
}
