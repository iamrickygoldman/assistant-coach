<?php

namespace Tests\Feature\Auth;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\FeatureCase;

use App\Models\User;

class DashboardTest extends FeatureCase
{
    public function test_dashboard()
    {
        $user = $this->getAdminUser();
        $response = $this->actingAs($user)
                         ->get(route('login.route'));
        $response->assertRedirect(route('dashboard'));

        $user = $this->getStudentUser();
        $response = $this->actingAs($user)
                         ->get(route('login.route'));
        $response->assertRedirect(route('dashboard'));

        $user = $this->getSuperAdminUser();
        $response = $this->actingAs($user)
                         ->get(route('login.route'));
        $response->assertRedirect(route('dashboard'));

        $user = $this->getTrainerUser();
        $response = $this->actingAs($user)
                         ->get(route('login.route'));
        $response->assertRedirect(route('dashboard'));

        $user = $this->getStudentTrainerUser();
        $response = $this->actingAs($user)
                         ->get(route('login.route'));
        $response->assertRedirect(route('dashboard'));
    }
}
