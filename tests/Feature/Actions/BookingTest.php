<?php

namespace Tests\Feature\Actions;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\FeatureCase;

use App\Models\Organization;
use App\Models\Role;
use App\Models\Trainer;
use App\Models\User;

class BookingTest extends FeatureCase
{
    public function test_api_request_booking()
    {
        $user1 = User::factory(1)->create()->first();
        $user1->roles()->attach(Role::TRAINER_ID);
        $user2 = User::factory(1)->create()->first();
        $user2->roles()->attach(Role::TRAINER_ID);
        $organization1 = Organization::factory(1)->create()->first();
        $trainer1 = Trainer::factory(1)->create(['user_id' => $user1->id,'organization_id' => $organization1->id])->first();
        $trainer2 = Trainer::factory(1)->create(['user_id' => $user2->id])->first();

        // Request with no data.
        $response = $this->call('POST',route('api.calendar.booking.request'),[]);
        $response->assertStatus(400);
        $response->assertJson([
            'success' => false,
            'error' => __('notifications.invalid_address'),
        ]);

        // Request with invalid organization id.
        $response = $this->call('POST',route('api.calendar.booking.request'),$this->getRequestData(organization_id: -1));
        $response->assertStatus(400);
        $response->assertJson([
            'success' => false,
            'error' => __('notifications.invalid_address'),
        ]);

        // Request with invalid trainer id.
        $response = $this->call('POST',route('api.calendar.booking.request'),$this->getRequestData(trainer_id: -1));
        $response->assertStatus(400);
        $response->assertJson([
            'success' => false,
            'error' => __('notifications.invalid_address'),
        ]);

        // Request with invalid camp id.
        $response = $this->call('POST',route('api.calendar.booking.request'),$this->getRequestData(camp_id: -1));
        $response->assertStatus(400);
        $response->assertJson([
            'success' => false,
            'error' => __('notifications.invalid_address'),
        ]);

        // Request with both organization and trainer ids.
        $response = $this->call('POST',route('api.calendar.booking.request'),$this->getRequestData(trainer_id: $trainer1->id, organization_id: $organization1->id));
        $response->assertStatus(400);
        $response->assertJson([
            'success' => false,
            'error' => __('notifications.invalid_address'),
        ]);

        // Request with valid trainer.
        $response = $this->call('POST',route('api.calendar.booking.request'),$this->getRequestData(trainer_id: $trainer2->id));
        $response->assertCreated();
        $response->assertJson(['success' => true,]);

        // Request with organization.
        $response = $this->actingAs($user1)
                         ->call('POST',route('api.calendar.booking.request'),$this->getRequestData(organization_id: $organization1->id));
        $response->assertCreated();
        $response->assertJson(['success' => true,]);
    }

    private function getRequestData(?string $start = null, ?string $stop = null, ?int $trainer_id = null, ?int $organization_id = null, ?int $camp_id = null, ?string $first_name = 'Tesst', ?string $last_name = null, ?string $email = 'fake@iamricky.com', ?string $phone = null, ?string $notes = null): array
    {
        $data = [];
        $fields = ['start', 'stop', 'trainer_id', 'organization_id', 'camp_id', 'first_name', 'last_name', 'email', 'phone', 'notes'];
        foreach ($fields as $field)
        {
            if (!is_null($$field))
            {
                $data[$field] = $$field;
            }
        }
        return $data;
    }
}
