<?php

namespace Tests\Feature\Actions;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\FeatureCase;

use App\Models\Field;
use App\Models\Organization;
use App\Models\Role;
use App\Models\Student;
use App\Models\Trainer;
use App\Models\User;

class CoachTest extends FeatureCase
{
    public function test_field_save()
    {
        $user1 = User::factory(1)->create()->first();
        $user1->roles()->attach(Role::TRAINER_ID);
        $user2 = User::factory(1)->create()->first();
        $user2->roles()->attach(Role::TRAINER_ID);
        $user3 = User::factory(1)->create()->first();
        $user3->roles()->attach(Role::STUDENT_ID);
        $organization1 = Organization::factory(1)->create()->first();
        $trainer1 = Trainer::factory(1)->create(['user_id' => $user1->id,'organization_id' => $organization1->id])->first();
        $trainer2 = Trainer::factory(1)->create(['user_id' => $user1->id])->first();
        $trainer3 = Trainer::factory(1)->create(['user_id' => $user2->id])->first();
        $trainer4 = Trainer::factory(1)->create(['user_id' => $user3->id])->first();

        // Create with no user and no data.
        $response = $this->call('POST',route('coach.field.save'),[]);
        $response->assertStatus(302);
        $response->assertRedirect(url('/'));

        // Create with no user and incomplete data.
        $response = $this->call('POST',route('coach.field.save'),$this->getFieldData());
        $response->assertStatus(302);
        $response->assertRedirect(url('/'));

        // Create with no user and valid data.
        $response = $this->call('POST',route('coach.field.save'),$this->getFieldData(title: 'Edited Field', type: 'string', display_students_list: 0));
        $response->assertStatus(302);
        $response->assertRedirect(url('/'));

        $new_field = $this->getLastField();
        // Edit with no user.
        $response = $this->call('POST',route('coach.field.save'),$this->getFieldData(id: $new_field->id, title: 'Edited Field', type: 'string', display_students_list: 0));
        $response->assertStatus(302);
        $response->assertRedirect(url('/'));

        // Create with valid user, but no data.
        $response = $this->actingAs($user1)
                         ->call('POST',route('coach.field.save'),[]);
        $response->assertStatus(302);
        $response->assertRedirect(url('/'));

        // Create with valid user, but incomplete data.
        $response = $this->actingAs($user1)
        				 ->call('POST',route('coach.field.save'),$this->getFieldData());
        $response->assertStatus(302);
        $response->assertRedirect(url('/'));

        // Create with invalid organization id.
        $response = $this->actingAs($user1)
                         ->call('POST',route('coach.field.save'),$this->getFieldData(organization_id: -1));
        $response->assertStatus(302);
        $response->assertRedirect(url('/'));

        // Create with invalid trainer id.
        $response = $this->actingAs($user1)
                         ->call('POST',route('coach.field.save'),$this->getFieldData(trainer_id: -1));
        $response->assertStatus(302);
        $response->assertRedirect(url('/'));

        // Create with invalid field id.
        $response = $this->actingAs($user1)
                         ->call('POST',route('coach.field.save'),$this->getFieldData(id: -1));
        $response->assertStatus(302);
        $response->assertRedirect(url('/'));

        // Create with valid user but both organization and trainer ids.
        $response = $this->actingAs($user1)
                         ->call('POST',route('coach.field.save'),$this->getFieldData(trainer_id: $trainer1->id, organization_id: $organization1->id));
        $response->assertStatus(302);
        $response->assertRedirect(url('/'));

        // Create with invalid user role but valid trainer.
        $response = $this->actingAs($user3)
                         ->call('POST',route('coach.field.save'),$this->getFieldData(trainer_id: $trainer4->id));
        $response->assertStatus(302);
        $response->assertRedirect(url('/'));

        // Create with valid user and valid trainer.
        $response = $this->actingAs($user1)
                         ->call('POST',route('coach.field.save'),$this->getFieldData(trainer_id: $trainer1->id));
        $response->assertStatus(302);
        $new_field = $this->getLastField();
        $this->assertInstanceOf(Field::class, $new_field);
        $this->assertEquals('Test Field',$new_field->title);
        $this->assertEquals(Field::TYPE_NUMBER,$new_field->type);
        $this->assertEquals(1,$new_field->display_students_list);
        $this->assertEquals($trainer1->id,$new_field->trainer_id);
        $this->assertNull($new_field->organization_id);
        $response->assertRedirect(route('coach.field.edit',['id' => $new_field->id]));

        // Edit with wrong user.
        $response = $this->actingAs($user2)
                         ->call('POST',route('coach.field.save'),$this->getFieldData(id: $new_field->id, title: 'Edited Field', type: 'string', display_students_list: 0));
        $response->assertStatus(302);
        $response->assertRedirect(url('/'));

        // Edit with correct user.
        $response = $this->actingAs($user1)
                         ->call('POST',route('coach.field.save'),$this->getFieldData(id: $new_field->id, title: 'Edited Field', type: 'string', display_students_list: 0));
        $response->assertStatus(302);
        $new_field->refresh();
        $this->assertInstanceOf(Field::class, $new_field);
        $this->assertEquals('Edited Field',$new_field->title);
        $this->assertEquals('string',$new_field->type);
        $this->assertEquals(0,$new_field->display_students_list);
        $this->assertEquals($trainer1->id,$new_field->trainer_id);
        $this->assertNull($new_field->organization_id);
        $response->assertRedirect(route('coach.field.edit',['id' => $new_field->id]));

        // Create with valid user and valid organization.
        $response = $this->actingAs($user1)
                         ->call('POST',route('coach.field.save'),$this->getFieldData(organization_id: $organization1->id));
        $response->assertStatus(302);
        $new_field = $this->getLastField();
        $this->assertInstanceOf(Field::class, $new_field);
        $this->assertEquals('Test Field',$new_field->title);
        $this->assertEquals(Field::TYPE_NUMBER,$new_field->type);
        $this->assertEquals(1,$new_field->display_students_list);
        $this->assertEquals($organization1->id,$new_field->organization_id);
        $this->assertNull($new_field->trainer_id);
        $response->assertRedirect(route('coach.field.edit',['id' => $new_field->id]));

        // Edit with invalid user.
        $response = $this->actingAs($user2)
                         ->call('POST',route('coach.field.save'),$this->getFieldData(id: $new_field->id, title: 'Edited Field', type: 'string', display_students_list: 0));
        $response->assertStatus(302);
        $response->assertRedirect(url('/'));

        // Edit with valid user.
        $response = $this->actingAs($user1)
                         ->call('POST',route('coach.field.save'),$this->getFieldData(id: $new_field->id, title: 'Edited Field', type: 'string', display_students_list: 0));
        $response->assertStatus(302);
        $new_field->refresh();
        $this->assertInstanceOf(Field::class, $new_field);
        $this->assertEquals('Edited Field',$new_field->title);
        $this->assertEquals('string',$new_field->type);
        $this->assertEquals(0,$new_field->display_students_list);
        $this->assertEquals($organization1->id,$new_field->organization_id);
        $this->assertNull($new_field->trainer_id);
        $response->assertRedirect(route('coach.field.edit',['id' => $new_field->id]));

        // Create with valid user but invalid trainer.
        $response = $this->actingAs($user1)
        				 ->call('POST',route('coach.field.save'),$this->getFieldData(trainer_id: $trainer3->id));
        $response->assertStatus(302);
        $response->assertRedirect(url('/'));

        // Create with invalid user but valid organization.
        $response = $this->actingAs($user2)
                         ->call('POST',route('coach.field.save'),$this->getFieldData(organization_id: $organization1->id));
        $response->assertStatus(302);
        $response->assertRedirect(url('/'));
    }

    public function test_field_delete()
    {
        $user1 = User::factory(1)->create()->first();
        $user1->roles()->attach(Role::TRAINER_ID);
        $user2 = User::factory(1)->create()->first();
        $user2->roles()->attach(Role::TRAINER_ID);
        $organization1 = Organization::factory(1)->create()->first();
        $trainer1 = Trainer::factory(1)->create(['user_id' => $user1->id,'organization_id' => $organization1->id])->first();
        $trainer2 = Trainer::factory(1)->create(['user_id' => $user1->id])->first();
        $trainer3 = Trainer::factory(1)->create(['user_id' => $user2->id])->first();
        $field1 = Field::factory(1)->create(['trainer_id' => $trainer2->id, 'organization_id' => null])->first();
        $field2 = Field::factory(1)->create(['organization_id' => $organization1->id, 'trainer_id' => null])->first();

        // Delete with no user.
        $response = $this->get(route('coach.field.delete',['id' => $field1->id]));
        $response->assertStatus(302);
        $response->assertRedirect(url('/'));

        // Delete with invalid user.
        $response = $this->actingAs($user2)
                         ->get(route('coach.field.delete',['id' => $field1->id]));
        $response->assertStatus(302);
        $response->assertRedirect(url('/'));

        // Delete with valid user based on trainer.
        $response = $this->actingAs($user1)
                         ->get(route('coach.field.delete',['id' => $field1->id]));
        $response->assertStatus(302);
        $response->assertRedirect(route('coach.field.list'));
        $test = Field::find($field1->id);
        $this->assertNull($test);

        // Delete with valid user based on organization.
        $response = $this->actingAs($user1)
                         ->get(route('coach.field.delete',['id' => $field2->id]));
        $response->assertStatus(302);
        $response->assertRedirect(route('coach.field.list'));
        $test = Field::find($field2->id);
        $this->assertNull($test);
    }

    private function getFieldData(?int $id = null, ?int $trainer_id = null, ?int $organization_id = null, string $title = 'Test Field', string $type = Field::TYPE_NUMBER, bool $display_students_list = true): array
    {
    	$data = [
    		'title' => $title,
    		'type' => $type,
    		'display_students_list' => $display_students_list,
    	];
        $fields = ['id', 'trainer_id', 'organization_id'];
        foreach ($fields as $field)
        {
            if (!is_null($$field))
            {
                $data[$field] = $$field;
            }
        }
    	return $data;
    }

    private function getLastField()
    {
    	return Field::orderBy('id','desc')->first();
    }

    public function test_organization_save()
    {
        $user1 = User::factory(1)->create()->first();
        $user1->roles()->attach(Role::TRAINER_ID);
        $user2 = User::factory(1)->create()->first();
        $user2->roles()->attach(Role::TRAINER_ID);
        $user3 = User::factory(1)->create()->first();
        $user3->roles()->attach(Role::STUDENT_ID);

        // Create with no user and no data.
        $response = $this->call('POST',route('coach.organization.save'),[]);
        $response->assertStatus(302);
        $response->assertRedirect(url('/'));

        // Create with no user and no data.
        $response = $this->call('POST',route('coach.organization.save'),[]);
        $response->assertStatus(302);
        $response->assertRedirect(url('/'));

        // Create with no user and valid data.
        $response = $this->call('POST',route('coach.organization.save'),$this->getOrganizationData());
        $response->assertStatus(302);
        $response->assertRedirect(url('/'));

        $new_organization = $this->getLastOrganization();
        // Edit with no user.
        $response = $this->call('POST',route('coach.organization.save'),$this->getOrganizationData(id: $new_organization->id));
        $response->assertStatus(302);
        $response->assertRedirect(url('/'));

        // Create with invalid organization id.
        $response = $this->actingAs($user1)
                         ->call('POST',route('coach.organization.save'),$this->getOrganizationData(id: -1));
        $response->assertStatus(302);
        $response->assertRedirect(url('/'));

        // Create with invalid user role.
        $response = $this->actingAs($user3)
                         ->call('POST',route('coach.organization.save'),$this->getOrganizationData());
        $response->assertStatus(302);
        $response->assertRedirect(url('/'));

        // Create with valid user and minimum data.
        $response = $this->actingAs($user1)
                         ->call('POST',route('coach.organization.save'),$this->getOrganizationData());
        $response->assertStatus(302);

        $new_organization = $this->getLastOrganization();
        $this->assertInstanceOf(Organization::class, $new_organization);
        $this->assertEquals($user1->id, $new_organization->user_id);
        $this->assertEquals('Test Organization',$new_organization->name);
        $this->assertNull($new_organization->email);
        $this->assertNull($new_organization->phone);
        $this->assertNull($new_organization->address1);
        $this->assertNull($new_organization->address2);
        $this->assertNull($new_organization->city);
        $this->assertNull($new_organization->state);
        $this->assertNull($new_organization->zip);
        $this->assertNull($new_organization->country);
        $response->assertRedirect(route('coach.organization.edit',['id' => $new_organization->id]));

        // Create with valid user and maximum data.
        $response = $this->actingAs($user1)
                         ->call('POST',route('coach.organization.save'),$this->getOrganizationData(email: $this->fake_email, phone: '5555555555', address1: '555 Street', address2: '#555', city: 'Town', state: 'IL', zip: '55555', country: 'USA'));
        $response->assertStatus(302);

        $new_organization = $this->getLastOrganization();
        $this->assertInstanceOf(Organization::class, $new_organization);
        $this->assertEquals($user1->id, $new_organization->user_id);
        $this->assertEquals('Test Organization',$new_organization->name);
        $this->assertEquals($this->fake_email,$new_organization->email);
        $this->assertEquals('5555555555',$new_organization->phone);
        $this->assertEquals('555 Street',$new_organization->address1);
        $this->assertEquals('#555',$new_organization->address2);
        $this->assertEquals('Town',$new_organization->city);
        $this->assertEquals('IL',$new_organization->state);
        $this->assertEquals('55555',$new_organization->zip);
        $this->assertEquals('USA',$new_organization->country);
        $response->assertRedirect(route('coach.organization.edit',['id' => $new_organization->id]));

        // Edit with wrong user.
        $response = $this->actingAs($user2)
                         ->call('POST',route('coach.organization.save'),$this->getOrganizationData(id: $new_organization->id));
        $response->assertStatus(302);
        $response->assertRedirect(url('/'));

        // Edit with correct user.
        $response = $this->actingAs($user1)
                         ->call('POST',route('coach.organization.save'),$this->getOrganizationData(id: $new_organization->id, name: 'Edited Organization'));
        $response->assertStatus(302);
        $new_organization->refresh();
        $this->assertInstanceOf(Organization::class, $new_organization);
        $this->assertEquals($user1->id, $new_organization->user_id);
        $this->assertEquals('Edited Organization',$new_organization->name);
        $this->assertEquals($this->fake_email,$new_organization->email);
        $this->assertEquals('5555555555',$new_organization->phone);
        $this->assertEquals('555 Street',$new_organization->address1);
        $this->assertEquals('#555',$new_organization->address2);
        $this->assertEquals('Town',$new_organization->city);
        $this->assertEquals('IL',$new_organization->state);
        $this->assertEquals('55555',$new_organization->zip);
        $this->assertEquals('USA',$new_organization->country);
        $response->assertRedirect(route('coach.organization.edit',['id' => $new_organization->id]));

        // Edit with invalid user.
        $response = $this->actingAs($user3)
                         ->call('POST',route('coach.organization.save'),$this->getOrganizationData(id: $new_organization->id, name: 'Edited Field Again'));
        $response->assertStatus(302);
        $response->assertRedirect(url('/'));

        // Edit with user who belongs to but doesn't own.
        $trainer2 = Trainer::factory(1)->create(['user_id' => $user2->id,'organization_id' => $new_organization->id])->first();
        $response = $this->actingAs($user2)
                         ->call('POST',route('coach.organization.save'),$this->getOrganizationData(id: $new_organization->id));
        $response->assertStatus(302);
        $response->assertRedirect(url('/'));

        // Edit with owning user.
        $response = $this->actingAs($user1)
                         ->call('POST',route('coach.organization.save'),$this->getOrganizationData(id: $new_organization->id, name: 'Edited Organization Again'));
        $response->assertStatus(302);
        $new_organization->refresh();
        $this->assertInstanceOf(Organization::class, $new_organization);
        $this->assertEquals('Edited Organization Again',$new_organization->name);
        $this->assertEquals($this->fake_email,$new_organization->email);
        $this->assertEquals('5555555555',$new_organization->phone);
        $this->assertEquals('555 Street',$new_organization->address1);
        $this->assertEquals('#555',$new_organization->address2);
        $this->assertEquals('Town',$new_organization->city);
        $this->assertEquals('IL',$new_organization->state);
        $this->assertEquals('55555',$new_organization->zip);
        $this->assertEquals('USA',$new_organization->country);
        $response->assertRedirect(route('coach.organization.edit',['id' => $new_organization->id]));
    }

    public function test_organization_delete()
    {
        $user1 = User::factory(1)->create()->first();
        $user1->roles()->attach(Role::TRAINER_ID);
        $user2 = User::factory(1)->create()->first();
        $user2->roles()->attach(Role::TRAINER_ID);
        $user3 = User::factory(1)->create()->first();
        $user3->roles()->attach(Role::TRAINER_ID);
        $organization1 = Organization::factory(1)->create(['user_id' => $user1->id])->first();
        $trainer1 = Trainer::factory(1)->create(['user_id' => $user2->id, 'organization_id' => $organization1->id])->first();

        // Delete with no user.
        $response = $this->get(route('coach.organization.delete',['id' => $organization1->id]));
        $response->assertStatus(302);
        $response->assertRedirect(url('/'));

        // Delete with user who doesn't belongs or own.
        $response = $this->actingAs($user3)
                         ->get(route('coach.organization.delete',['id' => $organization1->id]));
        $response->assertStatus(302);
        $response->assertRedirect(url('/'));

        // Delete with user who belongs to but doesn't own.
        $response = $this->actingAs($user2)
                         ->get(route('coach.organization.delete',['id' => $organization1->id]));
        $response->assertStatus(302);
        $response->assertRedirect(url('/'));

        // Delete with user who doesn't belongs or own.
        $response = $this->actingAs($user3)
                         ->get(route('coach.organization.delete',['id' => $organization1->id]));
        $response->assertStatus(302);
        $response->assertRedirect(url('/'));

        // Delete with owning user.
        $response = $this->actingAs($user1)
                         ->get(route('coach.organization.delete',['id' => $organization1->id]));
        $response->assertStatus(302);
        $response->assertRedirect(route('coach.organization.list'));
        $test = Organization::find($organization1->id);
        $this->assertNull($test);
        $test = Organization::withTrashed()->find($organization1->id);
        $this->assertInstanceOf(Organization::class, $test);
        $this->assertNotNull($test->deleted_at);
    }

    private function getOrganizationData(?int $id = null, string $name = 'Test Organization', ?string $email = null, ?string $phone = null, ?string $address1 = null, ?string $address2 = null, ?string $city = null, ?string $state = null, ?string $zip = null, ?string $country = null): array
    {
        $data = [
            'name' => $name,
        ];
        $fields = ['id', 'email', 'phone', 'address1', 'address2', 'city', 'state', 'zip', 'country'];
        foreach ($fields as $field)
        {
            if (!is_null($$field))
            {
                $data[$field] = $$field;
            }
        }
        return $data;
    }

    private function getLastOrganization()
    {
        return Organization::orderBy('id','desc')->first();
    }

    public function test_student_save()
    {
        $user1 = User::factory(1)->create()->first();
        $user1->roles()->attach(Role::TRAINER_ID);
        $user2 = User::factory(1)->create()->first();
        $user2->roles()->attach(Role::TRAINER_ID);
        $user3 = User::factory(1)->create()->first();
        $user3->roles()->attach(Role::STUDENT_ID);
        $organization1 = Organization::factory(1)->create()->first();
        $trainer1 = Trainer::factory(1)->create(['user_id' => $user1->id,'organization_id' => $organization1->id])->first();
        $trainer2 = Trainer::factory(1)->create(['user_id' => $user1->id])->first();
        $trainer3 = Trainer::factory(1)->create(['user_id' => $user2->id])->first();
        $trainer4 = Trainer::factory(1)->create(['user_id' => $user3->id])->first();
        $field1 = Field::factory(1)->create(['type' => FIELD::TYPE_NUMBER, 'organization_id' => $organization1->id, 'trainer_id' => null])->first();
        $field2 = Field::factory(1)->create(['type' => FIELD::TYPE_STRING, 'organization_id' => $organization1->id, 'trainer_id' => null])->first();

        // Create with no user and no data.
        $response = $this->call('POST',route('coach.student.save'),[]);
        $response->assertStatus(302);
        $response->assertRedirect(url('/'));

        // Create with no user and incomplete data.
        $response = $this->call('POST',route('coach.student.save',$this->getStudentData()));
        $response->assertStatus(302);
        $response->assertRedirect(url('/'));

        // Create with no user and valid data.
        $response = $this->call('POST',route('coach.student.save'),$this->getStudentData(nickname: 'Nick', trainer_id: $trainer1->id, birth_date: '1990-01-02'));
        $response->assertStatus(302);
        $response->assertRedirect(url('/'));

        $new_student = $this->getLastStudent();
        // Edit with no user.
        $response = $this->call('POST',route('coach.student.save'),$this->getStudentData(id: $new_student->id, nickname: 'Nick', birth_date: '1990-02-03'));
        $response->assertStatus(302);
        $response->assertRedirect(url('/'));

        // Create with valid user, but no data.
        $response = $this->actingAs($user1)
                         ->call('POST',route('coach.student.save'),[]);
        $response->assertStatus(302);
        $response->assertRedirect(url('/'));

        // Create with valid user, but incomplete data.
        $response = $this->actingAs($user1)
                         ->call('POST',route('coach.student.save'),$this->getStudentData());
        $response->assertStatus(302);
        $response->assertRedirect(url('/'));

        // Create with invalid organization id.
        $response = $this->actingAs($user1)
                         ->call('POST',route('coach.student.save'),$this->getStudentData(organization_id: -1));
        $response->assertStatus(302);
        $response->assertRedirect(url('/'));

        // Create with invalid trainer id.
        $response = $this->actingAs($user1)
                         ->call('POST',route('coach.student.save'),$this->getStudentData(trainer_id: -1));
        $response->assertStatus(302);
        $response->assertRedirect(url('/'));

        // Create with invalid student id.
        $response = $this->actingAs($user1)
                         ->call('POST',route('coach.student.save'),$this->getStudentData(id: -1));
        $response->assertStatus(302);
        $response->assertRedirect(url('/'));

        // Create with valid user but both organization and trainer ids.
        $response = $this->actingAs($user1)
                         ->call('POST',route('coach.student.save'),$this->getStudentData(trainer_id: $trainer1->id, organization_id: $organization1->id));
        $response->assertStatus(302);
        $response->assertRedirect(url('/'));

        // Create with invalid user role but valid trainer.
        $response = $this->actingAs($user3)
                         ->call('POST',route('coach.student.save'),$this->getStudentData(trainer_id: $trainer4->id));
        $response->assertStatus(302);
        $response->assertRedirect(url('/'));

        // Create with valid user and valid trainer with no extra data.
        $response = $this->actingAs($user1)
                         ->call('POST',route('coach.student.save'),$this->getStudentData(trainer_id: $trainer1->id));
        $response->assertStatus(302);
        $new_student = $this->getLastStudent();
        $this->assertInstanceOf(Student::class, $new_student);
        $this->assertEquals($trainer1->id,$new_student->trainer_id);
        $this->assertNull($new_student->organization_id);
        $this->assertNull($new_student->nickname);
        $this->assertNull($new_student->birth_day);
        $this->assertNull($new_student->birth_month);
        $this->assertNull($new_student->birth_year);
        $this->assertEmpty($new_student->fields);
        $response->assertRedirect(route('coach.student.edit',['id' => $new_student->id]));

        // Edit with wrong user.
        $response = $this->actingAs($user2)
                         ->call('POST',route('coach.student.save'),$this->getStudentData(id: $new_student->id, nickname: 'Nick', birth_date: '2001-03-11'));
        $response->assertStatus(302);
        $response->assertRedirect(url('/'));

        // Edit with correct user.
        $response = $this->actingAs($user1)
                         ->call('POST',route('coach.student.save'),$this->getStudentData(id: $new_student->id, nickname: 'Nick', birth_date: '2001-03-11'));
        $response->assertStatus(302);
        $new_student->refresh();
        $this->assertInstanceOf(Student::class, $new_student);
        $this->assertEquals($trainer1->id,$new_student->trainer_id);
        $this->assertNull($new_student->organization_id);
        $this->assertEquals('Nick',$new_student->nickname);
        $this->assertEquals(11,$new_student->birth_day);
        $this->assertEquals(3,$new_student->birth_month);
        $this->assertEquals(2001,$new_student->birth_year);
        $this->assertEmpty($new_student->fields);
        $response->assertRedirect(route('coach.student.edit',['id' => $new_student->id]));

        // Create with valid user and valid organization with all extra data.
        $response = $this->actingAs($user1)
                         ->call('POST',route('coach.student.save'),$this->getStudentData(organization_id: $organization1->id, nickname: 'Nick', birth_date: '2001-03-11', field_data: [Field::TYPE_NUMBER => [$field1->id => 33]]));
        $response->assertStatus(302);
        $new_student = $this->getLastStudent();
        $this->assertInstanceOf(Student::class, $new_student);
        $this->assertNull($new_student->trainer_id);
        $this->assertEquals($organization1->id, $new_student->organization_id);
        $this->assertEquals('Nick',$new_student->nickname);
        $this->assertEquals(11,$new_student->birth_day);
        $this->assertEquals(3,$new_student->birth_month);
        $this->assertEquals(2001,$new_student->birth_year);
        $fd = $new_student->field_data;
        $this->assertNotEmpty($fd);
        $this->assertArrayHasKey(Field::TYPE_NUMBER, $fd);
        $this->assertNotEmpty($fd[Field::TYPE_NUMBER]);
        $this->assertArrayHasKey($field1->id, $fd[Field::TYPE_NUMBER]);
        $this->assertEquals(33, $fd[Field::TYPE_NUMBER][$field1->id]);
        $response->assertRedirect(route('coach.student.edit',['id' => $new_student->id]));

        // Edit with wrong user.
        $response = $this->actingAs($user2)
                         ->call('POST',route('coach.student.save'),$this->getStudentData(id: $new_student->id, nickname: 'Nick', birth_date: '2001-03-11'));
        $response->assertStatus(302);
        $response->assertRedirect(url('/'));

        // Edit with correct user.
        $response = $this->actingAs($user1)
                         ->call('POST',route('coach.student.save'),$this->getStudentData(id: $new_student->id, nickname: 'Bruce Banner', birth_date: '2012-10-22', field_data: [Field::TYPE_STRING => [$field2->id => 'thirty-three']]));
        $response->assertStatus(302);
        $new_student->refresh();
        $this->assertInstanceOf(Student::class, $new_student);
        $this->assertNull($new_student->trainer_id);
        $this->assertEquals($organization1->id,$new_student->organization_id);
        $this->assertEquals('Bruce Banner',$new_student->nickname);
        $this->assertEquals(22,$new_student->birth_day);
        $this->assertEquals(10,$new_student->birth_month);
        $this->assertEquals(2012,$new_student->birth_year);
        $fd = $new_student->field_data;
        $this->assertNotEmpty($fd);
        $this->assertArrayNotHasKey(Field::TYPE_NUMBER, $fd);
        $this->assertArrayHasKey(Field::TYPE_STRING, $fd);
        $this->assertNotEmpty($fd[Field::TYPE_STRING]);
        $this->assertArrayHasKey($field2->id, $fd[Field::TYPE_STRING]);
        $this->assertEquals('thirty-three', $fd[Field::TYPE_STRING][$field2->id]);
        $response->assertRedirect(route('coach.student.edit',['id' => $new_student->id]));

        // Create with valid user but invalid trainer.
        $response = $this->actingAs($user1)
                         ->call('POST',route('coach.student.save'),$this->getFieldData(trainer_id: $trainer3->id));
        $response->assertStatus(302);
        $response->assertRedirect(url('/'));

        // Create with invalid user but valid organization.
        $response = $this->actingAs($user2)
                         ->call('POST',route('coach.student.save'),$this->getFieldData(organization_id: $organization1->id));
        $response->assertStatus(302);
        $response->assertRedirect(url('/'));

        // Create with valid user and valid organization with invite email.
        $response = $this->actingAs($user1)
                         ->call('POST',route('coach.student.save'),$this->getStudentData(organization_id: $organization1->id, invite_email: $this->real_email, fake: true));
        $response->assertStatus(302);
        $new_student = $this->getLastStudent();
        $this->assertInstanceOf(Student::class, $new_student);
        $this->assertNull($new_student->trainer_id);
        $this->assertEquals($organization1->id, $new_student->organization_id);
        $this->assertNull($new_student->nickname);
        $this->assertNull($new_student->birth_day);
        $this->assertNull($new_student->birth_month);
        $this->assertNull($new_student->birth_year);
        $this->assertEmpty($new_student->field_data);
        $response->assertRedirect(route('coach.student.edit',['id' => $new_student->id]));
    }

    public function test_student_delete()
    {
        $user1 = User::factory(1)->create()->first();
        $user1->roles()->attach(Role::TRAINER_ID);
        $user2 = User::factory(1)->create()->first();
        $user2->roles()->attach(Role::TRAINER_ID);
        $organization1 = Organization::factory(1)->create()->first();
        $trainer1 = Trainer::factory(1)->create(['user_id' => $user1->id,'organization_id' => $organization1->id])->first();
        $trainer2 = Trainer::factory(1)->create(['user_id' => $user1->id])->first();
        $trainer3 = Trainer::factory(1)->create(['user_id' => $user2->id])->first();
        $student1 = Student::factory(1)->create(['trainer_id' => $trainer2->id, 'organization_id' => null])->first();
        $student2 = Student::factory(1)->create(['organization_id' => $organization1->id, 'trainer_id' => null])->first();

        // Delete with no user.
        $response = $this->get(route('coach.student.delete',['id' => $student1->id]));
        $response->assertStatus(302);
        $response->assertRedirect(url('/'));

        // Delete with invalid user.
        $response = $this->actingAs($user2)
                         ->get(route('coach.student.delete',['id' => $student1->id]));
        $response->assertStatus(302);
        $response->assertRedirect(url('/'));

        // Delete with valid user based on trainer.
        $response = $this->actingAs($user1)
                         ->get(route('coach.student.delete',['id' => $student1->id]));
        $response->assertStatus(302);
        $response->assertRedirect(route('coach.student.list'));
        $test = Student::find($student1->id);
        $this->assertNull($test);

        // Delete with valid user based on organization.
        $response = $this->actingAs($user1)
                         ->get(route('coach.student.delete',['id' => $student2->id]));
        $response->assertStatus(302);
        $response->assertRedirect(route('coach.student.list'));
        $test = Student::find($student2->id);
        $this->assertNull($test);
    }

    public function test_student_invite()
    {
        $user1 = User::factory(1)->create()->first();
        $user1->roles()->attach(Role::TRAINER_ID);
        $user2 = User::factory(1)->create()->first();
        $user2->roles()->attach(Role::TRAINER_ID);
        $user3 = User::factory(1)->create()->first();
        $user3->roles()->attach(Role::STUDENT_ID);
        $organization1 = Organization::factory(1)->create()->first();
        $trainer1 = Trainer::factory(1)->create(['user_id' => $user1->id,'organization_id' => $organization1->id])->first();
        $trainer2 = Trainer::factory(1)->create(['user_id' => $user1->id])->first();
        $trainer3 = Trainer::factory(1)->create(['user_id' => $user2->id])->first();
        $trainer4 = Trainer::factory(1)->create(['user_id' => $user3->id])->first();
        $student1 = Student::factory(1)->create(['trainer_id' => $trainer2->id, 'organization_id' => null])->first();
        $student2 = Student::factory(1)->create(['organization_id' => $organization1->id, 'trainer_id' => null])->first();

        // Invite with no user.
        $response = $this->call('POST',route('coach.student.invite', ['id' => $student1->id]),$this->getInviteData(email: $this->real_email, trainer_id: $trainer2->id));
        $response->assertStatus(302);
        $response->assertRedirect(url('/'));

        // Invite with valid user, but no data.
        $response = $this->actingAs($user1)
                         ->call('POST',route('coach.student.invite', ['id' => $student1->id]),[]);
        $response->assertStatus(302);
        $response->assertRedirect(url('/'));

        // Invite with valid user, but incomplete data.
        $response = $this->actingAs($user1)
                         ->call('POST',route('coach.student.invite', ['id' => $student1->id]),$this->getInviteData(email: $this->real_email));
        $response->assertStatus(302);
        $response->assertRedirect(url('/'));

        // Invite with invalid organization id.
        $response = $this->actingAs($user1)
                         ->call('POST',route('coach.student.invite', ['id' => $student1->id]),$this->getInviteData(organization_id: -1, email: $this->real_email));
        $response->assertStatus(302);
        $response->assertRedirect(url('/'));

        // Invite with invalid trainer id.
        $response = $this->actingAs($user1)
                         ->call('POST',route('coach.student.invite', ['id' => $student1->id]),$this->getInviteData(trainer_id: -1, email: $this->real_email));
        $response->assertStatus(302);
        $response->assertRedirect(url('/'));

        // Invite with invalid student id.
        $response = $this->actingAs($user1)
                         ->call('POST',route('coach.student.invite', ['id' => -1]),$this->getInviteData(trainer_id: $trainer2->id,email: $this->real_email));
        $response->assertStatus(302);
        $response->assertRedirect(url('/'));

        // Invite with valid user but both organization and trainer ids.
        $response = $this->actingAs($user1)
                         ->call('POST',route('coach.student.invite', ['id' => $student1->id]),$this->getInviteData(trainer_id: $trainer1->id, organization_id: $organization1->id, email: $this->real_email));
        $response->assertStatus(302);
        $response->assertRedirect(url('/'));

        // Invite with invalid user role but valid trainer.
        $response = $this->actingAs($user3)
                         ->call('POST',route('coach.student.invite', ['id' => $student1->id]),$this->getInviteData(trainer_id: $trainer4->id, email: $this->real_email));
        $response->assertStatus(302);
        $response->assertRedirect(url('/'));

        // Invite with valid user and valid trainer.
        $response = $this->actingAs($user1)
                         ->call('POST',route('coach.student.invite', ['id' => $student1->id]),$this->getInviteData(trainer_id: $trainer1->id, email: $this->real_email));
        $response->assertStatus(302);
        $student1->refresh();
        $this->assertNotNull($student1->invite_key);
        $response->assertRedirect(route('coach.student.edit',['id' => $student1->id]));

        // Invite with valid user and valid organization.
        $response = $this->actingAs($user1)
                         ->call('POST',route('coach.student.invite', ['id' => $student1->id]),$this->getInviteData(organization_id: $organization1->id, email: $this->real_email));
        $response->assertStatus(302);
        $student1->refresh();
        $this->assertNotNull($student1->invite_key);
        $response->assertRedirect(route('coach.student.edit',['id' => $student1->id]));
    }

    private function getStudentData(?int $id = null, ?int $trainer_id = null, ?int $organization_id = null, ?string $nickname = null, ?string $birth_date = null, ?int $birth_day = null, ?int $birth_month = null, ?int $birth_year = null, array $field_data = [], ?string $invite_email = null, ?bool $fake = null): array
    {
        $data = [
            'field_data' => $field_data,
        ];
        $fields = ['id', 'trainer_id', 'organization_id', 'nickname', 'birth_date', 'birth_day', 'birth_month', 'birth_year', 'invite_email', 'fake'];
        foreach ($fields as $field)
        {
            if (!is_null($$field))
            {
                $data[$field] = $$field;
            }
        }
        return $data;
    }

    private function getInviteData(?int $trainer_id = null, ?int $organization_id = null, ?string $email = null, bool $fake = true): array
    {
        $data = [];
        $fields = ['trainer_id', 'organization_id', 'email', 'fake'];
        foreach ($fields as $field)
        {
            if (!is_null($$field))
            {
                $data[$field] = $$field;
            }
        }
        return $data;
    }

    private function getLastStudent()
    {
        return Student::orderBy('id','desc')->first();
    }
}
