<?php

namespace Tests\Feature\Routes;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

use App\Models\User;

class PrivateTest extends TestCase
{
    public function test_dashboard()
    {
        // No user.
        $response = $this->get(route('dashboard'));
        $response->assertStatus(302);

        // Admin.
        $user = $this->getAdminUser();
        $response = $this->actingAs($user)
                         ->get(route('admin.dashboard'));
        $response->assertStatus(200);

        // Student.
        $user = $this->getStudentUser();
        $response = $this->actingAs($user)
                         ->get(route('student.dashboard'));
        $response->assertStatus(200);

        // Student and Trainer.
        $user = $this->getStudentTrainerUser();
        $response = $this->actingAs($user)
                         ->get(route('dashboard'));
        $response->assertStatus(200);

        // SuperAdmin.
        $user = $this->getSuperAdminUser();
        $response = $this->actingAs($user)
                         ->get(route('admin.dashboard'));
        $response->assertStatus(200);

        // Trainer.
        $user = $this->getTrainerUser();
        $response = $this->actingAs($user)
                         ->get(route('coach.dashboard'));
        $response->assertStatus(200);
    }
}
