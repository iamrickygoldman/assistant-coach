<?php

namespace Tests\Feature\Routes;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\FeatureCase;

use Carbon\Carbon;

class CalendarTest extends FeatureCase
{
    public function test_api_get_base_url()
    {
        $response = $this->get(route('api.calendar.base-url'));
        $response->assertStatus(200);
        $response->assertJsonStructure([
            'url',
        ]);
    }

    public function test_api_get_localize()
    {
        $response = $this->get(route('api.calendar.localize'));
        $response->assertStatus(200);
        $response->assertJsonStructure([
            'days' => [
                'long' => ['sunday','monday','tuesday','wednesday','thursday','friday','saturday'],
                'short' => ['sun','mon','tue','wed','thu','fri','sat']
            ],
            'intervals' => [
                'hour','day','week','month','year','date'
            ],
            'months' => [
                'long' => ['january','february','march','april','may','june','july','august','september','october','november','december'],
                'short' => ['jan','feb','mar','apr','may','jun','jul','aug','sep','oct','nov','dec']
            ],
            'booking' => [
                'bookingSuccess','bookingError'
            ]
        ]);
    }

    public function test_api_get_frame()
    {
        $response = $this->get(route('api.calendar.frame'));
        $response->assertStatus(200);
        $response->assertSee('panel-wrap');
        $response->assertSee('ac-calendar-modal');
        $response->assertSee('ac-modal-content');
    }

    public function test_api_get_day()
    {
        $response = $this->get(route('api.calendar.day').'?date=2020-03-25');
        $response->assertStatus(200);
        $response->assertSee('modal-card-body');
        $response->assertSee('Wednesday, 3/25/2020');
    }

    public function test_api_get_week()
    {
        $dt = Carbon::now();
        $today = $dt->format('Y-m-d');
        $dt->addWeek();
        $next_week = $dt->format('Y-m-d');
        $response = $this->get(route('api.calendar.week'));
        $response->assertStatus(200);
        $response->assertSee($today);
        $response = $this->get(route('api.calendar.week').'?offset=1');
        $response->assertStatus(200);
        $response->assertSee($next_week);
    }

    public function test_api_get_month()
    {
        $dt = Carbon::now();
        $dt->startOfMonth();
        $first = $dt->format('Y-m-d');
        $dt->addMonth();
        $next_month_first = $dt->format('Y-m-d');
        $response = $this->get(route('api.calendar.month'));
        $response->assertStatus(200);
        $response->assertSee($first);
        $response = $this->get(route('api.calendar.month').'?offset=1');
        $response->assertStatus(200);
        $response->assertSee($next_month_first);
    }

    public function test_api_get_form()
    {
        $response = $this->get(route('api.calendar.form').'?date=2020-03-25&time=18:43');
        $response->assertStatus(200);
        $response->assertSee('first_name');
        $response->assertSee(route('api.calendar.booking.request'));
        $response->assertSee('2020-03-25 18:43:00');
    }
}
