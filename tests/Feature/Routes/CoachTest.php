<?php

namespace Tests\Feature\Routes;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

use App\Models\Field;
use App\Models\Organization;
use App\Models\Role;
use App\Models\Student;
use App\Models\Trainer;
use App\Models\User;

class CoachTest extends TestCase
{
    public function test_dashboard()
    {
        // No user.
    	$response = $this->get(route('coach.dashboard'));
        $response->assertStatus(302);

        // Wrong role.
        $user = $this->getStudentUser();
        $response = $this->actingAs($user)
                         ->get(route('coach.dashboard'));
        $response->assertStatus(302);

        // Student and Trainer.
        $user = $this->getStudentTrainerUser();
        $response = $this->actingAs($user)
                         ->get(route('coach.dashboard'));
        $response->assertStatus(200);

        // Trainer.
        $user = $this->getTrainerUser();
        $response = $this->actingAs($user)
                         ->get(route('coach.dashboard'));
        $response->assertStatus(200);
    }

    public function test_field_edit()
    {
        $user1 = User::factory(1)->create()->first();
        $user1->roles()->attach(Role::TRAINER_ID);
        $user2 = User::factory(1)->create()->first();
        $user2->roles()->attach(Role::TRAINER_ID);
        $organization1 = Organization::factory(1)->create()->first();
        $trainer1 = Trainer::factory(1)->create(['user_id' => $user1->id,'organization_id' => $organization1->id])->first();
        $trainer2 = Trainer::factory(1)->create(['user_id' => $user1->id])->first();
        $field1 = Field::factory(1)->create(['organization_id' => $organization1->id, 'trainer_id' => null])->first();
        $field2 = Field::factory(1)->create(['trainer_id' => $trainer2->id, 'organization_id' => null])->first();

        // No user.
        $response = $this->get(route('coach.field.edit',['id' => $field1->id]));
        $response->assertStatus(302);

        // Valid params and organization.
        $response = $this->actingAs($user1)
                         ->get(route('coach.field.edit',['id' => $field1->id]));
        $response->assertStatus(200);

        // Valid params but user does not own.
        $response = $this->actingAs($user2)
                         ->get(route('coach.field.edit',['id' => $field1->id]));
        $response->assertStatus(302);

        // Valid params and trainer.
        $response = $this->actingAs($user1)
                         ->get(route('coach.field.edit',['id' => $field2->id]));
        $response->assertStatus(200);

        // Invalid user role.
        $student = $this->getStudentUser();
        $response = $this->actingAs($student)
                         ->get(route('coach.field.edit',['id' => $field1->id]));
        $response->assertStatus(302);
    }

    public function test_field_list()
    {
        // No user.
        $response = $this->get(route('coach.field.list'));
        $response->assertStatus(302);
        $response = $this->get(route('coach.field.list',['sort' => 'title', 'direction', 'asc', 'list' => 'personal']));
        $response->assertStatus(302);

        // Wrong role.
        $user = $this->getStudentUser();
        $response = $this->actingAs($user)
                         ->get(route('coach.field.list'));
        $response->assertStatus(302);
        $response = $this->get(route('coach.field.list',['sort' => 'title', 'direction', 'asc', 'list' => 'personal']));
        $response->assertStatus(302);

        // Trainer.
        $user = $this->getTrainerUser();
        $response = $this->actingAs($user)
                         ->get(route('coach.field.list'));
        $response->assertStatus(200);
        $response = $this->get(route('coach.field.list',['sort' => 'title', 'direction', 'asc', 'list' => 'personal']));
        $response->assertStatus(200);
    }

    public function test_field_new()
    {
        // No user.
    	$response = $this->get(route('coach.field.new'));
        $response->assertStatus(302);

        // Wrong role.
        $user = $this->getStudentUser();
        $response = $this->actingAs($user)
                         ->get(route('coach.field.new'));
        $response->assertStatus(302);

        // No params.
    	$user = $this->getTrainerUser();
    	$response = $this->actingAs($user)
    					 ->get(route('coach.field.new'));
    	$response->assertStatus(302);

        // Invalid params.
    	$response = $this->actingAs($user)
    					 ->get(route('coach.field.new',['o' => 0]));
    	$response->assertStatus(302);

        // Valid params but trainer unassined to user.
    	$trainer1 = Trainer::factory(1)->create(['user_id' => null])->first();
        $user->refresh();
    	$response = $this->actingAs($user)
    					 ->get(route('coach.field.new',['o' => 'personal|'.$trainer1->id]));
    	$response->assertStatus(302);

        // Valid params and trainer.
    	$trainer2 = Trainer::factory(1)->create(['user_id' => $user->id])->first();
        $user->refresh();
    	$response = $this->actingAs($user)
    					 ->get(route('coach.field.new',['o' => 'personal|'.$trainer2->id]));
    	$response->assertStatus(200);

        // Valid params and organization.
    	$organization1 = Organization::factory(1)->create()->first();
    	$trainer3 = Trainer::factory(1)->create(['user_id' => $user->id, 'organization_id' => $organization1->id])->first();
        $user->refresh();
    	$response = $this->actingAs($user)
    					 ->get(route('coach.field.new',['o' => $organization1->name.'|'.$organization1->id]));
    	$response->assertStatus(200);
    }

    public function test_organization_edit()
    {
        $user1 = User::factory(1)->create()->first();
        $user1->roles()->attach(Role::TRAINER_ID);
        $user2 = User::factory(1)->create()->first();
        $user2->roles()->attach(Role::TRAINER_ID);
        $user3 = User::factory(1)->create()->first();
        $user3->roles()->attach(Role::TRAINER_ID);
        $organization1 = Organization::factory(1)->create(['user_id' => $user1->id])->first();
        $trainer1 = Trainer::factory(1)->create(['user_id' => $user1->id])->first();
        $trainer2 = Trainer::factory(1)->create(['user_id' => $user2->id,'organization_id' => $organization1->id])->first();

        // No user.
        $response = $this->get(route('coach.organization.edit',['id' => $organization1->id]));
        $response->assertRedirect('/');

        // User owns.
        $response = $this->actingAs($user1)
                         ->get(route('coach.organization.edit',['id' => $organization1->id]));
        $response->assertStatus(200);

        // User does not own but belongs.
        $response = $this->actingAs($user2)
                         ->get(route('coach.organization.edit',['id' => $organization1->id]));
        $response->assertRedirect(route('coach.organization.view',['id' => $organization1->id]));

        // User does not own or belong.
        $response = $this->actingAs($user3)
                         ->get(route('coach.organization.edit',['id' => $organization1->id]));
        $response->assertRedirect('/');

        // Invalid user role.
        $student = $this->getStudentUser();
        $response = $this->actingAs($student)
                         ->get(route('coach.organization.edit',['id' => $organization1->id]));
        $response->assertRedirect('/');
    }

    public function test_organization_list()
    {
        // No user.
        $response = $this->get(route('coach.organization.list'));
        $response->assertStatus(302);
        $response = $this->get(route('coach.organization.list',['sort' => 'title', 'direction', 'asc', 'list' => 'personal']));
        $response->assertStatus(302);

        // Wrong role.
        $user = $this->getStudentUser();
        $response = $this->actingAs($user)
                         ->get(route('coach.organization.list'));
        $response->assertStatus(302);
        $response = $this->actingAs($user)
                        ->get(route('coach.organization.list',['sort' => 'title', 'direction', 'asc', 'list' => 'personal']));
        $response->assertStatus(302);

        // Trainer.
        $user = $this->getTrainerUser();
        $response = $this->actingAs($user)
                         ->get(route('coach.organization.list'));
        $response->assertStatus(200);
        $response = $this->actingAs($user)
                        ->get(route('coach.organization.list',['sort' => 'title', 'direction', 'asc', 'list' => 'personal']));
        $response->assertStatus(200);
    }

    public function test_organization_new()
    {
        // No user.
        $response = $this->get(route('coach.organization.new'));
        $response->assertStatus(302);

        // Wrong role.
        $user = $this->getStudentUser();
        $response = $this->actingAs($user)
                         ->get(route('coach.organization.new'));
        $response->assertStatus(302);

        // Trainer.
        $user = $this->getTrainerUser();
        $trainer1 = Trainer::factory(1)->create(['user_id' => $user->id])->first();
        $response = $this->actingAs($user)
                         ->get(route('coach.organization.new'));
        $response->assertStatus(200);
    }

    public function test_organization_view()
    {
        $user1 = User::factory(1)->create()->first();
        $user1->roles()->attach(Role::TRAINER_ID);
        $user2 = User::factory(1)->create()->first();
        $user2->roles()->attach(Role::TRAINER_ID);
        $user3 = User::factory(1)->create()->first();
        $user3->roles()->attach(Role::TRAINER_ID);
        $organization1 = Organization::factory(1)->create(['user_id' => $user1->id])->first();
        $trainer1 = Trainer::factory(1)->create(['user_id' => $user1->id])->first();
        $trainer2 = Trainer::factory(1)->create(['user_id' => $user2->id,'organization_id' => $organization1->id])->first();

        // No user.
        $response = $this->get(route('coach.organization.view',['id' => $organization1->id]));
        $response->assertStatus(302);

        // User owns.
        $response = $this->actingAs($user1)
                         ->get(route('coach.organization.view',['id' => $organization1->id]));
        $response->assertStatus(200);

        // User does not own but belongs.
        $response = $this->actingAs($user2)
                         ->get(route('coach.organization.view',['id' => $organization1->id]));
        $response->assertStatus(200);

        // User does not own or belong.
        $response = $this->actingAs($user3)
                         ->get(route('coach.organization.view',['id' => $organization1->id]));
        $response->assertStatus(302);

        // Invalid user role.
        $student = $this->getStudentUser();
        $response = $this->actingAs($student)
                         ->get(route('coach.organization.view',['id' => $organization1->id]));
        $response->assertStatus(302);
    }

    public function test_student_edit()
    {
        $user1 = User::factory(1)->create()->first();
        $user1->roles()->attach(Role::TRAINER_ID);
        $user2 = User::factory(1)->create()->first();
        $user2->roles()->attach(Role::TRAINER_ID);
        $organization1 = Organization::factory(1)->create()->first();
        $trainer1 = Trainer::factory(1)->create(['user_id' => $user1->id,'organization_id' => $organization1->id])->first();
        $trainer2 = Trainer::factory(1)->create(['user_id' => $user1->id])->first();
        $student1 = Student::factory(1)->create(['organization_id' => $organization1->id, 'trainer_id' => null])->first();
        $student2 = Student::factory(1)->create(['trainer_id' => $trainer2->id, 'organization_id' => null])->first();

        // No user.
        $response = $this->get(route('coach.student.edit',['id' => $student1->id]));
        $response->assertStatus(302);

        // Valid params and organization.
        $response = $this->actingAs($user1)
                         ->get(route('coach.student.edit',['id' => $student1->id]));
        $response->assertStatus(200);

        // Valid params but user does not own.
        $response = $this->actingAs($user2)
                         ->get(route('coach.student.edit',['id' => $student1->id]));
        $response->assertStatus(302);

        // Valid params and trainer.
        $response = $this->actingAs($user1)
                         ->get(route('coach.student.edit',['id' => $student2->id]));
        $response->assertStatus(200);

        // Invalid user role.
        $student = $this->getStudentUser();
        $response = $this->actingAs($student)
                         ->get(route('coach.student.edit',['id' => $student1->id]));
        $response->assertStatus(302);
    }

    public function test_student_list()
    {
        // No user.
        $response = $this->get(route('coach.student.list'));
        $response->assertStatus(302);
        $response = $this->get(route('coach.student.list',['sort' => 'name', 'direction', 'asc', 'list' => 'personal']));
        $response->assertStatus(302);

        // Wrong role.
        $user = $this->getStudentUser();
        $response = $this->actingAs($user)
                         ->get(route('coach.student.list'));
        $response->assertStatus(302);
        $response = $this->actingAs($user)
                        ->get(route('coach.student.list',['sort' => 'name', 'direction', 'asc', 'list' => 'personal']));
        $response->assertStatus(302);

        // Trainer.
        $user = $this->getTrainerUser();
        $response = $this->actingAs($user)
                         ->get(route('coach.student.list'));
        $response->assertStatus(200);
        $response = $this->actingAs($user)
                        ->get(route('coach.student.list',['sort' => 'name', 'direction', 'asc', 'list' => 'personal']));
        $response->assertStatus(200);
    }

    public function test_student_new()
    {
        // No user.
        $response = $this->get(route('coach.student.new'));
        $response->assertStatus(302);

        // Wrong role.
        $user = $this->getStudentUser();
        $response = $this->actingAs($user)
                         ->get(route('coach.student.new'));
        $response->assertStatus(302);

        // No params.
        $user = $this->getTrainerUser();
        $response = $this->actingAs($user)
                         ->get(route('coach.student.new'));
        $response->assertStatus(302);

        // Invalid params.
        $response = $this->actingAs($user)
                         ->get(route('coach.student.new',['o' => 0]));
        $response->assertStatus(302);

        // Valid params but trainer unassined to user.
        $trainer1 = Trainer::factory(1)->create(['user_id' => null])->first();
        $user->refresh();
        $response = $this->actingAs($user)
                         ->get(route('coach.student.new',['o' => 'personal|'.$trainer1->id]));
        $response->assertStatus(302);

        // Valid params and trainer.
        $trainer2 = Trainer::factory(1)->create(['user_id' => $user->id])->first();
        $user->refresh();
        $response = $this->actingAs($user)
                         ->get(route('coach.student.new',['o' => 'personal|'.$trainer2->id]));
        $response->assertStatus(200);

        // Valid params and organization.
        $organization1 = Organization::factory(1)->create()->first();
        $trainer3 = Trainer::factory(1)->create(['user_id' => $user->id, 'organization_id' => $organization1->id])->first();
        $user->refresh();
        $response = $this->actingAs($user)
                         ->get(route('coach.student.new',['o' => $organization1->name.'|'.$organization1->id]));
        $response->assertStatus(200);
    }
}
