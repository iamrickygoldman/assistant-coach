<?php

namespace Tests\Feature\Routes;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class PublicTest extends TestCase
{
    public function test_home()
    {
        // No user.
        $response = $this->get('/');
        $response->assertStatus(200);

        // Student.
        $user = $this->getStudentUser();
        $response = $this->actingAs($user)
                         ->get('/');
        $response->assertStatus(200);

        // Student and Trainer.
        $user = $this->getStudentTrainerUser();
        $response = $this->actingAs($user)
                         ->get('/');
        $response->assertStatus(200);
    }

    public function test_calendar()
    {
        // No user.
        $response = $this->get(route('calendar'));
        $response->assertStatus(200);

        // Student.
        $user = $this->getStudentUser();
        $response = $this->actingAs($user)
                         ->get(route('calendar'));
        $response->assertStatus(200);

        // Student and Trainer.
        $user = $this->getStudentTrainerUser();
        $response = $this->actingAs($user)
                         ->get(route('calendar'));
        $response->assertStatus(200);
    }
}
