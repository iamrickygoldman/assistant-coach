<?php

namespace Tests\Feature\Routes;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

use App\Models\User;

class AdminTest extends TestCase
{
    public function test_dashboard_superadmin()
    {
        // No user.
        $response = $this->get(route('admin.dashboard'));
        $response->assertStatus(302);

        // Wrong role.
        $user = $this->getStudentUser();
        $response = $this->actingAs($user)
                         ->get(route('admin.dashboard'));
        $response->assertStatus(302);

        // SuperAdmin.
        $user = $this->getSuperAdminUser();
        $response = $this->actingAs($user)
                         ->get(route('admin.dashboard'));
        $response->assertStatus(200);

        // Admin.
        $user = $this->getAdminUser();
        $response = $this->actingAs($user)
                         ->get(route('admin.dashboard'));
        $response->assertStatus(200);
    }
}
