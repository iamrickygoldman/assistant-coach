<?php

namespace Tests\Feature\Routes;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

use App\Models\User;

class StudentTest extends TestCase
{
    public function test_dashboard()
    {
        // No user.
        $response = $this->get(route('student.dashboard'));
        $response->assertStatus(302);

        // Wrong role.
        $user = $this->getTrainerUser();
        $response = $this->actingAs($user)
                         ->get(route('student.dashboard'));
        $response->assertStatus(302);

        // Student and Trainer.
        $user = $this->getStudentTrainerUser();
        $response = $this->actingAs($user)
                         ->get(route('student.dashboard'));
        $response->assertStatus(200);

        // Student
    	$user = $this->getStudentUser();
        $response = $this->actingAs($user)
                         ->get(route('student.dashboard'));
        $response->assertStatus(200);
    }
}
