<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use App\Models\Role;
use App\Models\User;

class TestSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->createUsersWithRoles();
    }

    private function createUsersWithRoles()
    {
        $trainer = User::create([
            'first_name' => 'Trainer',
            'email' => 'trainer@iamricky.com',
            'email_verified_at' => now(),
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            'remember_token' => Str::random(10),
        ]);
        $trainer->roles()->attach(Role::TRAINER_ID);

        $student = User::create([
            'first_name' => 'Student',
            'email' => 'student@iamricky.com',
            'email_verified_at' => now(),
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            'remember_token' => Str::random(10),
        ]);
        $student->roles()->attach(Role::STUDENT_ID);

        $admin = User::create([
            'first_name' => 'Admin',
            'email' => 'admin@iamricky.com',
            'email_verified_at' => now(),
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            'remember_token' => Str::random(10),
        ]);
        $admin->roles()->attach(Role::ADMIN_ID);

        $student_trainer = User::create([
            'first_name' => 'StudentTrainer',
            'email' => 'student.trainer@iamricky.com',
            'email_verified_at' => now(),
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            'remember_token' => Str::random(10),
        ]);
        $student_trainer->roles()->attach([Role::STUDENT_ID,Role::TRAINER_ID]);
    }
}
