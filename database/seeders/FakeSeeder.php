<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Field;
use App\Models\Organization;
use App\Models\Role;
use App\Models\Student;
use App\Models\Trainer;
use App\Models\User;

class FakeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::factory(750)->country('USA')->language('en')->create();
        
        $organizations = Organization::factory(15)->country('USA')->create();

        Trainer::factory(150)->create();

        Student::factory(600)->create();

        foreach($organizations as $organization)
        {
            Trainer::factory(1)->create(['user_id' => $organization->user_id, 'organization_id' => $organization->id]);
        }

        Field::factory(1000)->create();
    }
}
