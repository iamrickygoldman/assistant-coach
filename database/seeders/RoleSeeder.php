<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Role;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::create([
        	'title' => 'SuperAdmin',
        	'description' => 'No limitations.',
        ]);

        Role::create([
        	'title' => 'Admin',
        	'description' => 'Cannot edit other admins or superadmins.',
        ]);

        Role::create([
        	'title' => 'Trainer',
        	'description' => 'Trainer account.',
        ]);

        Role::create([
        	'title' => 'Student',
        	'description' => 'Student account.',
        ]);
    }
}
