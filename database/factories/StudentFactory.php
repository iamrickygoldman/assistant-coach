<?php

namespace Database\Factories;

use App\Models\Student;
use Illuminate\Database\Eloquent\Factories\Factory;

use App\Helpers\ValuesHelper;
use App\Models\Organization;
use App\Models\Role;
use App\Models\Trainer;
use App\Models\User;

class StudentFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Student::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $year = $this->faker->optional(0.9)->numberBetween((int)date('Y')-100,(int)date('Y'));
        $month = $this->faker->optional(0.9)->numberBetween(1,12);
        $date = null;
        if (!is_null($month) && !is_null($year))
        {
            $date = $this->faker->optional(0.9)->numberBetween(1, ValuesHelper::getMaxDaysOfMonth($month, $year));
        }
        $trainer = $this->faker->randomDigit();
        if ($trainer > 7)
        {
            $organization = null;
            $trainer = Trainer::all()->random()->id;
        }
        else
        {
            $organization = Organization::all()->random()->id;
            $trainer = null;
        }
        $user = User::all()->random();
        $user->roles()->syncWithoutDetaching(Role::STUDENT_ID);

        return [
            'nickname' => $this->faker->optional(0.3)->firstName,
            'birth_day' => $date,
            'birth_month' => $month,
            'birth_year' => $year,
            'user_id' => $user->id,
            'organization_id' => $organization,
            'trainer_id' => $trainer,
        ];
    }
}
