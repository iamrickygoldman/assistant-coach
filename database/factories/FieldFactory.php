<?php

namespace Database\Factories;

use App\Models\Field;
use Illuminate\Database\Eloquent\Factories\Factory;

use App\Models\Organization;
use App\Models\Trainer;

class FieldFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Field::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $types = [
            Field::TYPE_BOOLEAN,
            Field::TYPE_DATE,
            Field::TYPE_LIST,
            Field::TYPE_NUMBER,
            Field::TYPE_STRING,
            Field::TYPE_TEXT,
        ];
        $rand = $this->faker->randomDigit();
        if ($rand == 1)
        {
            $organization = Organization::all()->random()->id;
            $trainer = null;
        }
        else
        {
            $organization = null;
            $trainer = Trainer::whereNull('organization_id')->get()->random()->id;
        }
        $trainer = Trainer::all()->random();
        return [
            'title' => $this->faker->word,
            'type' => $this->faker->randomElement($types),
            'display_students_list' => $this->faker->numberBetween(0,1),
            'organization_id' => $organization,
            'trainer_id' => $trainer,
        ];
    }
}
