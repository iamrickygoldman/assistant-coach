<?php

namespace Database\Factories;

use App\Models\Organization;
use Illuminate\Database\Eloquent\Factories\Factory;

use App\Models\Role;
use App\Models\Trainer;
use App\Models\User;

class OrganizationFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Organization::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $user = User::all()->random();
        $user->roles()->syncWithoutDetaching(Role::TRAINER_ID);

        return [
            'name' => $this->faker->company,
            'email' => $this->faker->optional(0.9)->safeEmail,
            'phone' => $this->faker->optional(0.9)->e164PhoneNumber,
            'address1' => $this->faker->optional(0.9)->randomElement([$this->faker->buildingNumber.' '.$this->faker->streetName.' '.$this->faker->streetSuffix]),
            'address2' => $this->faker->optional(0.2)->secondaryAddress,
            'city' => $this->faker->optional(0.9)->city,
            'state' => $this->faker->optional(0.9)->stateAbbr,
            'zip' => $this->faker->optional(0.9)->postcode,
            'country' => $this->faker->optional(0.9)->countryCode,
            'user_id' => $user->id,
        ];
    }

    public function country($country)
    {
        return $this->state(function (array $attributes) use ($country) {
            return [
                'country' => $country,
            ];
        });
    }
}
