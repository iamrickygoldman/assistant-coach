<?php

namespace Database\Factories;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

use App\Models\Role;

class UserFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = User::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'first_name' => $this->faker->firstName,
            'last_name' => $this->faker->optional(0.7)->lastName,
            'email' => $this->faker->unique()->safeEmail,
            'phone' => $this->faker->optional()->e164PhoneNumber,
            'address1' => $this->faker->optional(0.9)->randomElement([$this->faker->buildingNumber.' '.$this->faker->streetName.' '.$this->faker->streetSuffix]),
            'address2' => $this->faker->optional(0.2)->secondaryAddress,
            'city' => $this->faker->optional(0.9)->city,
            'state' => $this->faker->optional(0.9)->stateAbbr,
            'zip' => $this->faker->optional(0.9)->postcode,
            'country' => $this->faker->optional(0.9)->countryCode,
            'language' => $this->faker->languageCode,
            'timezone' => $this->faker->timezone,
            'account_status' => $this->faker->numberBetween(1,2),
            'email_verified_at' => now(),
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            'remember_token' => Str::random(10),
        ];
    }

    /**
     * Indicate that the model's email address should be unverified.
     *
     * @return \Illuminate\Database\Eloquent\Factories\Factory
     */
    public function unverified()
    {
        return $this->state(function (array $attributes) {
            return [
                'email_verified_at' => null,
            ];
        });
    }

    public function accountStatus($status)
    {
        return $this->state(function (array $attributes) use ($status) {
            return [
                'account_status' => $status,
            ];
        });
    }

    public function country($country)
    {
        return $this->state(function (array $attributes) use ($country) {
            return [
                'country' => $country,
            ];
        });
    }

    public function language($language)
    {
        return $this->state(function (array $attributes) use ($language) {
            return [
                'language' => $language,
            ];
        });
    }
}
