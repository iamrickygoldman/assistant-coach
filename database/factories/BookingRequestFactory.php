<?php

namespace Database\Factories;

use App\Models\BookingRequest;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\BookingRequest>
 */
class BookingRequestFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = BookingRequest::class;

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'start' => $this->faker->dateTime(),
            'stop' => $this->faker->dateTime(),
            'first_name' => $this->faker->firstName,
            'last_name' => $this->faker->optional(0.7)->lastName,
            'email' => $this->faker->safeEmail,
            'phone' => $this->faker->optional(0.5)->e164PhoneNumber,
            'notes' => $this->faker->optional(0.3)->paragraph,
        ];
    }
}
