<?php

namespace Database\Factories;

use App\Models\Trainer;
use Illuminate\Database\Eloquent\Factories\Factory;

use App\Models\Organization;
use App\Models\Role;
use App\Models\User;

class TrainerFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Trainer::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $organization = $this->faker->randomDigit();
        if ($organization > 7)
        {
            $organization = Organization::all()->random()->id;
        }
        else
        {
            $organization = null;
        }
        $user = User::all()->random();
        $user->roles()->syncWithoutDetaching(Role::TRAINER_ID);

        return [
            'nickname' => $this->faker->optional(0.3)->firstName,
            'user_id' => $user->id,
            'organization_id' => $organization,
        ];
    }
}
