<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('booking_requests', function (Blueprint $table) {
            $table->id();
            $table->datetime('start')->nullable();
            $table->datetime('stop')->nullable();
            $table->string('first_name');
            $table->string('last_name')->nullable();
            $table->string('email');
            $table->string('phone')->nullable();
            $table->text('notes')->nullable();
            $table->timestamp('read_at')->nullable();
            $table->softDeletes();
        });

        Schema::table('booking_requests', function (Blueprint $table) {
            $table->foreignId('camp_id')->nullable()->constrained()->cascadeOnUpdate()->cascadeOnDelete()->after('id');
            $table->foreignId('trainer_id')->nullable()->constrained()->cascadeOnUpdate()->cascadeOnDelete()->after('id');
            $table->foreignId('organization_id')->nullable()->constrained()->cascadeOnUpdate()->cascadeOnDelete()->after('id');
        });

        Schema::table('bookings', function (Blueprint $table) {
            $table->foreignId('booking_request_id')->nullable()->constrained()->cascadeOnUpdate()->cascadeOnDelete()->after('camp_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bookings', function (Blueprint $table) {
            $table->drop('booking_request_id');
        });

        Schema::dropIfExists('booking_requests');
    }
};
