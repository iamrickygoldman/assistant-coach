<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bookings', function (Blueprint $table) {
            $table->id();
            $table->datetime('start');
            $table->datetime('stop');
        });

        Schema::table('bookings', function (Blueprint $table) {
            $table->foreignId('camp_id')->nullable()->constrained()->cascadeOnUpdate()->cascadeOnDelete()->after('id');
            $table->foreignId('trainer_id')->nullable()->constrained()->cascadeOnUpdate()->cascadeOnDelete()->after('id');
            $table->foreignId('organization_id')->nullable()->constrained()->cascadeOnUpdate()->cascadeOnDelete()->after('id');
            $table->foreignId('student_id')->nullable()->constrained()->cascadeOnUpdate()->cascadeOnDelete()->after('id');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bookings');
    }
}
