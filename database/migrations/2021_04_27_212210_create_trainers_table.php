<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTrainersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trainers', function (Blueprint $table) {
            $table->id();
            $table->string('nickname')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->string('invite_key')->nullable()->unique();
        });

        Schema::table('trainers', function (Blueprint $table) {
            $table->foreignId('organization_id')->nullable()->constrained()->cascadeOnUpdate()->cascadeOnDelete()->after('id');
            $table->foreignId('user_id')->nullable()->constrained()->cascadeOnUpdate()->nullOnDelete()->after('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trainers');
    }
}
