<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCampsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('camps', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->text('description');
            $table->timestamps();
        });

        Schema::table('camps', function (Blueprint $table) {
            $table->foreignId('trainer_id')->nullable()->constrained()->cascadeOnUpdate()->cascadeOnDelete()->after('id');
            $table->foreignId('organization_id')->nullable()->constrained()->cascadeOnUpdate()->cascadeOnDelete()->after('id');
        });

        Schema::create('camp_student', function (Blueprint $table) {
            $table->id();
        });

        Schema::table('camp_student', function (Blueprint $table) {
            $table->foreignId('student_id')->nullable()->constrained()->cascadeOnUpdate()->cascadeOnDelete()->after('id');
            $table->foreignId('camp_id')->nullable()->constrained()->cascadeOnUpdate()->cascadeOnDelete()->after('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('camp_student');
        Schema::dropIfExists('camps');
    }
}
