<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFieldsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fields', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->string('type');
            $table->text('options')->nullable();
            $table->boolean('display_students_list')->default(false);
            $table->timestamps();
        });

        Schema::table('fields', function (Blueprint $table) {
            $table->foreignId('trainer_id')->nullable()->constrained()->cascadeOnUpdate()->cascadeOnDelete()->after('id');
            $table->foreignId('organization_id')->nullable()->constrained()->cascadeOnUpdate()->cascadeOnDelete()->after('id');
        });

        Schema::create('field_student', function (Blueprint $table) {
            $table->id();
            $table->decimal('num_val',16,4)->nullable();
            $table->text('text_val')->nullable();
            $table->datetime('dt_val')->nullable();
            $table->timestamps();
        });

        Schema::table('field_student', function (Blueprint $table) {
            $table->foreignId('student_id')->constrained()->cascadeOnUpdate()->cascadeOnDelete()->after('id');
            $table->foreignId('field_id')->constrained()->cascadeOnUpdate()->cascadeOnDelete()->after('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('field_student');
        Schema::dropIfExists('fields');
    }
}
